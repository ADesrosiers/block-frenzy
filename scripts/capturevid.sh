#!/bin/bash

WINNAME=BlockFrenzy
XLOC=300
YLOC=1000
OFFSETX=1
OFFSETY=28

# need to add to the y since we don't want to see the top application bar
XLOCGAME=$(($XLOC + $OFFSETX))
YLOCGAME=$(($YLOC + $OFFSETY))

# Place the window
# weight=0, xloc=$XLOC, yloc=$YLOC, xdim=unchanged, ydim=unchanged 
wmctrl -r $WINNAME -e 0,$XLOC,$YLOC,-1,-1

# Focus the window
wmctrl -a $WINNAME

# Find dimensions of BlockFrenzy window and put in a temp var
DIMS=$(wmctrl -lG | grep "$WINNAME" | sed -r "s/0x([0-9A-Fa-f]+[ ]+){4}([0-9]+)[ ]+([0-9]+)[ ]+\w+[ ]+\w+/\2x\3/g")

echo "Using dimensions $DIMS"

# Start the video capture using the correct dimensions
#echo ${XLOC}x${YLOC}

#echo $YLOCGAME

#avconv -f alsa -i pulse -f x11grab -acodec pcm_s16le -r 30 -s $DIMS -i :0.0+${XLOCGAME},${YLOCGAME}+nomouse -vcodec libx264 -crf 0 -preset ultrafast -threads 0 $WINNAME.mkv

avconv -f alsa -i pulse -f x11grab -acodec pcm_s16le -r 30 -s $DIMS -i :0.0+${XLOCGAME},${YLOCGAME}+nomouse -vcodec libx264 -crf 0 -preset ultrafast -threads 0 $WINNAME.mkv

echo "Finished.."

# NOTES

# To record audio output:
# 1. open pavucontrol, select "recording" tab
# 2. start avconv, then a recording device should appear in pavucontrol.
# 3. change the source to "Monitor of Build-in Audio Analog Stereo"
