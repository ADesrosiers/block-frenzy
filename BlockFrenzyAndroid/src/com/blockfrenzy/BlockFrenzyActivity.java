package com.blockfrenzy;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.utils.Logger;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.RelativeLayout;

public class BlockFrenzyActivity extends AndroidApplication implements ActivityRequestHandler
{
    private final int HIDE_ADS = 0;
    private final int SHOW_ADS = 1;
    
    private Logger m_logger = new Logger(BlockFrenzyActivity.class.getSimpleName());
    protected AdView m_adView = null;
    
	protected Handler m_handler = new Handler()
	{
		public void handleMessage(android.os.Message msg)
		{
			switch(msg.what)
			{
				case SHOW_ADS :
				{
					m_logger.debug("Showing ad banner");
					m_adView.setVisibility(View.VISIBLE);
					break;
				}
				
				case HIDE_ADS :
				{
					m_logger.debug("Hiding ad banner");
                    m_adView.setVisibility(View.GONE);
					break;
				}
			}
		};
	};
	
	@Override
	public void initialize(ApplicationListener listener,
			AndroidApplicationConfiguration config)
	{
		// Call super, then set up a custom view with ad support
		super.initialize(listener, config);
      
        // Add our game view
        ViewParent parent = graphics.getView().getParent();
        if(parent instanceof ViewGroup)
        {
            // Need a layout to put the game in the background with an ad banner on top.
            RelativeLayout layout = new RelativeLayout(this);
            
        	// First remove the graphics view from its parent, then add it to the new layout
        	ViewGroup group = (ViewGroup) parent;
        	group.removeView(graphics.getView());
        	
            layout.addView(graphics.getView());
            
            // Create and add the ads view
            m_adView = new AdView(this, AdSize.SMART_BANNER, "a15067406aaa743");
            
            // TODO remove debug Set test mode
            AdRequest adRequest = new AdRequest();
            adRequest.addTestDevice(AdRequest.TEST_EMULATOR);
            adRequest.addTestDevice("87BFF8C6639F3777E42BD5AC30A67C41");
            
            // Load an ad
            m_adView.loadAd(adRequest);
            
            // Setup and add the ad view
            RelativeLayout.LayoutParams adParams = new RelativeLayout.LayoutParams(
            		RelativeLayout.LayoutParams.WRAP_CONTENT, 
                    RelativeLayout.LayoutParams.WRAP_CONTENT);
            
            adParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            adParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
    		
            layout.addView(m_adView, adParams);
            
            // Set the layout in the content view
            setContentView(layout);
        }
        else
        {
        	// Cannot display ads!!
        	m_logger.error("Couldn't set up the AdView. Ads will not be displayed");
        }
	}
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        
        m_logger.setLevel(Logger.DEBUG);
        
//        // Need a layout to put the game in the background with an ad banner on top.
//        RelativeLayout layout = new RelativeLayout(this);
//
//        // Do the stuff that initialize() would do for you
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
//                        WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
//        
//        View gameView = initializeForView(new BlockFrenzyGame(), false);
//        
//        m_adView = new AdView(this, AdSize.BANNER, "");
//        
//        // No longer called because we want to use a custom view
        initialize(new BlockFrenzyGame(this), false);
    }

	@Override
	public void showAds(boolean show)
	{
		if(m_adView != null)
		{
			m_handler.sendEmptyMessage(show ? SHOW_ADS : HIDE_ADS);
		}
		else
		{
			m_logger.error("Can't access AdView because it was never created.");
		}
	}
}