package com.blockfrenzy;

import android.graphics.Point;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import com.blockfrenzy.Block.State;

public class GestureListener extends GameController implements View.OnTouchListener
{
    private final BlockFrenzyActivity m_activity;
    private final AbstractRenderer m_renderer;
    private final BlockFrenzyGrid m_grid;
    private GestureDetector m_gestureDetector = null;
    private int m_blockWidth = 0;
    
    private int m_upDragStart = -1;

    private int m_swapEnd = 0;
    private int m_swapStart = -1;
    private Block m_swapSource = null;
    private Block m_swapDest = null;
    private int m_swapIdx = -1;
    
    public GestureListener(BlockFrenzyActivity activity, AbstractRenderer renderer)
    {
        m_activity = activity;
        m_renderer = renderer;
        m_grid = activity.getGrid();
        m_gestureDetector = new GestureDetector(new MyGestureDetector());
        m_blockWidth = m_renderer.getBlockWidth();
    }
    
    public Block getSwapSource()
    {
        return m_swapSource;
    }
    
    public Block getSwapDest()
    {
        return m_swapDest;
    }
    
    public int getSwapDistance()
    {
        return m_swapEnd - m_swapStart;
    }
    
    private class MyGestureDetector extends GestureDetector.SimpleOnGestureListener
    {
        
        @Override
        public boolean onScroll(MotionEvent arg0, MotionEvent event, float arg2, float arg3)
        {
            final MotionEvent e = event;
            // We want this action to be performed on the rendering thread (to avoid thread safety issues), so we push the runnable
            // to the thread's asynchronous request stack.
            m_activity.invokeLater(new Runnable()
            {
                
                @Override
                public void run()
                {
                    StringBuilder sb = new StringBuilder("onScroll()");
                    sb.append(": ").append((int)e.getX()).append(", ").append(m_activity.getCurrTime());
                    Log.e(this.getClass().getCanonicalName(), sb.toString());
                    
                    setSwappingLocation((int)e.getX());
    
                    // Process the upward drag if it has been initiated
                    int upDragUpdate = (int)e.getY();
                    if(m_upDragStart > upDragUpdate)
                    {
                        // To figure out the time to update the row timer by, we need to figure out the percent of a block
                        // that the user moved the stack. For instance, moving up by half a block would be 50%. Then we figure out what
                        // 50% of the timer interval is and advance the timer by that much. Note that we may spill over into another row.
                        long updateTime = m_activity.getRowInterval() * (m_upDragStart - upDragUpdate) / m_blockWidth;
                        //Log.d(this.getClass().getName(), "Update time is " + updateTime);
                        long remTime = m_activity.getRowTimer().getRemainingTime(m_activity.getCurrTime());
                        
                        if(remTime > 0)
                        {
                            if(updateTime > remTime)
                            {
                                updateTime = remTime;
                            }
                            
                            // Update the timer by resetting it and adjusting the start time by updateTime.
                            m_activity.getRowTimer().reset(m_activity.getRowInterval(), m_activity.getRowTimer().getStartTime() - updateTime);
                        }
                        
                        // Update the start drag
                        m_upDragStart = upDragUpdate;
                    }
                }
            });
            
            return false;
        }

        @Override
        public boolean onDown(MotionEvent e)
        {
            final int x = (int) e.getX();
            final int y = (int) e.getY();
            // We want this action to be performed on the rendering thread (to avoid thread safety issues), so we push the runnable
            // to the thread's asynchronous request stack.
            m_activity.invokeLater(new Runnable()
            {
                
                @Override
                public void run()
                {
                    final Point p = m_renderer.getBlockAtLocation(x, y);
                    //StringBuilder sb = new StringBuilder("Touch detected");
                    
                    if(p != null && p.y > 0)
                    {
                        //sb.append(": ").append(p.x).append(", ").append(p.y).append(")");
                        Block source = m_grid.getValue(p.x, p.y);
                        if(source.getId() != Block.EMPTY_ID)
                        {
                            if(source.getState() != Block.State.CLEARING && source.getState() != Block.State.FALLING)
                            {
                                source.setState(State.SWAPPING);
                                m_swapSource = source;
                                m_swapEnd = x;
                                m_swapIdx = p.x;
                                m_swapStart = x;
                                
                                SwapEvent event = new SwapEvent(source, null, 0);
                                notifyListeners(event);
                            }
                        }
                        else
                        {
                            // Only allow up drag if the press was above the level of all blocks
                            int z = 0;
                            for(; z < m_grid.getWidth(); ++z)
                            {
                                if(m_grid.getValue(z, p.y).getId() != Block.EMPTY_ID)
                                {
                                    break;
                                }
                            }
                            
                            if(z == m_grid.getWidth())
                            {
                                //Log.d(this.getClass().getName(), "Setting up drag to " + y);
                                m_upDragStart = y;
                            }
                        }
                    }
                    //Log.e(this.getClass().getCanonicalName(), sb.toString());
                }
            });
            return true;
        }
    }
    
    private void notifyListeners(SwapEvent event)
    {
        for(GameControlListener listener : m_listeners)
        {
            listener.onSwap(event, this);
        }
    }
    
    private void setSwappingLocation(int x)
    {
        if(m_swapSource != null)
        {
            // Deselect the block if it is being cleared
            // Added "m_swapSource.getId() == Block.EMPTY_ID" to make sure empty blocks can't be swapped.
            // This caused strange side effects when put into setSwapSource(), so it's here instead.
            if(m_swapSource.getState() == Block.State.CLEARING)
            {
                m_swapSource = null;
                m_swapDest = null;
                m_swapEnd = -1;
            }
            else
            {
                // Need to make sure the destination is not in the clear state
                int y = 1;
                for(; y < m_grid.getHeight(); ++y)
                {
                    if(m_grid.getValue(m_swapIdx, y) == m_swapSource)
                    {
                        break;
                    }
                }
                
                // Make sure the user isn't dragging off the screen or trying to swap with a clearing block.
                if((m_swapIdx == m_grid.getWidth() - 1 && x > m_swapStart) ||
                   (m_swapIdx == 0 &&  x < m_swapStart) ||
                   (x > m_swapStart && m_swapIdx < m_grid.getWidth() - 1 && m_grid.getValue(m_swapIdx + 1, y).getState() == Block.State.CLEARING) ||
                   (x < m_swapStart && m_swapIdx > 0 && m_grid.getValue(m_swapIdx - 1, y).getState() == Block.State.CLEARING))
                {
                    m_swapEnd = m_swapStart;
                }
                else
                {
                    m_swapEnd = x;
                }
                
                // Need to check if the block has moved far enough to be swapped. If it has,
                // swap it and update all relevant variables. Note that we can only swap if
                // the destination is not being cleared
                if(Math.abs(m_swapEnd - m_swapStart) >= m_blockWidth)
                {
                    if(m_swapEnd >= 0)
                    {
                        // Found the swap source, now find the destination
                        if(m_swapEnd > m_swapStart)
                        {
                            if(m_grid.getValue(m_swapIdx + 1, y).getState() != Block.State.CLEARING)
                            {
                                swap(y, m_swapIdx, m_swapIdx + 1);
                                ++this.m_swapIdx;
                            }
                        }
                        else 
                        {
                            if(m_grid.getValue(m_swapIdx - 1, y).getState() != Block.State.CLEARING)
                            {
                                swap(y, m_swapIdx, m_swapIdx - 1);
                                --this.m_swapIdx;
                            }
                        }
                        m_swapStart = m_swapEnd;
                        
                        // Also need to check for matches and update gravity
                        m_activity.gravity();
                        m_activity.checkMatches();
                        
                        // Check if our swap source block made a match and deselect it if it did
                        // The call to gravity() may have set the swap source to null.
                        if(m_swapSource != null && (m_swapSource.getState() == Block.State.CLEARING || m_swapSource.getState() == Block.State.FALLING))
                        {
                            m_swapSource = null;
                            this.m_swapEnd = -1;
                        }
                        else if(m_swapSource == null)
                        {
                            this.m_swapEnd = -1;
                        }
                        else
                        {
                            // Do nothing
                        }
                    }
                }

                if(m_swapEnd >= 0 && m_swapIdx >= 0)
                {
                    // We will skip row 0 because it isn't in play
                    y = 1;
                    for(; y < m_grid.getHeight(); ++y)
                    {
                        if(m_grid.getValue(m_swapIdx, y) == m_swapSource)
                        {
                            // Found the swap source, now find the destination
                            if(m_swapEnd > m_swapStart)
                            {
                                m_swapDest = m_grid.getValue(m_swapIdx + 1, y);
                            }
                            else if(m_swapEnd < m_swapStart)
                            {
                                m_swapDest = m_grid.getValue(m_swapIdx - 1, y);
                            }
                        }
                    }
                }
            }

            SwapEvent event = new SwapEvent(m_swapSource, m_swapDest, m_swapEnd - m_swapStart);
            notifyListeners(event);
        }
    }

    
    private void swap(int y, int x1, int x2)
    {
        Block b1 = m_grid.getValue(x1, y);
        Block b2 = m_grid.getValue(x2, y);
        
        if(b1.getState() == Block.State.FALLING)
        {
            b2.setState(State.FALLING);
            b2.setTimer(b1.getTimer());
            b1.setState(State.NORMAL);
            b1.setTimer(null);
        }
        else if(b2.getState() == Block.State.FALLING)
        {
            b1.setState(State.FALLING);
            b1.setTimer(b2.getTimer());
            b2.setState(State.NORMAL);
            b2.setTimer(null);
        }
        else
        {
            // Do nothing
        }
        
        m_grid.swap(x1, y, x2, y);

        m_activity.getSounds().playSwap();
    }
    
    
    // This will just call dragReleased() and pass it the last known swap location.
    // This is useful if a block being swapped suddenly starts falling and we need to
    // Clear the swapping state.
    // TODO currently this will not swap the blocks even if they are close enough to be considered swaps.
    public void stopSwap()
    {
        dragReleased(m_swapStart);
    }

    private void dragReleased(int x)
    {
        do
        {   
            // Set the state back to normal
            if(m_swapSource == null)
            {
                break;
            }
            
            m_swapSource.setState(Block.State.NORMAL);
            
            // Make sure the user isn't dragging off the screen
            if((m_swapIdx < 0) ||
               (m_swapIdx == m_grid.getWidth() - 1 && x > m_swapStart) || 
               (m_swapIdx == 0 &&  x < m_swapStart))
            {
                break;
            }

            // We will skip row 0 because it isn't in play
            int y = 1;
            for(; y < m_grid.getHeight(); ++y)
            {
                if(m_grid.getValue(m_swapIdx, y) == m_swapSource)
                {
                    break; // Found the y value
                }
            }
            
            // Make sure user isn't swapping with clearing block
            if((m_swapIdx < m_grid.getWidth() - 1 && m_grid.getValue(m_swapIdx + 1, y).getState() == Block.State.CLEARING) ||
               (m_swapIdx > 0 && m_grid.getValue(m_swapIdx - 1, y).getState() == Block.State.CLEARING))
            {
                break;
            }
            
            // TODO remove magic numbers
            // Swap left
            if(x - m_swapStart <= -m_blockWidth * 0.5)
            {
                swap(y, m_swapIdx, m_swapIdx - 1);
                // Also need to check for matches and update gravity
                m_activity.gravity();
                m_activity.checkMatches();
            }
            else if(x - m_swapStart >= m_blockWidth * 0.5)
            {
                swap(y, m_swapIdx, m_swapIdx + 1);
                // Also need to check for matches and update gravity
                m_activity.gravity();
                m_activity.checkMatches();
            }
            else
            {
                // Don't swap it, because we were less than halfway over the swap point.
            }
        }
        while(false);
        
        m_swapSource = null;
        m_swapDest = null;
        m_swapEnd = -1;
        this.m_swapIdx = -1;
        m_swapStart = -1;

        SwapEvent event = new SwapEvent(m_swapSource, m_swapDest, m_swapEnd - m_swapStart);
        notifyListeners(event);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event)
    {
        if(event.getActionMasked() == MotionEvent.ACTION_UP)
        {
            final int x = (int) event.getX();
            m_activity.invokeLater(new Runnable()
            {
                
                @Override
                public void run()
                {
                    // Clear the up drag
                    if(m_upDragStart != -1)
                    {
                        m_upDragStart = -1;
                    }
                    
                    dragReleased(x);
                }
            });
            return true;
        }
        else
        {
            return m_gestureDetector.onTouchEvent(event);
        }
    }
}
