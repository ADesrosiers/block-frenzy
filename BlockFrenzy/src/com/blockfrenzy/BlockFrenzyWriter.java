package com.blockfrenzy;

import java.io.*;

import android.content.Context;
import android.util.Log;

public class BlockFrenzyWriter {

	public int write(String str, Context c)
	{
		String file = "tmp1.json";

		try {

			FileOutputStream fos = c.openFileOutput(file, Context.MODE_PRIVATE);
			DataOutputStream out = new DataOutputStream(fos);
			out.writeBytes(str);
			out.close();

		} catch (IOException ex) {
			Log.e("File write error: ", ex.toString());
		}
		return 0;
	}
}
