package com.blockfrenzy;

import java.util.Vector;

public abstract class GameController
{
    protected final Vector<GameControlListener> m_listeners = new Vector<GameControlListener>();
    
    public static class SwapEvent
    {
        private final Block m_source;
        private final Block m_dest;
        private final int m_dist;
        
        public SwapEvent(Block source, Block dest, int dist)
        {
            m_source = source;
            m_dest = dest;
            m_dist = dist;
        }
        
        public Block getDest()
        {
            return m_dest;
        }
        
        public int getDist()
        {
            return m_dist;
        }
        
        public Block getSource()
        {
            return m_source;
        }
    }
    
    public static interface GameControlListener
    {
        public void onSwap(SwapEvent event, GameController source);
    }
    
    public void addListener(GameControlListener listener)
    {
        if(m_listeners.contains(listener) == false)
        {
            m_listeners.add(listener);
        }
    }
    
    public void removeListener(GameControlListener listener)
    {
        m_listeners.remove(listener);
    }
    
    public abstract Block getSwapSource();
    
    public abstract Block getSwapDest();
    
    public abstract int getSwapDistance();
    
    public abstract void stopSwap();
}
