package com.blockfrenzy;

import com.blockfrenzy.BlockGroupFactory.BlockGroup;

public class Block 
{
    public static final int EMPTY_ID = 0;
    
    public enum State {NORMAL, CLEARING, FALLING, SWAPPING};
    
    private int m_id = EMPTY_ID;
    private State m_state = State.NORMAL;
    private Timer m_timer = null;
    private BlockGroup m_blockGroup = null;
    
    public Block(int id)
    {
        m_id = id;
    }
    
    public Block(Block src)
    {
        m_id = src.m_id;
        m_state = src.m_state;
        m_timer = src.m_timer;
        m_blockGroup = src.m_blockGroup;
    }
    
    public int getId() 
    {
        return m_id;
    }

    public void setId(int id)
    {
    	m_id = id;
    }

    public State getState() 
    {
        return m_state;
    }
    
    public void setState(State state) 
    {
        this.m_state = state;
    }
    
    public void reset()
    {
        m_state = State.NORMAL;
        m_id = EMPTY_ID;
        m_timer = null;
        m_blockGroup = null;
    }
    
    public Timer getTimer()
    {
        return m_timer;
    }
    
    public void setTimer(Timer timer)
    {
        this.m_timer = timer;
    }
    
    public void setBlockGroup(BlockGroup blockGroup)
    {
        this.m_blockGroup = blockGroup;
    }
    
    public BlockGroup getBlockGroup()
    {
        return m_blockGroup;
    }
}
