package com.blockfrenzy;

import java.io.*;

import android.util.Log;

public class BlockFrenzyReader {
	
	public String read(String file, BlockFrenzyActivity activity) 
	{
		String str = "";
        try {
        	FileInputStream fis = activity.getBaseContext().openFileInput(file);
        	DataInputStream dis = new DataInputStream(fis);
        	str = dis.readLine(); /*String is encoded as one line*/
		} catch(IOException i) {Log.e("File error", i.toString());}
		return str;
	}
}
