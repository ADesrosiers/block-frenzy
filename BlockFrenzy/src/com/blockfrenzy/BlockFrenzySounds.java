package com.blockfrenzy;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;

public class BlockFrenzySounds
{
    private MediaPlayer m_swapPlayer = null;
    private MediaPlayer m_clearPlayer = null;
    private MediaPlayer m_warningPlayer = null;
    private MediaPlayer m_matchPlayer = null;
    
    public BlockFrenzySounds(Context context, Activity activity)
    {
        // Need to set the volume control otherwise changing the volume won't do anything.
        activity.setVolumeControlStream(AudioManager.STREAM_MUSIC);
        
        // Create the players from the sound files
        m_swapPlayer = MediaPlayer.create(context, R.raw.swap);
        m_clearPlayer = MediaPlayer.create(context, R.raw.clear);
        m_warningPlayer = MediaPlayer.create(context, R.raw.warning);
        m_matchPlayer = MediaPlayer.create(context, R.raw.match);
    }
    
    public void playSwap()
    {
        m_swapPlayer.start();
    }
    
    public void playClear()
    {
        m_clearPlayer.start();
    }
    
    public void playWarning()
    {
        m_warningPlayer.start();
    }
    
    public void playMatch()
    {
        m_matchPlayer.start();
    }
}
