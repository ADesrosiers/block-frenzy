package com.blockfrenzy;

import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.View;

import com.blockfrenzy.Block.State;
import com.blockfrenzy.BlockGroupFactory.BlockGroup;

public class BlockFrenzyActivity extends Activity implements SurfaceHolder.Callback, Runnable
{
    /** A handle to the View in which the game is running. */
    private BlockFrenzyView m_blockFrenzyView;
    private static final long INITIAL_ROW_TIME_INTERVAL = 12000L;
    private static final long CLEAR_TIME = 1000L;
    private static final long FALL_TIME = 200L;
    private static final int  GRID_WIDTH    = 6;
    private static final int  GRID_HEIGHT   = 10;
    /*
     * Member (state) fields
     */

    public enum LossWarningState {ON, OFF};
    public static interface LossWarningListener
    {
        public void onLossWarningChanged(LossWarningState state);
    }
    
    private final Vector<LossWarningListener> m_lossWarningListeners = new Vector<LossWarningListener>();
    
    private BlockFrenzyGrid   m_grid;
    
    private Vector<Runnable> m_asynchRequests = new Vector<Runnable>();

    /**
     * The time, in ms it takes for one row to come up from the bottom of the screen.
     * This value will decrease with time in order to make the game progressively harder.
     */
    private long             m_rowInterval = INITIAL_ROW_TIME_INTERVAL;
    
    private PauseableTimer            m_rowTimer = new PauseableTimer(m_rowInterval,
                                                System.currentTimeMillis());
    
    /**
     * Used so that we don't have to call System.currentTimeMillis() in each method
     */
    private long             m_currTime;
    
    private Timer m_nextClear = null;
    
    private LossWarningState m_lossWarningState = LossWarningState.OFF;
    
    private BlockGroup m_currGroup = BlockGroupFactory.getInstance().createBlockGroup();
    
    private long m_pauseTime;
    
    private boolean m_isPaused = false;
    
    private Context m_context = null;
    
    private BlockFrenzySounds m_sounds = null;
    

    /** Indicate whether the surface has been created & is ready to draw */
    private boolean           m_run         = false;

    private static final Block[]     BLOCKS      = new Block[] 
    {
        new Block(1),
        new Block(2),
        new Block(3),
        new Block(4),
        new Block(5),
        new Block(6)
    };
    
    public void setContext(Context context)
    {
        this.m_context = context;
    }
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        // Log for debugging
        Log.d(BlockFrenzyActivity.class.getName(), "onCreate");
        
        // Must call super according to android dev guide
        super.onCreate(savedInstanceState);

        m_grid = new BlockFrenzyGrid(GRID_WIDTH, GRID_HEIGHT);
        for (int x = 0; x < GRID_WIDTH; ++x)
        {
            for (int y = 0; y < (GRID_HEIGHT / 2); ++y)
            {
                m_grid.setValue(x, y, BlockFrenzyActivity.randomBlock());
            }
        }
        
        // Make sure to set the pause time so the onResume() works correctly
        m_pauseTime = System.currentTimeMillis();
        
        // tell system to use the layout defined in our XML file
        setContentView(R.layout.block_frenzy_layout);

        // get handles to the LunarView from XML, and its LunarThread
        m_blockFrenzyView = (BlockFrenzyView) findViewById(R.id.blockfrenzy);
        m_blockFrenzyView.setActivity(this);
        
        m_sounds = new BlockFrenzySounds(m_context, this);
        
        /* Load all game state data back out to objects from file */
        loadStateData();
        
    }
    
    public BlockFrenzySounds getSounds()
    {
        return m_sounds;
    }
    
    public BlockFrenzyGrid getGrid()
    {
        return m_grid;
    }
    
    public Timer getRowTimer()
    {
        return m_rowTimer;
    }
    
    public void invokeLater(Runnable r)
    {
        synchronized (m_asynchRequests)
        {
            m_asynchRequests.add(r);
        }
    }
    
    public long getCurrTime()
    {
        return m_currTime;
    }
    
    public long getCurrGroupId()
    {
        return m_currGroup.getId();
    }
    
    public void addLossWarningListener(LossWarningListener listener)
    {
        if(m_lossWarningListeners.contains(listener) == false)
        {
            m_lossWarningListeners.add(listener);
        }
    }
    
    public void removeLossWarningListener(LossWarningListener listener)
    {
        m_lossWarningListeners.remove(listener);
    }
    
    public long getRowInterval()
    {
        return m_rowInterval;
    }
    
    // Only set the timer if it actually expires sooner than the current next timer
    public void setNextTimer(Timer t)
    {
        if(t != null && (m_nextClear == null || m_nextClear.getRemainingTime(m_currTime) > t.getRemainingTime(m_currTime)))
        {
            m_nextClear = t;
        }
    }
    
    public static Block randomBlock()
    {
        return new Block(BLOCKS[(int) (Math.random() * BLOCKS.length)]);
    }

    private void processRequests()
    {
        // Synchronize so nothing can be added or removed while we are processing.
        // TODO possibly can remove one at a time rather than synchronizing
        synchronized (m_asynchRequests)
        {
            // Complete all asynchronous requests
            for(Runnable r : m_asynchRequests)
            {
                r.run();
            }
            m_asynchRequests.clear();
        }
    }
    
    public void updatePhysics()
    {
        m_currTime = System.currentTimeMillis();
        processRequests();
        
        // Check if the matches are ready to be removed
        Block curr = null;
        boolean isMatchCheckNeeded = false;
        if(m_nextClear != null && m_nextClear.getRemainingTime(m_currTime) < 0)
        {
            m_nextClear = null;
            Timer timer = null;
            boolean hasClears = false;
            
            // Need to check all blocks
            for(int x = 0; x < m_grid.getWidth(); ++x)
            {
                // We start checking at row 1 because row 0 is not in play yet and therefore there can't
                // be any action in that row.
                for(int y = 1; y < m_grid.getHeight(); ++y)
                {
                    curr = m_grid.getValue(x, y);
                    if(curr.getTimer() != null && curr.getTimer().getRemainingTime(m_currTime) < 0)
                    {
                        if(curr.getState() == Block.State.CLEARING)
                        {
                            doChain(x, y, curr.getBlockGroup());
                            curr.reset();
                            hasClears = true;
                        }
                        else if(curr.getState() == Block.State.FALLING)
                        {
                            if(m_grid.getValue(x, y-1).getId() == Block.EMPTY_ID)
                            {
                                if(timer == null)
                                {
                                    timer = new Timer(FALL_TIME, m_currTime);
                                    setNextTimer(timer);
                                }
                                
                                // Need to move the block down one and reset the timer
                                curr.setTimer(timer);
                                m_grid.setValue(x, y-1, curr);
                                m_grid.setValue(x, y, new Block(Block.EMPTY_ID));
                            }
                            else
                            {
                                // It's done falling so put the state back to normal and clear the timer
                                curr.setTimer(null);
                                curr.setState(Block.State.NORMAL);
//                                curr.setBlockGroup(null);
//                                
//                                // Also need to clear the block groups applied above this block
//                                // OK to re-use the curr variable here because this is the last place it's used in the loop
//                                // TODO might be a better way to do this without the extra loop here
//                                for(int z = y + 1; z < m_grid.getHeight(); ++z)
//                                {
//                                    curr = m_grid.getValue(x, z);
//                                    if(curr.getId() == Block.EMPTY_ID || 
//                                       curr.getState() == Block.State.CLEARING || 
//                                       curr.getState() == Block.State.FALLING)
//                                    {
//                                        break;
//                                    }
//                                    else
//                                    {
//                                        curr.setBlockGroup(null);
//                                    }
//                                }
                                
                                isMatchCheckNeeded = true;
                            }
                        }
                        else
                        {
                            // Do nothing
                        }
                    }
                    else
                    {
                        setNextTimer(curr.getTimer());
                    }
                }
            }
            
            // Only need to invoke gravity if blocks were cleared
            if(hasClears == true)
            {
                //Log.d(this.getClass().getName(), "nClears is " + nClears);
                m_sounds.playClear();
                this.gravity();
            }
        }
        
        if (m_rowTimer.getRemainingTime(m_currTime) < 0)
        {
            // Need to check for game over or for game over warning
            boolean isReset = false;
            for(int x = 0; x < m_grid.getWidth(); ++x)
            {
                if(m_grid.getValue(x, m_grid.getHeight() - 1).getId() != Block.EMPTY_ID)
                {
                    // For now, let's just reset everything
                    reset();
                    isReset = true;
                    break;
                }
            }
            
            if(isReset == false)
            {
                // Check for loss warning
                for(int x = 0; x < m_grid.getWidth(); ++x)
                {
                    if(m_grid.getValue(x, m_grid.getHeight() - 2).getId() != Block.EMPTY_ID)
                    {
                        // Notify the observers
                        m_lossWarningState = LossWarningState.ON;
                        for(LossWarningListener listener : m_lossWarningListeners)
                        {
                            listener.onLossWarningChanged(m_lossWarningState);
                        }
                        
                        m_sounds.playWarning();
                        
                        break;
                    }
                }
                
                // Shift the grid by one row to introduce the a new row at the
                // bottom of the
                // playing field.
                m_grid.shiftRows(1);

                m_sounds.playSwap();
    
                // Fill a new row
                for (int x = 0; x < m_grid.getWidth(); ++x)
                {
                    m_grid.setValue(x, 0, randomBlock());
                }

                // Update the row interval and reset the row timer.
                // Let's decrement by 1% each time so that it speeds up quickly at first, but then
                // doesn't speed up as much if the speed is already fast.
                m_rowInterval = (long)((float)m_rowInterval * 0.99F);
                m_rowTimer.reset(m_rowInterval, m_currTime);
            }

            // Check for matches and clear them
            isMatchCheckNeeded = true;
        }

        if(isMatchCheckNeeded == true)
        {
            // We also need to check the matches because the falling block may have just matched with something.
            checkMatches();
            
            // Clear blockgroups
            for(int x = 0; x < m_grid.getWidth(); ++x)
            {
                for(int y = 1; y < m_grid.getHeight(); ++y)
                {
                    curr = m_grid.getValue(x, y);
                    if(curr.getId() != Block.EMPTY_ID && curr.getState() != Block.State.FALLING && curr.getState() != Block.State.CLEARING)
                    {
                        curr.setBlockGroup(null);
                    }
                }
            }
        }
    }

    private BlockGroup mergeGroups(BlockGroup group1, BlockGroup mergeFromGroup, Timer t)
    {
        BlockGroup mergeToGroup = group1;
        BlockGroup currGroup = null;

        // Temporary move all blocks into the mergeFromGroup
        mergeFromGroup.addAll(group1);
        
        // Find the minimum group out of all the blockGroups. Note that this is not necessarily one of the input groups
        for(Block b : mergeFromGroup)
        {
            currGroup = b.getBlockGroup();
            if(currGroup != null && currGroup.getId() < mergeToGroup.getId())
            {
                mergeToGroup = currGroup;
            }
        }
        
        // Clear the mergeToGroup
        mergeToGroup.clear();
        
        // Now merge all the blocks into the minimum group
        for(Block b : mergeFromGroup)
        {
            currGroup = b.getBlockGroup();
            if(currGroup != null && currGroup.getId() != mergeToGroup.getId())
            {
                // Make sure to clear out the old groups so they don't stick around in memory
                currGroup.clear();
            }
            
            // Add the block to the toGroup
            mergeToGroup.add(b);
            
            // Set the block's group to be the toGroup
            b.setBlockGroup(mergeToGroup);
            
            // Also set the clear timer
            b.setTimer(t);
        }
        
        // Clear the fromGroup
        mergeFromGroup.clear();
        
        // Return the mergeTo group
        return mergeToGroup;
    }
    
    public void checkMatches()
    {
        Block curr;
        Vector<BlockGroup> groups = new Vector<BlockGroup>();
        Timer t = new Timer(CLEAR_TIME, m_currTime);
        
        // This is used to figure out if a match was caused by a chain
        int startGroupId = m_currGroup.getId();
        
        // Check horizontal
        int lastId = -1;
        for(int y = 1; y < m_grid.getHeight(); ++y)
        {
            m_currGroup.clear(); // Added to possibly fix disappearing blocks issue. May not be necessary
            lastId = -1;
            for(int x = 0; x <= m_grid.getWidth(); ++x)
            {
                // We do this in order to avoid duplicating code after the for loop
                if(x < m_grid.getWidth())
                {
                    curr = m_grid.getValue(x, y);
                }
                else
                {
                    curr = new Block(Block.EMPTY_ID);
                }
                
                if(lastId != curr.getId() || curr.getState() == Block.State.FALLING || curr.getState() == Block.State.CLEARING)
                {
                    if(m_currGroup.size() >= 3)
                    {
                        BlockGroup nextGroup = BlockGroupFactory.getInstance().createBlockGroup();
                        BlockGroup minGroup = mergeGroups(m_currGroup, nextGroup, t);
                        groups.add(minGroup);
                        
                        if(minGroup.getId() < startGroupId)
                        {
                            minGroup.incrementChain();
                        }
                        
                        m_currGroup = nextGroup;
                    }
                    
                    m_currGroup.clear();
                    lastId = curr.getId();
                }
                
                // Add current block to the group if it's not empty, not being cleared, and not falling
                if(lastId != Block.EMPTY_ID && curr.getState() != Block.State.CLEARING && curr.getState() != Block.State.FALLING)
                {
                    m_currGroup.add(curr);
                }
            }
        }
        
        // Check vertical. Note that we have to check for intersections with horizontal matches and
        // merge them if we find them.
        for(int x = 0; x < m_grid.getWidth(); ++x)
        {
            m_currGroup.clear(); // Added to possibly fix disappearing blocks issue. May not be necessary
            lastId = -1;
            for(int y = 1; y <= m_grid.getHeight(); ++y)
            {
                // We do this in order to avoid duplicating code after the for loop
                if(y < m_grid.getHeight())
                {
                    curr = m_grid.getValue(x, y);
                }
                else
                {
                    curr = new Block(Block.EMPTY_ID);
                }
                
                if(lastId != curr.getId() || curr.getState() == Block.State.FALLING || curr.getState() == Block.State.CLEARING)
                {

                    if(m_currGroup.size() >= 3)
                    {
                        BlockGroup nextGroup = BlockGroupFactory.getInstance().createBlockGroup();
                        BlockGroup minGroup = mergeGroups(m_currGroup, nextGroup, t);

                        if(minGroup.getId() < startGroupId)
                        {
                            minGroup.incrementChain();
                        }
                        
                        groups.add(minGroup);
                        
                        m_currGroup = BlockGroupFactory.getInstance().createBlockGroup();
                    }
                    
                    m_currGroup.clear();
                    lastId = curr.getId();
                }
                
                // Add current block to the group if it's not empty and not being cleared
                if(lastId != Block.EMPTY_ID && curr.getState() != Block.State.CLEARING && curr.getState() != Block.State.FALLING)
                {
                    m_currGroup.add(curr);
                }
            }
        }
    
        if(groups.isEmpty() == false)
        {   
            // Need to mark the block as clearing and notify timer observers
            // TODO we might be able to remove some unused groups first
            for(BlockGroup bg : groups)
            {
                for(Block b : bg)
                {
                    // If we just matched a swapping block, stop swapping
                    if(b.getState() == Block.State.SWAPPING)
                    {
                        m_blockFrenzyView.getController().stopSwap();
                    }
                    
                    b.setState(Block.State.CLEARING);
                }
            }

            setNextTimer(t);
            
            m_sounds.playMatch();
        }
        
        // Need to check if we are no longer in a loss warning state
        if(m_lossWarningState == LossWarningState.ON)
        {
            boolean isWarning = false;
            for(int x = 0; x < m_grid.getWidth(); ++x)
            {
                if(m_grid.getValue(x, m_grid.getHeight() - 1).getId() != Block.EMPTY_ID)
                {
                    isWarning = true;
                    break;
                }
            }
            
            if(isWarning == false)
            {
                // Notify the observers
                m_lossWarningState = LossWarningState.OFF;
                for(LossWarningListener listener : m_lossWarningListeners)
                {
                    listener.onLossWarningChanged(m_lossWarningState);
                }
            }
        }
    }
    
    private void setupChain()
    {
        Block b = null;
        BlockGroup bg = null;
        for(int x = 0; x < m_grid.getWidth(); ++x)
        {
            bg = null;
            for(int y = 1; y < m_grid.getHeight(); ++y)
            {
                b = m_grid.getValue(x, y);
                if(b.getState() == Block.State.CLEARING)
                {
                    // Set the block group to the current group being cleared
                    bg = b.getBlockGroup();
                }
                else if(b.getState() == Block.State.FALLING || b.getId() == Block.EMPTY_ID)
                {
                    // If the current block is empty or falling then we are done processing this vertical column
                    break;
                }
                else
                {
                    // Set the normal block's group to the group of the clearing block below it
                    b.setBlockGroup(bg);
                }
            }
        }
    }
    
    // This is called when a block 
    public void doChain(int x, int y, BlockGroup group)
    {
        Block chain = null;
        // Also need to clear the block groups applied above this block
        // TODO might be a better way to do this without the extra loop here
        for(int z = y + 1; z < m_grid.getHeight(); ++z)
        {
            chain = m_grid.getValue(x, z);
            if(chain.getId() == Block.EMPTY_ID || 
               chain.getState() == Block.State.CLEARING || 
               chain.getState() == Block.State.FALLING)
            {
                break;
            }
            else
            {
                chain.setBlockGroup(group);
            }
        }
    }
    
    public void gravity()
    {
        Timer timer = null;
        Block curr = null;
        Block prev = null;
        Block chain = null;
        boolean isFalling = false;
        
        for(int x = 0; x < m_grid.getWidth(); ++x)
        {
            isFalling = false;
            prev = m_grid.getValue(x, 1);
            // Start at two because the first row is not in play yet and we want to check if the block below this one is empty
            for(int y = 2; y < m_grid.getHeight(); ++y)
            {
                curr = m_grid.getValue(x, y);
                
                // If the block is clearing or it's already falling, don't do anything to it.
                // Need to check the case where the block below is falling in case we swap onto a falling block
                if(curr.getId() != Block.EMPTY_ID && 
                   curr.getState() != Block.State.CLEARING && 
                   curr.getState() != Block.State.FALLING)
                {
                    if(prev.getId() == Block.EMPTY_ID || isFalling == true)
                    {
                        // If we haven't created the timer yet then do it now and check if it is the closest to elapsing by
                        // calling setNextTimer().
                        if(timer == null)
                        {
                            timer = new Timer(FALL_TIME, m_currTime);
                            setNextTimer(timer);
                        }
                        
                        // Let's stop the swap.
                        // TODO maybe in the future can restore the swap after the gravity is finished
                        if(curr.getState() == Block.State.SWAPPING)
                        {
                            m_blockFrenzyView.getController().stopSwap();
                        }
                        
                        // Set the block to falling and initiate the falling timer. Also immediately move this block down one row.
                        // This is because as soon as the falling starts the block is assumed to be in the next row.
                        curr.setState(Block.State.FALLING);
                        curr.setTimer(timer);
                        
                        m_grid.setValue(x, y-1, curr);
                        
                        // We set the isFalling to true. This allows us to just copy the block from above to fill in the space
                        // instead of setting it to a new empty block and then copying the block from above.
                        isFalling = true;
                    }
                    else if(prev.getState() == Block.State.FALLING)
                    {
                        // Important need to check this condition in case a block is swapped directly on top of a falling block.
                        curr.setState(Block.State.FALLING);
                        curr.setTimer(prev.getTimer());
                    }
                }
                else
                {
                    if(isFalling == true)
                    {
                        // Need to put an empty block                        
                        curr = new Block(Block.EMPTY_ID);
                        m_grid.setValue(x, y-1, curr);
                    }
                    
                    isFalling = false;
                }
                    
                prev = curr;
            }

            // Need to check the top block
            if(isFalling == true)
            {
                // Need to put an empty block                        
                curr = new Block(Block.EMPTY_ID);
                m_grid.setValue(x, m_grid.getHeight()-1, curr);
            }
        }
    }
    
    public void reset()
    {
        for (int x = 0; x < m_grid.getWidth(); ++x)
        {
            for (int y = 0; y < m_grid.getHeight(); ++y)
            {
                if(y < (m_grid.getHeight() / 2))
                {
                    m_grid.setValue(x, y, randomBlock());
                }
                else
                {
                    m_grid.setValue(x, y, new Block(Block.EMPTY_ID));
                }
            }
        }
        m_asynchRequests.clear();
        m_rowInterval = INITIAL_ROW_TIME_INTERVAL;
        m_rowTimer.reset(m_rowInterval, m_currTime);
        m_nextClear = null;
    }
    
    public Timer getNextClear()
    {
        return m_nextClear;
    }
    
    @Override
    protected void onPause() 
    {
        // Log for debugging
        Log.d(BlockFrenzyActivity.class.getName(), "onPause");
        
        // Must call super according to android dev guide
        super.onPause();
        
        m_pauseTime = System.currentTimeMillis();

        /* Save all current game state */
        saveStateData();
    }
    
    @Override
    public boolean onSearchRequested()
    {
        boolean retval = super.onSearchRequested();

        if(m_isPaused)
        {
            m_rowTimer.unpause(m_currTime);
            pause(false);
        }
        else
        {
            m_rowTimer.pause(m_currTime);
            pause(true);
        }
        
        m_isPaused = !m_isPaused;
        
        return retval;
    }
    
    @Override
    protected void onResume() 
    {
        // Log for debugging
        Log.d(BlockFrenzyActivity.class.getName(), "onResume");
        
        // Must call super according to android dev guide
        super.onResume();
        
        // Need to update all the timers. We can update them by updating the start time to the remaining time
        // plus the elapsed time.
        long currTime = System.currentTimeMillis();
        long elapsed = currTime - m_pauseTime;
        
        // Update row timer and next clear timer if non-null
        // Make sure it's not negative (the Timer class will throw exception if it is)
        m_rowTimer.reset(m_rowTimer.getInterval(), Math.max(m_rowTimer.getRemainingTime(currTime) + elapsed, 0));
        if(m_nextClear != null)
        {
            // Make sure it's not negative (the Timer class will throw exception if it is)
            m_nextClear.reset(m_nextClear.getInterval(), Math.max(m_nextClear.getRemainingTime(currTime) + elapsed, 0));
        }
        
        // Update all the block timers
        Timer t;
        for(int x = 0; x < m_grid.getWidth(); ++x)
        {
            for(int y = 0; y < m_grid.getHeight(); ++y)
            {
                t = m_grid.getValue(x, y).getTimer();
                if(t != null)
                {   
                    // Make sure it's not negative (the Timer class will throw exception if it is)
                    t.reset(t.getInterval(), Math.max(t.getRemainingTime(currTime) + elapsed, 0));
                }
            }
        }

        SurfaceHolder holder = this.m_blockFrenzyView.getHolder();
        holder.addCallback(this);
        
        unpause();

    }
    
    @Override
    public void onBackPressed()
    {
        // Log for debugging
        Log.d(BlockFrenzyActivity.class.getName(), "onBackPressed");
        super.onBackPressed();
    }
    
    @Override
    protected void onDestroy()
    {
        // Log for debugging
        Log.d(BlockFrenzyActivity.class.getName(), "onDestroy");
        super.onDestroy();
    }
    
    @Override
    protected void onStart()
    {
        // Log for debugging
        Log.d(BlockFrenzyActivity.class.getName(), "onStart");
        super.onStart();
    }
    
    @Override
    protected void onStop()
    {
        // Log for debugging
        Log.d(BlockFrenzyActivity.class.getName(), "onStop");
        super.onStop();
    }
    
    @Override
    protected void onRestart()
    {
        // Log for debugging
        Log.d(BlockFrenzyActivity.class.getName(), "onRestart");
        super.onRestart();
    }
    
    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs)
    {
        // Log for debugging
        Log.d(BlockFrenzyActivity.class.getName(), "onCreateView");
        return super.onCreateView(name, context, attrs);
    }
    
    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        // Log for debugging
        Log.d(BlockFrenzyActivity.class.getName(), "onSaveInstanceState");
        super.onSaveInstanceState(outState);
    }
    
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        // Log for debugging
        Log.d(BlockFrenzyActivity.class.getName(), "onRestoreInstanceState");
        super.onRestoreInstanceState(savedInstanceState);
    }

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
        //AbstractGameLoader.loadGameState("tmp1.json", this.m_blockFrenzyView, this);
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @purpose Create state-holding JSONObject from all necessary game objects' data and write to file
	 * @current Saving following objects: Grid blocks (state, id, timer); nextClear timer.
	 */
	public void saveStateData()
	{
		long now = System.currentTimeMillis(); // time to pass once for all objects to be saved
		
		JSONObject objToWrite = new JSONObject(); // initialize to fill

		try {
			try {
				int width = m_grid.getWidth();
				int height = m_grid.getHeight();
				
				JSONArray arr = new JSONArray(); // JSONArray to hold grid array<Block>
		        for(int x = 0; x < width; ++x)
		        {
		            for(int y = 0; y < height; ++y)
		            {
		    			JSONObject blockObj = new JSONObject();// JSONObject for each block //alternatively only: getValue(x, y).JSONizeState(now, (y * m_width + x)); 
		    			// capture block.id
		    			blockObj.put((y * width + x) + "id", m_grid.getValue(x, y).getId());
		    			// capture block.state
		    			blockObj.put((y * width + x) + "state", m_grid.getValue(x, y).getState());
		    			// capture block.timer if available
		    			if (m_grid.getValue(x, y).getTimer() != null)
		    			{
		    				JSONObject timer = new JSONObject();
		    				try {
		    					Timer t = m_grid.getValue(x, y).getTimer();
		    					if (this != null && t.getRemainingTime(now) > 0) 
		    					{
		    						timer.put("interval", t.getInterval());
		    						timer.put("remains", t.getRemainingTime(now));

		    					}
		    				} catch (JSONException j){Log.e("JSON Error: Timers", j.toString());} 
		    				blockObj.put((y * width + x) + "timer", timer);
		    			}
		    			//put block data into array
		    			arr.put((y * width + x), blockObj);
		            }
		        }
		        
		        objToWrite.put("array", arr); // place array into one JSONObject to return and write later
		        
			}catch (JSONException ex){Log.e("JSON Error: GRID", ex.toString());}

			if (this.getNextClear() != null) {
				JSONObject clearTime = new JSONObject();
				try {
					Timer t = this.getNextClear();
					if (this != null && t.getRemainingTime(now) > 0) 
					{
						clearTime.put("interval", t.getInterval());
						clearTime.put("remains", t.getRemainingTime(now));

					}
				} catch (JSONException j){Log.e("JSON Error: Timers", j.toString());} 
				objToWrite.put("clearTime", clearTime); // Visit Activity's Timer
			}
		} catch(JSONException j){Log.e("JSON Error: Visiting", j.toString());}
		//Log.d("JSONHelp obj:", objToReadWrite.toString());

		
		new BlockFrenzyWriter().write(objToWrite.toString(), this.getBaseContext());
		System.out.println(System.currentTimeMillis()-now);
	}
	/**
	 * @purpose Load all objects' data from JSONObjects retrieved via file (internal storage)
	 */
	public void loadStateData()
	{
		long now = System.currentTimeMillis(); //get time once for all objects

		// make sure objToReadWrite initialized
		try {
		JSONObject objToRead = (JSONObject) new JSONTokener(new BlockFrenzyReader().read("tmp1.json", this)).nextValue(); 
		//Log.d("decodeString", objToReadWrite.toString());
				
			try {
				int width = m_grid.getWidth();
				int height = m_grid.getHeight();
				
		        for(int x = 0; x < width; ++x)
		        {
		            for(int y = 0; y < height; ++y)
		            {
					JSONArray arr = objToRead.getJSONArray("array");
					
	        		JSONObject readObj = (JSONObject)arr.get((y * width + x));
	        		//System.out.println(obj.toString());
	        			// Restores Block.m_Id
	        			m_grid.getValue(x, y).setId(readObj.getInt((y * width + x) + "id")); 
	        			// Restores Block.State
	        			m_grid.getValue(x, y).setState(setStringToState(readObj.getString((y * width + x) + "state"))); 
	        			// Restores block timer
	        			if(readObj.optJSONObject((y * width + x) + "timer") != null) {
	        				JSONObject timer = readObj.optJSONObject((y * width + x) + "timer");
		        			m_grid.getValue(x, y).setTimer((new Timer(timer.optLong("interval"), timer.optLong("remains"))));
	        			}
	        		}
		       }
			}catch (JSONException ex){Log.e("JSON Grid Load", ex.toString());}
			
			if (objToRead.optJSONObject("clearTime") != null)
			{
				//Timer t = 
				//t.setStateFromJSON(objToRead.optJSONObject("clearTime"), now);
				//m_interval = obj.optLong("interval");
				//m_start = (m_interval - obj.optLong("remaining"));
				JSONObject obj = objToRead.getJSONObject("clearTime");
				this.setNextTimer(new Timer(obj.optLong("interval"), obj.optLong("remaining")));
			}
		} catch(JSONException j){Log.e("JSON: Loading", j.toString());}
		  catch(Exception ex){Log.e("loadStateData()", ex.toString());}
		System.out.println(System.currentTimeMillis() - now);
		
	}
	/**
	 * 
	 * @param str
	 * @return Block.State based on string val (usually passed from JSON encoded String)
	 */
	public static State setStringToState(String str) 
	{
		State s;
		if (str == "NORMAL")
			s = State.NORMAL;
		if (str == "CLEARING")
			s = State.CLEARING;
		if (str == "FALLING")
			s = State.FALLING;
		if (str == "SWAPPING")
			s = State.SWAPPING;
		else {s = State.NORMAL;}
	
		return s;		
	}

    @Override
    public void run()
    {
        AbstractRenderer renderer = m_blockFrenzyView.getRenderer();
        this.addLossWarningListener(renderer);
        
        // Check for initial matches
        this.checkMatches();
        this.gravity(); // TODO might not be needed
        
        SurfaceHolder surfaceHolder = m_blockFrenzyView.getHolder();
        
        m_isPaused = false;
        while (m_run)
        {
            Canvas c = null;
            try
            {
                c = surfaceHolder.lockCanvas(null);
                synchronized (surfaceHolder)
                {
                    if (m_isPaused == false)
                    {
                        this.updatePhysics();
                    }
                    renderer.render(c);
                }
            }
            finally
            {
                // do this in a finally so that if an exception is thrown
                // during the above, we don't leave the Surface in an
                // inconsistent state
                if (c != null)
                {
                    surfaceHolder.unlockCanvasAndPost(c);
                }
            }
        }
    }
    /**
     * Pauses the physics update & animation.
     */
    public void pause(boolean isPaused)
    {
        synchronized (m_blockFrenzyView.getHolder())
        {
            m_isPaused = isPaused;
        }
    }

    /**
     * Used to signal the m_thread whether it should be running or not.
     * Passing true allows the m_thread to run; passing false will shut it
     * down if it's already running. Calling start() after this was most
     * recently called with false will result in an immediate shutdown.
     * 
     * @param b
     *            true to run, false to shut down
     */
    public void setRunning(boolean b)
    {
        m_run = b;
    }


    /* Callback invoked when the surface dimensions change. */
    public void setSurfaceSize(int width, int height)
    {
        // synchronized to make sure these all change atomically
        synchronized (m_blockFrenzyView.getHolder())
        {
            m_blockFrenzyView.getRenderer().setRenderBounds(0, width, 0, height);
        }
    }

    /**
     * Resumes from a pause.
     */
    public void unpause()
    {
        synchronized (m_blockFrenzyView.getHolder())
        {
            m_isPaused = true;
        }
    }
}