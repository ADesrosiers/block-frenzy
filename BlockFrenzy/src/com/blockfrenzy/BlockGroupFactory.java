package com.blockfrenzy;

import java.util.Vector;

public class BlockGroupFactory
{
    private static BlockGroupFactory m_instance = null;
    
    private int m_currGroupId = 0;
    
    public static BlockGroupFactory getInstance()
    {
        if(m_instance == null)
        {
            m_instance = new BlockGroupFactory();
        }
        return m_instance;
    }
    
    private BlockGroupFactory()
    {
        
    }
    
    public BlockGroup createBlockGroup()
    {
        BlockGroup newGroup = new BlockGroup(m_currGroupId);
        ++m_currGroupId;
        return newGroup;
    }

    public static class BlockGroup extends Vector<Block>
    {
        private final int m_id;
        private int m_chainCount = 0;
        
        private BlockGroup(int id)
        {
            m_id = id;
        }
        
        public int getId()
        {
            return m_id;
        }
        
        public void incrementChain()
        {
            ++m_chainCount;
        }
        
        public int getChainCount()
        {
            return m_chainCount;
        }
        
        @Override
        public synchronized boolean equals(Object object)
        {
            boolean isEqual = false;
            
            if(object != null && object instanceof BlockGroup && ((BlockGroup) object).getId() == this.getId())
            {
                isEqual = true;
            }
            
            return isEqual;
        }
    
        /**
         * 
         */
        private static final long serialVersionUID = 7169499070388094095L;
    
    }
}
