package com.blockfrenzy;

import android.content.Context;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class BlockFrenzyView extends SurfaceView implements
        SurfaceHolder.Callback
{
    /** The m_thread that actually draws the animation */
    private Thread m_thread;

    private GestureListener m_gestureListener = null;
    
    private AbstractRenderer m_renderer = null;
    
    private BlockFrenzyActivity m_activity = null;

    public BlockFrenzyView(Context context, AttributeSet attrs)
    {
        super(context, attrs);

        // register our interest in hearing about changes to our surface
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);

        setFocusable(true); // make sure we get key events
    }
    
    public void setActivity(BlockFrenzyActivity activity)
    {
        this.m_activity = activity;
        m_activity.setContext(this.getContext());
    }

    /**
     * Standard window-focus override. Notice focus lost so we can pause on
     * focus lost. e.g. user switches to take a call.
     */
    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus)
    {
        if (!hasWindowFocus)
        {
            m_activity.pause(true);
        }
    }

    /* Callback invoked when the surface dimensions change. */
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
            int height)
    {
        m_activity.setSurfaceSize(width, height);
    }

    /*
     * Callback invoked when the Surface has been created and is ready to be
     * used.
     */
    @Override
    public void surfaceCreated(SurfaceHolder holder)
    {
        m_renderer = new GlassRenderer(getContext(), m_activity);
        m_thread = new Thread(m_activity);

        m_gestureListener = new GestureListener(m_activity, m_renderer);
        this.setOnTouchListener(m_gestureListener);
        m_gestureListener.addListener(m_renderer);
        
        // start the m_thread here so that we don't busy-wait in run()
        // waiting for the surface to be created
        m_activity.setRunning(true);
        m_thread.start();
    }
    
    public AbstractRenderer getRenderer()
    {
        return m_renderer;
    }
    
    public GameController getController()
    {
        return m_gestureListener;
    }

    /*
     * Callback invoked when the Surface has been destroyed and must no longer
     * be touched. WARNING: after this method returns, the Surface/Canvas must
     * never be touched again!
     */
    @Override
    public void surfaceDestroyed(SurfaceHolder holder)
    {
        // we have to tell m_thread to shut down & wait for it to finish, or
        // else
        // it might touch the Surface after we return and explode
        boolean retry = true;
        m_activity.setRunning(false);
        while (retry)
        {
            try
            {
                m_thread.join();
                retry = false;
            }
            catch (InterruptedException e)
            {
            }
        }
    }
}
