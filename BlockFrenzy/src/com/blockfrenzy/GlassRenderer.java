package com.blockfrenzy;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import com.blockfrenzy.BlockFrenzyActivity.LossWarningState;
import com.blockfrenzy.GameController.SwapEvent;

public class GlassRenderer implements AbstractRenderer
{
    /** The drawable to use as the background of the animation canvas */
    private Bitmap m_backgroundImage;
    private int m_xLeft;
    private int m_xRight;
    private int m_yTop;
    private int m_yBottom;
    private int m_gridStartWidth;
    private int m_gridStartHeight;

    private Bitmap[] m_bitmaps;
    
    private BlockFrenzyGrid m_grid;
    private BlockFrenzyActivity m_activity;
    private Timer            m_rowTimer;
    private Timer               m_lossWarningTimer = null;
    
    private Block   m_source = null;
    private Block   m_dest = null;
    private int     m_distance = 0;
    
    //private Context m_context = null;
    
    public GlassRenderer(Context context, BlockFrenzyActivity activity)
    {
        m_grid = activity.getGrid();
        m_activity = activity;
        m_rowTimer = m_activity.getRowTimer();
        
        //m_context = context;
        Resources res = context.getResources();

        // load background image as a Bitmap instead of a Drawable b/c
        // we don't need to transform it and it's faster to draw this way
        m_backgroundImage = BitmapFactory.decodeResource(res,
                R.drawable.block_frenzy_bg_hdpi);
        
        m_bitmaps = new Bitmap[]{
            BitmapFactory.decodeResource(res, R.drawable.orange_block_hdpi),
            BitmapFactory.decodeResource(res, R.drawable.blue_block_hdpi),
            BitmapFactory.decodeResource(res, R.drawable.red_block_hdpi),
            BitmapFactory.decodeResource(res, R.drawable.green_block_hdpi),
            BitmapFactory.decodeResource(res, R.drawable.purple_block_hdpi),
            BitmapFactory.decodeResource(res, R.drawable.yellow_block_hdpi)
        };
        
        // Calculate starting bounds
        m_gridStartWidth = m_xLeft + (m_xRight - m_xLeft) / 2 - (m_bitmaps[0].getWidth() * m_grid.getWidth()) / 2;
        m_gridStartHeight = (m_yBottom - m_yTop) / 2 - (m_bitmaps[0].getHeight() * m_grid.getHeight()) / 2;
    }

    @Override
    public void render(Canvas canvas)
    {
        // Unfortunately, we have to pre-process the grid to find the swapping destination. This is because when the user
        // initiates the drag, we don't know which way they are dragging yet. By the time we find that out, the block's position
        // in the grid array may have changed (because the blocks rise). So we have to save the block object and then when the
        // user drags, we save the drag location (left or right), and then on each render we need to save the block to the left
        // or right of the dragging block. If we don't do this before the render, we may have already rendered the left block in
        // the wrong place. The only way around this would be to register for updates when a new row is added to the bottom.
        
        long currentTime = m_activity.getCurrTime();
        
        // Draw the background image. Operations on the Canvas accumulate
        // so this is like clearing the screen.
        canvas.drawBitmap(m_backgroundImage, m_xLeft, m_yTop, null);
        
        // The following prints out the current group id
        // TODO remove debug
        Paint groupPaint = new Paint();
        groupPaint.setTextSize(14); // TODO remove debug
        groupPaint.setColor(Color.WHITE);
        canvas.drawText(String.valueOf(m_activity.getCurrGroupId()), 
                m_xLeft + 10, 
                m_yTop + 20,
                groupPaint);
        // TODO end remove debug

        Bitmap image = m_bitmaps[0];
        
        int adjustedHeight = m_gridStartHeight;
        if(m_rowTimer.getInterval() > 0)
        {
            adjustedHeight += (int)((m_rowTimer.getRemainingTime(null)) * image.getHeight() / m_rowTimer.getInterval());
        }
        
        
        Paint p = new Paint();
        
        // Display red if user is about to lose
        if(m_lossWarningTimer == null)
        {
            p.setColor(Color.BLACK);
        }
        else
        {
            long remTime = m_lossWarningTimer.getRemainingTime(currentTime);
            if(remTime < 0) m_lossWarningTimer.reset(null, currentTime);
            
            float percentLeft = 2.0F - 2.0F*(float)m_lossWarningTimer.getRemainingTime(currentTime) / (float)m_lossWarningTimer.getInterval();
            if(percentLeft > 1.0F)
            {
                percentLeft = 2.0F - percentLeft;
            }
            int c = Color.rgb((int)(percentLeft*(float)0xFF), 0x00, 0x00);
            p.setColor(c);
        }
        
        p.setAlpha(128);
        Paint blockPaint = new Paint(p);

        Paint debugPaint = new Paint();
        debugPaint.setTextSize(14); // TODO remove debug
        debugPaint.setColor(Color.WHITE);
        
        canvas.drawRect(m_gridStartWidth, 
                        m_gridStartHeight, 
                        m_gridStartWidth + (m_grid.getWidth()*image.getWidth()), 
                        m_gridStartHeight + (m_grid.getHeight()*image.getHeight()),
                        p);
        
        Block curr;
        Block swap = null;
        int swapX = 0;
        int swapY = 0;
        for(int y = 0; y < m_grid.getHeight(); ++y)
        {
            
            // TODO remove debug
            // This prints the row offset
            int val = (m_grid.getOffset() + y) % m_grid.getHeight();
            if(val < 0) val += m_grid.getHeight();
            canvas.drawText(String.valueOf(val), 
                          (float)(m_gridStartWidth - 15), 
                          (float)(adjustedHeight + (m_grid.getHeight() - 1 - y) * image.getHeight() + image.getHeight() / 2),
                          debugPaint);
            // TODO end remove debug
            
            for(int x = 0; x < m_grid.getWidth(); ++x)
            {
                curr = m_grid.getValue(x, y);
                if(curr.getId() != Block.EMPTY_ID)
                {
                    
                    if(curr.getState() == Block.State.CLEARING)
                    {
                        Timer t = curr.getTimer();
                        long remaining = t.getRemainingTime(currentTime);
                        if(remaining > 0)
                        {
                            blockPaint.setAlpha((int)(255.0F * ((float)remaining) / ((float)t.getInterval())));
                        }
                        else
                        {
                            blockPaint.setAlpha(0);
                        }

                        canvas.drawText(String.valueOf(curr.getBlockGroup().getChainCount()), 
                                        (float)(m_gridStartWidth + x * image.getWidth() + 25), 
                                        (float)(adjustedHeight + (m_grid.getHeight() - 1 - y) * image.getHeight() + image.getHeight() / 2),
                                        debugPaint);
                    }
                    else
                    {
                        blockPaint.setAlpha(255);
                    }
                    
                    if(curr == m_source)
                    {
                        // Just set it up for rendering later. We do this so the swapping block is always 
                        // rendered on top of all other blocks.
                        swap = curr;
                        // Need to make sure the block can't be moved outside of the play area
                        swapX = m_gridStartWidth + x * image.getWidth() + (m_distance);
                        swapY = adjustedHeight + (m_grid.getHeight() - 1 - y) * image.getHeight();
                    }
                    else if(curr == m_dest)
                    {
                        // Render the swapping destination block. It should move towards the
                        // swapping block as the swapping block moves towards it, and vice-versa
                        canvas.drawBitmap(m_bitmaps[m_dest.getId() - 1], 
                                (float)(m_gridStartWidth + x * image.getWidth() - (m_distance)),
                                (float)(adjustedHeight + (m_grid.getHeight() - 1 - y) * image.getHeight()),
                                p);
                    }
                    else if(curr.getState() == Block.State.FALLING)
                    {
                        // Render it according to the percentage of the fall that is left
                        float percentLeft = (float)curr.getTimer().getRemainingTime(currentTime) / (float)curr.getTimer().getInterval();

                        canvas.drawBitmap(m_bitmaps[curr.getId() - 1], 
                               (float)(m_gridStartWidth + x * image.getWidth()),
                               (float)(adjustedHeight + (m_grid.getHeight() - 1 - y) * image.getHeight() - (percentLeft*image.getHeight())),
                               p);
                    }
                    else
                    {
                        // Subtract 1 from the id to convert to 0-based index
                        canvas.drawBitmap(m_bitmaps[curr.getId() - 1], 
                               (float)(m_gridStartWidth + x * image.getWidth()),
                               (float)(adjustedHeight + (m_grid.getHeight() - 1 - y) * image.getHeight()),
                               p);
                        
//                        if(curr.getState() == Block.State.CLEARING)
//                        {
//                            canvas.drawCircle((float)(m_gridStartWidth + x * image.getWidth() + image.getWidth() / 2), 
//                                              (float)(adjustedHeight + (m_grid.getHeight() - 1 - y) * image.getHeight() + image.getHeight() / 2),
//                                              image.getWidth() / 8, 
//                                              p);
//                        }
                    }
                }
                
                // TODO remove debug
                // This section prints out the state. Useful for debugging various issues
//                char[] chars = new char[] {'N', 'C', 'F', 'S'};
//                String s = String.valueOf(chars[curr.getState().ordinal()]);
//                canvas.drawText(s, 
//                                (float)(m_gridStartWidth + x * image.getWidth() + image.getWidth() / 2), 
//                                (float)(adjustedHeight + (m_grid.getHeight() - 1 - y) * image.getHeight() + image.getHeight() / 2),
//                                debugPaint);
                
                // This section prints out the block group. Useful for debugging combo/chain issues
                if(curr.getBlockGroup() != null)
                {
                    canvas.drawText(String.valueOf(curr.getBlockGroup().getId()), 
                                    (float)(m_gridStartWidth + x * image.getWidth() + 2), 
                                    (float)(adjustedHeight + (m_grid.getHeight() - 1 - y) * image.getHeight() + image.getHeight() / 2),
                                    debugPaint);
                }
                
                // This section prints out the timer info
//                if(curr.getTimer() != null)
//                {
//                    long rem = curr.getTimer().getRemainingTime(currentTime);
//                    //if(rem < 0) rem = 0;
//                    canvas.drawText(String.valueOf(rem), 
//                                    (float)(m_gridStartWidth + x * image.getWidth() + 2), 
//                                    (float)(adjustedHeight + (m_grid.getHeight() - 1 - y) * image.getHeight() + image.getHeight() / 2),
//                                    debugPaint);
//                }
                
                // This section prints out part of the hash value
//                canvas.drawText(String.valueOf(curr.hashCode() & 0xFF), 
//                                (float)(m_gridStartWidth + x * image.getWidth() + image.getWidth() / 2), 
//                                (float)(adjustedHeight + (m_grid.getHeight() - 1 - y) * image.getHeight() + image.getHeight() / 2),
//                                debugPaint);
                // TODO end remove debug
            }
            
            if(y == 0)
            {
                p = blockPaint;
            }
        }
        
        if(swap != null)
        {
            canvas.drawBitmap(m_bitmaps[swap.getId() - 1], 
                    (float)(swapX),
                    (float)(swapY),
                    p);
            
            // TODO remove debug
//            char[] chars = new char[] {'N', 'C', 'F', 'S'};
//            String s = String.valueOf(chars[swap.getState().ordinal()]);
//            canvas.drawText(s, 
//                    (float)(swapX + image.getWidth() / 2), 
//                    (float)(swapY + image.getHeight() / 2),
//                    debugPaint);
        }
    }

    @Override
    public void setRenderBounds(int xLeft, int xRight, int yTop, int yBottom) throws IllegalArgumentException
    {
        if(xRight < xLeft || yBottom < yTop)
        {
            // TODO externalize string
            throw new IllegalArgumentException("xRight >= xLeft && yBottom >= yTop == false");
        }
        
        m_xLeft = xLeft;
        m_xRight = xRight;
        m_yTop = yTop;
        m_yBottom = yBottom;

        // don't forget to resize the background image
        m_backgroundImage = Bitmap.createScaledBitmap(
                m_backgroundImage, m_xRight - m_xLeft, m_yBottom - m_yTop, true);

        // Recalculate starting bounds
        m_gridStartWidth = m_xLeft + (m_xRight - m_xLeft) / 2 - (m_bitmaps[0].getWidth() * m_grid.getWidth()) / 2;
        m_gridStartHeight = (m_yBottom - m_yTop) / 2 - (m_bitmaps[0].getHeight() * m_grid.getHeight()) / 2;
        
        // TODO remove debug
        // This makes the play area fullscreen
//        Resources res = m_context.getResources();
//        m_gridStartWidth = 0;
//        m_gridStartHeight = 0;
//        int newW = (m_xRight - m_xLeft) / m_grid.getWidth();
//        int newH = (m_yBottom - m_yTop) / m_grid.getHeight();
//        if(newW < newH)
//        {
//            newH = newW;
//            m_gridStartHeight = (m_yBottom - m_yTop - (newH * m_grid.getHeight())) / 2;
//        }
//        else
//        {
//            newW = newH;
//            m_gridStartWidth = (m_xRight - m_xLeft - (newW * m_grid.getWidth())) / 2;
//        }
//        m_bitmaps = new Bitmap[]{
//            Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, R.drawable.orange_block_hdpi),newW,newH,false),
//            Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, R.drawable.blue_block_hdpi),newW,newH,false),
//            Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, R.drawable.red_block_hdpi),newW,newH,false),
//            Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, R.drawable.green_block_hdpi),newW,newH,false),
//            Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, R.drawable.purple_block_hdpi),newW,newH,false),
//            Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, R.drawable.yellow_block_hdpi),newW,newH,false)
//        };
        // TODO end remove debug
    }

    @Override
    public Point getBlockAtLocation(int x, int y)
    {
        Point p = null;
        int adjustedHeight = m_gridStartHeight + (int)((m_rowTimer.getRemainingTime(null)) * m_bitmaps[0].getHeight() / m_rowTimer.getInterval());
        
        int xIdx = -1;
        if(x >= m_gridStartWidth)
        {
            xIdx = (x - m_gridStartWidth) / m_bitmaps[0].getWidth();
        }
        
        int yIdx = m_grid.getHeight() - ((y - adjustedHeight) / m_bitmaps[0].getHeight()) - 1;
        
        if(xIdx >= 0 && xIdx < m_grid.getWidth() && yIdx >= 0 && yIdx < m_grid.getHeight())
        {
            p = new Point(xIdx, yIdx);
        }
        
        return p;
    }
    
    public int getBlockWidth()
    {
        return m_bitmaps[0].getWidth();
    }

    @Override
    public void onLossWarningChanged(LossWarningState state)
    {
        if(state == LossWarningState.ON)
        {
            m_lossWarningTimer = new Timer(1000, m_activity.getCurrTime());
        }
        else
        {
            m_lossWarningTimer = null;
        }
    }

    @Override
    public void onSwap(SwapEvent event, GameController source)
    {
        m_source = event.getSource();
        m_dest = event.getDest();
        m_distance = event.getDist();
    }
}
