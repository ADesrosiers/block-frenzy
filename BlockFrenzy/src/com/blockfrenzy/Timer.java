package com.blockfrenzy;

/**
 * @author Aaron
 *
 */
public class Timer 
{
    /**
     * The time the timer started at
     */
    protected long m_start;
    
    /**
     * Amount of time the timer is set to
     */
    protected long m_interval;
    
    /**
     * This constructor creates a new Timer object initialized with the interval given. If the 
     * start parameter is non-null, it is used as the start time, otherwise System.currentTimeMillis()
     * is used.
     * @param interval The interval this timer represents
     * @param start The optional parameter indicating the start time.
     */
    public Timer(long interval, Long start) throws IllegalArgumentException
    {
        m_interval = interval;
        if(start != null)
        {
            if(start < 0) throw new IllegalArgumentException("start time must be > 0");
            m_start = start;
        }
        else
        {
            m_start = System.currentTimeMillis();
        }
    }
    
    public Timer(Timer src)
    {
        this.m_interval = src.m_interval;
        this.m_start = src.m_start;
    }
    
    public long getInterval() 
    {
        return m_interval;
    }
    
    
    /**
     * Checks if the timer has elapsed. The calculation will use currentTime if it is non-null,
     * otherwise System.currentTimeMillis() will be used.
     * @param currentTime The optional parameter indicating the current time
     * @return The time remaining (will be negative if timer elapsed)
     */
    public long getRemainingTime(Long currentTime) throws IllegalArgumentException
    {
        long remaining = 0;
        if(currentTime != null)
        {
            if(currentTime < 0) throw new IllegalArgumentException("currentTime must be > 0");
            remaining = m_interval - (currentTime - m_start);
        }
        else
        {
            remaining = m_interval - (System.currentTimeMillis() - m_start);
        }
        return remaining;
    }
    
    public long getStartTime()
    {
        return m_start;
    }
    
    /**
     * This resets the timer. If the interval parameter is non-null, the Timer object's 
     * interval will be set to this value. Otherwise it will remain unchanged. If the start is
     * non-null, it will be used instead of a call to System.currentTimeMillis()
     * @param interval The new interval for the Timer object to use
     * @param start The optional start value to use
     */
    public void reset(Long interval, Long start) throws IllegalArgumentException
    {
        if(interval != null)
        {
            if(interval < 0) throw new IllegalArgumentException("interval time must be > 0");
            m_interval = interval;
        }
        
        if(start != null)
        {
            if(start < 0) throw new IllegalArgumentException("start time must be > 0");
            m_start = start;
        }
        else
        {
            m_start = System.currentTimeMillis();
        }
    }

    
}
