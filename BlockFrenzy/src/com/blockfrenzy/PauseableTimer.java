package com.blockfrenzy;

public class PauseableTimer extends Timer
{
    /*
     * The time the pause occurred.
     */
    protected long m_pauseTime;
    
    public PauseableTimer(long interval, Long start)
    {
        super(interval, start);
        m_pauseTime = 0;
    }
    
    public PauseableTimer(PauseableTimer src)
    {
        super(src);
        m_pauseTime = src.m_pauseTime;
    }
    
    public void pause(Long currentTime)
    {
        if(currentTime != null)
        {
            m_pauseTime = currentTime;
        }
        else
        {
            m_pauseTime = System.currentTimeMillis();
        }
    }
    
    public void unpause(Long currentTime)
    {
        if(currentTime != null)
        {
            // Update the start time
            m_start += currentTime - m_pauseTime;
        }
        else
        {
            // Update the start time
            m_start += System.currentTimeMillis() - m_pauseTime;
        }
        
        // Make sure to indicate that the timer is no longer paused
        m_pauseTime = 0;
    }
    
    public long getRemainingTime(Long currentTime) throws IllegalArgumentException
    {
        // If we are not paused, just use the base class getRemainingTime(). Otherwise, take the 
        // time paused into consideration.
        long remaining = 0;
        if(m_pauseTime == 0)
        {
            remaining = super.getRemainingTime(currentTime);
        }
        else
        {
            remaining = m_interval - (m_pauseTime - m_start);
        }
        
        return remaining;
    }
    
    public void reset(Long interval, Long start) throws IllegalArgumentException
    {
        // Call base class reset
        super.reset(interval, start);
        
        // Reset the pause
        m_pauseTime = 0;
    }
}
