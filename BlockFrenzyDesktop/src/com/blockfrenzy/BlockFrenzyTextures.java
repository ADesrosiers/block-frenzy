package com.blockfrenzy;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Logger;


public class BlockFrenzyTextures implements BlockFrenzySounds
{
	private static final String ASSETS_DIR = "data/";
    private static final String IMAGE_DIR = ASSETS_DIR + "packed/pack.atlas";
    private static final String BACKGROUND = "bg-moonscape";
    private static final String MOONSCAPE_ASTEROID = "bg-moonscape-asteroid";
    private static final String BUTTON = "button";
    private static final String BOUNDARY = "boundary";
    private static final String CIRCLE = "circle";
    private static final String CIRCLE_EDGE = "circle-edge";
    private static final String BLOCK_T_EDGE = "block-t-edge";
    private static final String BLOCK_L_EDGE = "block-l-edge";
    private static final String BLOCK_R_EDGE = "block-r-edge";
    private static final String BLOCK_B_EDGE = "block-b-edge";
    private static final String BLOCK_GLASS = "block-glass";

    private static final String BROKEN_TL_STRING = "-broken-tl";
    private static final String BROKEN_TR_STRING = "-broken-tr";
    private static final String BROKEN_BL_STRING = "-broken-bl";
    private static final String BROKEN_BR_STRING = "-broken-br";
    private static final String GLASS_GLARE = "glass-glare";
    private static final String CIRCLE_GLARE = "circle-glare";

    private static final String FONT_GAMEPLAY = ASSETS_DIR + "Gameplay.fnt";
    private static final String FONT_TITLE = ASSETS_DIR + "BlockFrenzy.fnt";
    private static final String FONT_CREDITS = ASSETS_DIR + "credits.fnt";
    private static final String FONT_INSTRUCTIONS = ASSETS_DIR + "instructions.fnt";
    private static final String WAV_SWAP = ASSETS_DIR + "swap.wav";
    private static final String WAV_CLEAR = ASSETS_DIR + "clear.wav";
    private static final String WAV_WARNING = ASSETS_DIR + "warning.wav";
    private static final String WAV_MATCH = ASSETS_DIR + "match.wav";
    private static final String WAV_COMBO = ASSETS_DIR + "combo.wav";
    
    private static final String SPARK_EFFECT = ASSETS_DIR + "Spark.effect";
    private static final String BG_EFFECT = ASSETS_DIR + "bg.effect";
    
    private TextureRegion m_topLeft;
    private TextureRegion m_topRight;
    private TextureRegion m_bottomRight;
    private TextureRegion m_bottomLeft;
    
    private NinePatch m_button;
    private NinePatch m_boundary;
    private TextureRegion m_circle;
    private TextureRegion m_circleEdge;
    private TextureRegion m_background;
    private TextureRegion m_moonscapeAsteroid;
    private TextureRegion m_blockTEdge;
    private TextureRegion m_blockLEdge;
    private TextureRegion m_blockREdge;
    private TextureRegion m_blockBEdge;
    private TextureRegion m_blockGlass;
    private TextureRegion m_glassGlare;
    private TextureRegion m_circleGlare;
    private ExplodingPiecePool m_explodingPiecePool;
    
    private ParticleEffectPool m_sparkEffectPool;
    private ParticleEffect 	   m_bgEffect;
    
    private AssetManager m_manager;
    private BitmapFont m_font;
    private BitmapFont m_titleFont;
    private BitmapFont m_creditsFont;
    private BitmapFont m_instructionsFont;
    private Sound m_swapPlayer;
    private Sound m_clearPlayer;
    private Sound m_warningPlayer;
    private Sound m_matchPlayer;
    private Sound m_comboPlayer;
    
    private boolean m_isLoaded = false;
    
    private Logger 	m_logger = new Logger(BlockFrenzyTextures.class.getName(), Logger.DEBUG);
    
    private static BlockFrenzyTextures m_instance = null;
    
    private BlockFrenzyTextures()
    {
    	m_logger.debug("BlockFrenzyTextures()");
        // Load all the assets
    	m_manager = new AssetManager();
    	initialize();
    }

    public static BlockFrenzyTextures getInstance()
    {
    	if(m_instance == null)
    	{
    		m_instance = new BlockFrenzyTextures();
    	}
    	return m_instance;
    }
    
    public void initialize()
    {
    	m_logger.debug("initialize()");
    	
    	if(m_isLoaded)
    	{
    		return;
    	}
    	
    	m_logger.debug("initializing..");

    	m_manager.load(IMAGE_DIR, TextureAtlas.class);
    	m_manager.load(FONT_GAMEPLAY, BitmapFont.class);
    	m_manager.load(FONT_TITLE, BitmapFont.class);
    	m_manager.load(FONT_CREDITS, BitmapFont.class);
    	m_manager.load(FONT_INSTRUCTIONS, BitmapFont.class);
    	m_manager.load(WAV_SWAP, Sound.class);
    	m_manager.load(WAV_CLEAR, Sound.class);
    	m_manager.load(WAV_WARNING, Sound.class);
    	m_manager.load(WAV_MATCH, Sound.class);
    	m_manager.load(WAV_COMBO, Sound.class);
    	
    	// Actually perform the load
    	m_manager.finishLoading();

        TextureAtlas atlas = m_manager.get(IMAGE_DIR, TextureAtlas.class);
        
        m_button = atlas.createPatch(BUTTON);
        m_boundary = atlas.createPatch(BOUNDARY);
        
        m_topLeft = atlas.createSprite("block" + BROKEN_TL_STRING);
        m_topRight = atlas.createSprite("block" + BROKEN_TR_STRING);
        m_bottomRight = atlas.createSprite("block" + BROKEN_BR_STRING);
        m_bottomLeft = atlas.createSprite("block" + BROKEN_BL_STRING);

        m_circle = atlas.createSprite(CIRCLE);
        m_circleEdge = atlas.createSprite(CIRCLE_EDGE);
        
        m_background = atlas.createSprite(BACKGROUND);
        m_moonscapeAsteroid = atlas.createSprite(MOONSCAPE_ASTEROID);
        
        m_blockTEdge = atlas.createSprite(BLOCK_T_EDGE);
        m_blockBEdge = atlas.createSprite(BLOCK_B_EDGE);
        m_blockLEdge = atlas.createSprite(BLOCK_L_EDGE);
        m_blockREdge = atlas.createSprite(BLOCK_R_EDGE);
        m_blockGlass = atlas.createSprite(BLOCK_GLASS);
        
        m_glassGlare = atlas.createSprite(GLASS_GLARE);
        m_circleGlare = atlas.createSprite(CIRCLE_GLARE);
        
        ParticleEffect sparkEffect = new ParticleEffect();
        sparkEffect.load(Gdx.files.internal(SPARK_EFFECT), atlas);
        
        m_bgEffect = new ParticleEffect();
        m_bgEffect.load(Gdx.files.internal(BG_EFFECT), atlas);
        
        m_sparkEffectPool = new ParticleEffectPool(sparkEffect, 4, 100);
        m_explodingPiecePool = new ExplodingPiecePool(80, 500);
        
        m_font = m_manager.get(FONT_GAMEPLAY, BitmapFont.class);
        m_font.setFixedWidthGlyphs("0123456789");
        m_font.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
        
        m_titleFont = m_manager.get(FONT_TITLE, BitmapFont.class);
        
        m_creditsFont = m_manager.get(FONT_CREDITS, BitmapFont.class);
        m_creditsFont.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
        
        m_instructionsFont = m_manager.get(FONT_INSTRUCTIONS, BitmapFont.class);
        m_instructionsFont.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
        
        m_swapPlayer = m_manager.get(WAV_SWAP, Sound.class);
        m_clearPlayer = m_manager.get(WAV_CLEAR, Sound.class);
        m_warningPlayer = m_manager.get(WAV_WARNING, Sound.class);
        m_matchPlayer = m_manager.get(WAV_MATCH, Sound.class);
        m_comboPlayer = m_manager.get(WAV_COMBO, Sound.class);

        m_isLoaded = true;
    	m_logger.debug("finished initializing");
    }
    
    public void terminate()
    {
    	m_logger.debug("terminate()");
    	if(m_isLoaded == false)
    	{
    		return;
    	}

    	m_logger.debug("terminating..");
    	
    	m_manager.clear();

        m_button = null;
        
        m_topLeft = null;
        m_topRight = null;
        m_bottomRight = null;
        m_bottomLeft = null;

        m_circle = null;
        m_circleEdge = null;
        
        m_background = null;
        m_moonscapeAsteroid = null;
        
        m_blockTEdge = null;
        m_blockBEdge = null;
        m_blockLEdge = null;
        m_blockREdge = null;
        m_blockGlass = null;
        
        m_sparkEffectPool = null;
        m_explodingPiecePool = null;
        
        m_font = null;
        m_titleFont = null;
        m_swapPlayer = null;
        m_clearPlayer = null;
        m_warningPlayer = null;
        m_matchPlayer = null;
        m_comboPlayer = null;
        
        m_isLoaded = false;
    	m_logger.debug("finished terminating");
    }

    public ParticleEffect getBgEffect()
    {
		return m_bgEffect;
	}
    
    public TextureRegion getGlassGlare()
    {
		return m_glassGlare;
	}

    public TextureRegion getCircleGlare()
    {
		return m_circleGlare;
	}
    
    public BitmapFont getGameplayFont()
    {
    	return m_font;
    }

    public BitmapFont getTitleFont()
    {
    	return m_titleFont;
    }

    public BitmapFont getCreditsFont()
    {
    	return m_creditsFont;
    }
    
    public BitmapFont getInstructionsFont()
    {
    	return m_instructionsFont;
    }
    
    public ParticleEffectPool getSparkEffectPool()
    {
    	return m_sparkEffectPool;
    }
    
    public ExplodingPiecePool getExplodingPiecePool()
    {
    	return m_explodingPiecePool;
    }
    
    public TextureRegion getCircleTexture()
    {
        return m_circle;
    }
    
    public TextureRegion getCircleEdgeTexture()
    {
        return m_circleEdge;
    }
    
    public TextureRegion getBackgroundTexture()
    {
        return m_background;
    }
    
    public NinePatch getButtonPatch()
    {
        return m_button;
    }
    
    public NinePatch getBoundaryPatch()
    {
        return m_boundary;
    }
    
    public TextureRegion getMoonscapeAsteroid()
    {
    	return m_moonscapeAsteroid;
    }
    
    public TextureRegion getBlockTEdge()
    {
    	return m_blockTEdge;
    }
    
    public TextureRegion getBlockBEdge()
    {
    	return m_blockBEdge;
    }
    
    public TextureRegion getBlockLEdge()
    {
    	return m_blockLEdge;
    }
    
    public TextureRegion getBlockREdge()
    {
    	return m_blockREdge;
    }
    
    public TextureRegion getBlockGlass()
    {
    	return m_blockGlass;
    }
    
    public TextureRegion getTopLeft()
    {
    	return m_topLeft;
    }
    
    public TextureRegion getTopRight()
    {
    	return m_topRight;
    }
    
    public TextureRegion getBottomRight()
    {
    	return m_bottomRight;
    }
    
    public TextureRegion getBottomLeft()
    {
    	return m_bottomLeft;
    }

	@Override
	public void playSwap()
	{
		m_swapPlayer.play();
	}

	@Override
	public void playClear()
	{
		m_clearPlayer.play();
	}

	@Override
	public void playWarning()
	{
		m_warningPlayer.play();
	}

	@Override
	public void playMatch()
	{
		m_matchPlayer.play();
	}

	@Override
	public void playCombo()
	{
		m_comboPlayer.play();
	}
}
