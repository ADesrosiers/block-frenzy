package com.blockfrenzy;

import java.util.Vector;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;
import static com.badlogic.gdx.math.Interpolation.*;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.Logger;
import com.blockfrenzy.BlockFrenzy.GameState;

public class SelectWindow extends Group
{
    private static final float MOVING_INTERVAL = 0.5F;
    private static final float BASE_DELAY = 1.0F;
    private static final float DELTA_DELAY = 0.3F;
    private static final float BUTTON_BUFFER = 0.078125F;
    private static final float BUTTON_START_Y = 0.85F;
    private static final float BUTTON_PAD = 50.0F;
    
    // TODO i18n-ize these
    private static final String NEW_GAME_STRING = "New game";
    private static final String RESUME_GAME_STRING = "Resume game";
    private static final String HIGH_SCORES_STRING = "High scores";
    private static final String BLOCK_FRENZY_STRING = "Block Frenzy";
    private static final String INSTRUCTIONS_STRING = "Instructions";
    
    private Logger m_logger = new Logger(SelectWindow.class.getSimpleName());
    private Vector<Actor> m_actors;
    private SequenceAction m_tearDownAction;
    private BlockFrenzyGame m_game;
    
    private Button m_resumeGameButton;
    private Button m_highScoresButton;
    private Button m_newGameButton;
    private Button m_creditsButton;
    private Button m_instructionsButton;
    private NinePatch m_buttonPatch = BlockFrenzyTextures.getInstance().getButtonPatch();
    
    public SelectWindow(BlockFrenzyGame game)
    {    	
    	m_logger.setLevel(Logger.DEBUG);
    	m_game = game;

        BitmapFont font = BlockFrenzyTextures.getInstance().getTitleFont();
        TextButtonStyle textButtonStyle = new TextButtonStyle();
        textButtonStyle.up = new NinePatchDrawable(m_buttonPatch);
        textButtonStyle.font = BlockFrenzyTextures.getInstance().getGameplayFont();
        textButtonStyle.fontColor = Color.WHITE;
        textButtonStyle.downFontColor = Color.GRAY;

        m_newGameButton = new TextButton(NEW_GAME_STRING, textButtonStyle);
        m_newGameButton.setWidth(m_newGameButton.getWidth() + BUTTON_PAD);
        m_newGameButton.setHeight(m_newGameButton.getHeight() + BUTTON_PAD);
        
        NinePatch buttonPatchRed = new NinePatch(m_buttonPatch);
        TextButtonStyle textButtonStyleRed = new TextButtonStyle();
        textButtonStyleRed.up = new NinePatchDrawable(buttonPatchRed);
        textButtonStyleRed.font = BlockFrenzyTextures.getInstance().getGameplayFont();
        textButtonStyleRed.fontColor = Color.WHITE;
        textButtonStyleRed.downFontColor = Color.GRAY;
        
        m_resumeGameButton = new TextButton(RESUME_GAME_STRING, textButtonStyleRed);
        m_resumeGameButton.setWidth(m_resumeGameButton.getWidth() + BUTTON_PAD);
        m_resumeGameButton.setHeight(m_resumeGameButton.getHeight() + BUTTON_PAD);

        m_highScoresButton = new TextButton(HIGH_SCORES_STRING, textButtonStyleRed);
        m_highScoresButton.setWidth(m_highScoresButton.getWidth() + BUTTON_PAD);
        m_highScoresButton.setHeight(m_highScoresButton.getHeight() + BUTTON_PAD);
        
        m_creditsButton = new TextButton(BlockFrenzyCredits.CREDITS_STRING, textButtonStyleRed);
        m_creditsButton.setWidth(m_creditsButton.getWidth() + BUTTON_PAD);
        m_creditsButton.setHeight(m_creditsButton.getHeight() + BUTTON_PAD);
        
        m_instructionsButton = new TextButton(INSTRUCTIONS_STRING, textButtonStyleRed);
        m_instructionsButton.setWidth(m_instructionsButton.getWidth() + BUTTON_PAD);
        m_instructionsButton.setHeight(m_instructionsButton.getHeight() + BUTTON_PAD);
        
        Label title = new Label(BLOCK_FRENZY_STRING, new LabelStyle(font, Color.WHITE));
        
        m_actors = new Vector<Actor>();

		m_actors.add(title);

		m_actors.add(m_resumeGameButton);
        m_resumeGameButton.addListener(new ClickListener()
        {
        	@Override
        	public void clicked(InputEvent event, float x, float y)
        	{
                m_logger.debug("Resume button clicked()");

                tearDownButtons();
                m_tearDownAction.addAction(run(new Runnable() {
					
					@Override
					public void run()
					{
                        m_game.setScreen(m_game.getBlockFrenzy());
                    }
                }));
            }
        });
        
		m_actors.add(m_newGameButton);
        m_newGameButton.addListener(new ClickListener()
        {
        	@Override
        	public void clicked(InputEvent event, float x, float y)
        	{
                m_logger.debug("New button clicked()");
                
                tearDownButtons();
                m_tearDownAction.addAction(run(new Runnable() {
					
					@Override
					public void run()
					{
                    	m_game.getBlockFrenzy().reset();
                        m_game.setScreen(m_game.getBlockFrenzy());
					}
				}));
        	}
		});

		m_actors.add(m_highScoresButton);
        m_highScoresButton.addListener(new ClickListener()
        {
        	@Override
        	public void clicked(InputEvent event, float x, float y)
        	{
                m_logger.debug("HighScores button clicked()");

                tearDownButtons();
                m_tearDownAction.addAction(run(new Runnable() {
					
					@Override
					public void run()
					{
                        m_game.setScreen(m_game.getScoresScreen());
                    }
                }));
            }
        });

		m_actors.add(m_creditsButton);
        m_creditsButton.addListener(new ClickListener()
        {
        	@Override
        	public void clicked(InputEvent event, float x, float y)
        	{
                m_logger.debug("Credits button clicked()");

                tearDownButtons();
                m_tearDownAction.addAction(run(new Runnable() {
					
					@Override
					public void run()
					{
                        m_game.setScreen(m_game.getCreditsScreen());
                    }
                }));
            }
        });

		m_actors.add(m_instructionsButton);
        m_instructionsButton.addListener(new ClickListener()
        {
        	@Override
        	public void clicked(InputEvent event, float x, float y)
        	{
                m_logger.debug("Credits button clicked()");

                tearDownButtons();
                m_tearDownAction.addAction(run(new Runnable() {
					
					@Override
					public void run()
					{
                        m_game.setScreen(m_game.getInstructionsScreen());
                    }
                }));
            }
        });
    }
    
    public void setupButtons()
    {
    	m_logger.debug("setupButtons");
    	
    	for(Actor actor : m_actors)
    	{
    		actor.remove();
    	}
    	
    	// Check if the resume button should be included
    	if(m_game.getGameState() != GameState.Lost)
    	{
    		if(m_actors.contains(m_resumeGameButton) == false)
    		{
    			m_actors.add(1, m_resumeGameButton);
    		}
    	}
    	else
    	{
    		m_actors.remove(m_resumeGameButton);
    	}
    	
        float currButtonY = this.getHeight()*BUTTON_START_Y - BUTTON_START_Y;
        for(int actorNo = 0; actorNo < m_actors.size(); ++actorNo)
        {
            Actor actor = m_actors.get(actorNo);

            currButtonY -= (actor.getHeight() + BUTTON_BUFFER * m_highScoresButton.getHeight());
            
            actor.setX(this.getWidth());
            actor.setY(currButtonY);
            
            MoveToAction buttonAction;
            buttonAction = moveTo(this.getWidth() / 2.0F - actor.getWidth() / 2.0F, 
                    currButtonY,
                    MOVING_INTERVAL);
            buttonAction.setInterpolation(swingOut);
            actor.addAction(sequence(delay(BASE_DELAY + DELTA_DELAY*actorNo), buttonAction));
            this.addActor(actor);
        }
    }
    
    private void tearDownButtons()
    {
    	m_logger.debug("tearDownButtons");
    	
        for(int actorNo = m_actors.size() - 1; actorNo >= 0; --actorNo)
        {
            Actor actor = m_actors.get(actorNo);
        	
            MoveToAction buttonAction;
            buttonAction = moveTo(
            		actor.getX(), 
                    -m_highScoresButton.getHeight(),
                    MOVING_INTERVAL);
            buttonAction.setInterpolation(pow4In);
            SequenceAction sequence = sequence(delay(DELTA_DELAY*(m_actors.size() - 1 - actorNo)), 
            								   buttonAction);
            actor.addAction(sequence);
            
            if(actorNo == 0)
            {
                m_tearDownAction = sequence;
            }
        }
    }
}
