package com.blockfrenzy;

import java.util.HashMap;
import java.util.Vector;

public class BlockGroupFactory
{
    private static BlockGroupFactory m_instance = new BlockGroupFactory();
    private int m_currGroupId = 0;
    private HashMap<Integer, BlockGroup> m_setupMap = new HashMap<Integer, BlockGroupFactory.BlockGroup>(61);
    
    private BlockGroupFactory()
    {
    }
    
    public static BlockGroupFactory getInstance()
    {
        return m_instance;
    }
    
    public void setCurrGroupId(int currGroupId)
    {
        m_currGroupId = currGroupId;
    }
    
    public int getCurrGroupId()
    {
        return m_currGroupId;
    }
    
    public HashMap<Integer, BlockGroup> getSetupMap()
    {
        return m_setupMap;
    }
    
    public BlockGroup createBlockGroup()
    {
        BlockGroup newGroup = new BlockGroup(m_currGroupId);
        ++m_currGroupId;
        return newGroup;
    }

    public static class BlockGroup extends Vector<Block>
    {
        private int m_id;
        private int m_chainCount = 0;
        private Point m_location = new Point(0, 0);
        
        /*
         * For serializable
         */
        public BlockGroup()
        {
        }
        
        public BlockGroup(int id)
        {
            m_id = id;
        }
        
        public int getId()
        {
            return m_id;
        }
        
        public void incrementChain()
        {
            ++m_chainCount;
        }
        
        public int getChainCount()
        {
            return m_chainCount;
        }
        
        public void setChainCount(int chainCount)
        {
            m_chainCount = chainCount;
        }
        
        public void setLocation(Point location)
        {
            m_location.x = location.x;
            m_location.y = location.y;
        }
        
        public Point getLocation()
        {
            return m_location;
        }
        
        @Override
        public synchronized boolean equals(Object object)
        {
            boolean isEqual = false;
            
            if(object != null && object instanceof BlockGroup && ((BlockGroup) object).getId() == this.getId())
            {
                isEqual = true;
            }
            
            return isEqual;
        }
    
        /**
         * 
         */
        private static final long serialVersionUID = 7169499070388094095L;
    }
}
