package com.blockfrenzy;

import java.io.IOException;
import java.io.OutputStream;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Base64Coder;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class BlockFrenzyIO 
{
	public static int write(FileHandle handle, String str)
	{
		try
		{
		    OutputStream os = handle.write(false);

		    String text = str;
		    
		    if(BlockFrenzyGame.DEBUG_MODE == false && text != null && text.isEmpty() == false)
		    {
		    	text = Base64Coder.encodeString(text);
		    }
		    
		    os.write(text.getBytes());
		    os.close();
		} 
		catch (IOException ex) 
		{
		    Gdx.app.log("BlockFrenzyIO", "File write error: " + ex.toString());
		}
		return 0;
	}

    public static String read(FileHandle handle)
    {
        String text = "";
        try
        {
        	text = handle.readString();

	        if((handle.type() == FileType.Absolute || handle.type() == FileType.External) && 
	        		BlockFrenzyGame.DEBUG_MODE == false && text != null && text.isEmpty() == false)
	        {
	        	text = Base64Coder.decodeString(text);
	        }
        }
        catch (GdxRuntimeException exc)
        {
            // Do nothing, just return empty string
        }
        
        return text;
    }
}
