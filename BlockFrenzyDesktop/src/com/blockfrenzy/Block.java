package com.blockfrenzy;

import static com.badlogic.gdx.math.Interpolation.*;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

import static com.blockfrenzy.actions.BlockFrenzyInterpolation.*;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool.PooledEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Json.Serializable;
import com.badlogic.gdx.utils.Logger;
import com.badlogic.gdx.utils.OrderedMap;
import com.blockfrenzy.BlockGroupFactory.BlockGroup;

public class Block extends Group implements Serializable
{
	private static final float EXPLODE_Y = 3.125F;
	private static final float EXPLODE_X = 4.6875F;
	
	private static final float[] ANGLES = {
		(float)Math.toRadians(135.0),
		(float)Math.toRadians(45.0),
		(float)Math.toRadians(35.0),
		(float)Math.toRadians(125.0)};
    private static final float DURATION = 1.0F;
    public static final int EMPTY_ID = 0;
    
    public enum State {NORMAL, CLEARING, FALLING, SWAPPING};
    
    // Used to convert from int back to State enum (from json)
    private static final State[] STATES = {State.NORMAL, State.CLEARING, State.FALLING, State.SWAPPING};
    
    private int m_id = EMPTY_ID;
    private State m_state = State.NORMAL;
    private Timer m_timer = null;
    private BlockGroup m_blockGroup = null;
    private boolean m_isExploding = false;
    private Logger m_logger = new Logger(this.getClass().getSimpleName(), Logger.DEBUG);

    // Should only be used by Json reader
    public Block()
    {
		TextureRegion blockGlassRegion = BlockFrenzyTextures.getInstance().getBlockGlass();
		this.setWidth(blockGlassRegion.getRegionHeight());
		this.setHeight(blockGlassRegion.getRegionHeight());
    }
    
    public Block(int id)
    {
    	this();
        m_id = id;
    }
    
    public Block(Block src)
    {
        m_id = src.m_id;
        m_state = src.m_state;
        m_timer = src.m_timer;
        m_blockGroup = src.m_blockGroup;
        setWidth(src.getWidth());
        setHeight(src.getHeight());
    }
    
    public int getId() 
    {
        return m_id;
    }
    
    public void setId(int m_id)
    {
		this.m_id = m_id;
	}

    public State getState() 
    {
        return m_state;
    }
    
    public void setState(State state) 
    {
        this.m_state = state;
    }
    
    public Timer getTimer()
    {
        return m_timer;
    }
    
    public void setTimer(Timer timer)
    {
        if(timer != null)
        {
            this.m_timer = new Timer(timer);
        }
        else
        {
            m_timer = null;
        }
    }
    
    public void setBlockGroup(BlockGroup blockGroup)
    {
        this.m_blockGroup = blockGroup;
    }
    
    public BlockGroup getBlockGroup()
    {
        return m_blockGroup;
    }

    @Override
    public void write(Json json)
    {
        // capture block.id
        json.writeValue("id", m_id);
        
        // capture block.state
        json.writeValue("state", m_state.ordinal());
        
        // capture block.timer
        if(m_timer != null)
        {
            json.writeValue("timer", m_timer);
        }
        
        if(m_blockGroup != null)
        {
            json.writeValue("groupId", m_blockGroup.getId());
            json.writeValue("chainCount", m_blockGroup.getChainCount());
        }
    }

    @Override
    public void read(Json json, OrderedMap<String, Object> jsonData)
    {
        // Restores Block.m_Id
        m_id = json.readValue("id", int.class, jsonData);
        
        // Restores Block.State
        m_state = STATES[json.readValue("state", int.class, jsonData)];
        
        // Restores block timer
        m_timer = json.readValue("timer", Timer.class, jsonData);
        
        Integer groupId = json.readValue("groupId", int.class, jsonData);
        if(groupId != null)
        {
            BlockGroup m_blockGroup = BlockGroupFactory.getInstance().getSetupMap().get(groupId);
            if(m_blockGroup == null)
            {
                m_blockGroup = new BlockGroup(groupId);
                m_blockGroup.setChainCount(json.readValue("chainCount", int.class, jsonData));
                BlockGroupFactory.getInstance().getSetupMap().put(groupId, m_blockGroup);
            }
        }
    }
    
    @Override
    protected void childrenChanged()
    {
    	if(this.getChildren().size <= 0)
    	{
    		m_logger.debug("Removing block!");
    		this.clearActions();
    		this.remove();
    	}
    }
    
	@Override
	public void draw(SpriteBatch batch, float parentAlpha)
	{
		if(m_id != EMPTY_ID)
		{
			TextureRegion blockTEdge = BlockFrenzyTextures.getInstance().getBlockTEdge();
			TextureRegion blockGlassRegion = BlockFrenzyTextures.getInstance().getBlockGlass();
			
			float edgeWidth = blockTEdge.getRegionHeight();
			
            if(m_state == Block.State.CLEARING)
            {
            	if(m_isExploding == false)
            	{
            		m_isExploding = true;
            		
            		ExplodingPiecePool pool = BlockFrenzyTextures.getInstance().getExplodingPiecePool();

            		// Add all broken glass pieces
            		TextureRegionActor glass = pool.newObject();
            		glass.setTextureRegion(BlockFrenzyTextures.getInstance().getTopLeft());
            		glass.setY(this.getHeight() - edgeWidth - glass.getHeight());
            		glass.setX(edgeWidth);
            		addActor(glass);

            		glass = pool.newObject();
            		glass.setTextureRegion(BlockFrenzyTextures.getInstance().getTopRight());
            		glass.setY(this.getHeight() - edgeWidth - glass.getHeight());
            		glass.setX(this.getWidth() - edgeWidth - glass.getWidth());
            		addActor(glass);

            		glass = pool.newObject();
            		glass.setTextureRegion(BlockFrenzyTextures.getInstance().getBottomRight());
            		glass.setY(edgeWidth);
            		glass.setX(this.getWidth() - edgeWidth - glass.getWidth());
            		addActor(glass);
            		
            		glass = pool.newObject();
            		glass.setTextureRegion(BlockFrenzyTextures.getInstance().getBottomLeft());
            		glass.setY(edgeWidth);
            		glass.setX(edgeWidth);
            		addActor(glass);

            		// Start the particle effect in the middle of the break
            		PooledEffect sparkEffect = BlockFrenzyTextures.getInstance().getSparkEffectPool().obtain();
            		sparkEffect.setPosition(glass.getX() + glass.getWidth(), glass.getY() + glass.getHeight());
            		addActor(new ParticleActor(sparkEffect));
            				
            		// Set the color
            		for(Actor actor : this.getChildren())
            		{
            			actor.setColor(new Color(BlockGenerator.getInstance().getColor(m_id - 1)));
            		}
            		
            		// Add all the edges
            		TextureRegionActor edge = pool.newObject();
            		edge.setTextureRegion(BlockFrenzyTextures.getInstance().getBlockTEdge());
            		edge.setY(this.getHeight() - edge.getHeight());
            		addActor(edge);

            		edge = pool.newObject();
            		edge.setTextureRegion(BlockFrenzyTextures.getInstance().getBlockBEdge());
            		addActor(edge);

            		edge = pool.newObject();
            		edge.setTextureRegion(BlockFrenzyTextures.getInstance().getBlockLEdge());
            		addActor(edge);

            		edge = pool.newObject();
            		edge.setTextureRegion(BlockFrenzyTextures.getInstance().getBlockREdge());
            		edge.setX(this.getWidth() - edge.getWidth());
            		addActor(edge);

            		// Setup all the actions (movement, rotation, fade-out
            		int setNo = 0;
            		for(final Actor actor : this.getChildren())
            		{
                        float rand = (float)Math.random();
	                    actor.addAction(
                    		sequence(
                				parallel(
		                    		moveBy(0.0F, -EXPLODE_Y * getHeight() * (float)Math.sin(ANGLES[setNo]), DURATION, parabolic),
		                    		moveBy(rand * EXPLODE_X * getHeight() * (float)Math.cos(ANGLES[setNo]), 0.0F, DURATION),
		                    		fadeOut(DURATION),
		                    		rotateBy(rand * 720.0F, DURATION)),
	                    		com.badlogic.gdx.scenes.scene2d.actions.Actions.removeActor()
							)
						);
	                    
	                    setNo = (setNo + 1) % 4;
            		}
            	}
            	
            	// Call group.draw() to draw the pieces
            	super.draw(batch, parentAlpha);
            }
            else
            {
				// Draw the center area
				TextureRegion blockBEdge = BlockFrenzyTextures.getInstance().getBlockBEdge();
				TextureRegion blockLEdge = BlockFrenzyTextures.getInstance().getBlockLEdge();
				TextureRegion blockREdge = BlockFrenzyTextures.getInstance().getBlockREdge();
				
				Color color = BlockGenerator.getInstance().getColor(m_id - 1);
				float alpha = -1.0F + parentAlpha + color.a;
				batch.setColor(new Color(color.r, color.g, color.b, alpha));
				batch.draw(blockGlassRegion, getX(), getY(), getOriginX(), getOriginY(), blockGlassRegion.getRegionWidth(), blockGlassRegion.getRegionHeight(), getScaleX(), getScaleY(), getRotation());

				
				color = Color.WHITE;
				batch.setColor(new Color(color.r, color.g, color.b, parentAlpha));
				
				// Draw the glare
				batch.draw(BlockFrenzyTextures.getInstance().getGlassGlare(), getX(), getY(), getOriginX(), getOriginY(), blockGlassRegion.getRegionWidth(), blockGlassRegion.getRegionHeight(), getScaleX(), getScaleY(), getRotation());

				// Draw the edges
				float origin = (float) blockGlassRegion.getRegionWidth() / 2.0F;
				batch.draw(blockTEdge, getX(), getY() + blockGlassRegion.getRegionHeight() - blockTEdge.getRegionHeight(), origin, origin, blockTEdge.getRegionWidth(), blockTEdge.getRegionHeight(), getScaleX(), getScaleY(), getRotation());
				batch.draw(blockBEdge, getX(), getY(), origin, origin, blockBEdge.getRegionWidth(), blockBEdge.getRegionHeight(), getScaleX(), getScaleY(), getRotation());
				batch.draw(blockLEdge, getX(), getY(), origin, origin, blockLEdge.getRegionWidth(), blockLEdge.getRegionHeight(), getScaleX(), getScaleY(), getRotation());
				batch.draw(blockREdge, getX() + blockGlassRegion.getRegionWidth() - blockREdge.getRegionWidth(), getY(), origin, origin, blockREdge.getRegionWidth(), blockREdge.getRegionHeight(), getScaleX(), getScaleY(), getRotation());
            }
		}
	}

	@Override
	public Actor hit(float x, float y) {
		// TODO Auto-generated method stub
		return null;
	}
}
