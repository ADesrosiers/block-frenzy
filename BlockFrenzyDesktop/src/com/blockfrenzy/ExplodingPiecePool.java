package com.blockfrenzy;

import com.badlogic.gdx.utils.Pool;

public class ExplodingPiecePool extends Pool<TextureRegionActor>
{
	public ExplodingPiecePool(int initialCapacity, int max)
	{
		super(initialCapacity, max);
	}
	
	@Override
	protected TextureRegionActor newObject()
	{
		return new TextureRegionActor();
	}

}
