package com.blockfrenzy;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class TextureRegionActor extends Actor
{
	private TextureRegion m_textureRegion;
	
	public void setTextureRegion(TextureRegion textureRegion)
	{
		m_textureRegion = textureRegion;
		setWidth(m_textureRegion.getRegionWidth());
		setHeight(m_textureRegion.getRegionHeight());
		setOriginX(getWidth() / 2.0F);
		setOriginY(getHeight() / 2.0F);
	}
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha)
	{
		// Combine the transparency
		float alpha = -1.0F + parentAlpha + getColor().a;
		
		//color.a = Math.min(color.a, parentAlpha);
		Color newColor = new Color(getColor().r, getColor().g, getColor().b, alpha);
		batch.setColor(newColor);
		batch.draw(
				m_textureRegion, 
				getX(), 
				getY(), 
				getOriginX(), 
				getOriginY(), 
				getWidth(), 
				getHeight(), 
				getScaleX(),
				getScaleY(),
				getRotation());
	}

	@Override
	public Actor hit(float x, float y)
	{
		return x >= 0 && x < getWidth() && y >= 0 && y < getHeight() ? this : null;
	}

}
