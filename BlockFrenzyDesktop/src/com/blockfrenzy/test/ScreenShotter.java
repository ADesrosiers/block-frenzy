package com.blockfrenzy.test;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import com.badlogic.gdx.utils.Logger;
import com.blockfrenzy.DesktopStarter;

public class ScreenShotter
{
	private static final String FILE_BASE = "screenshot";
	private static final String FILE_EXTENSION = "png";
	
	private Logger 			m_logger = new Logger(ScreenShotter.class.getSimpleName());
	private String 			m_fileBase = null;
	private String 			m_directory = null;
	private int    			m_count = 0;
	private long   			m_sleepTimeMillis;
	private DesktopStarter 	m_desktopStarter = null;
	private boolean			m_isRunning = true;
	
	public ScreenShotter(String fileBase, String directory, long sleepTimeMillis)
	{
		m_fileBase = fileBase;
		m_directory = directory;
		m_sleepTimeMillis = sleepTimeMillis;
		m_logger.setLevel(Logger.DEBUG);

		// Start BlockFrenzy
		m_desktopStarter = new DesktopStarter(480, 800);
		
		// Start screen shot thread
		new Thread(new Runnable() {
			
			@Override
			public void run()
			{
				Robot robot;
				try
				{
					final JFrame jFrame = m_desktopStarter.getApplication().getJFrame();
					
					// Make a WindowListener to stop the capturing thread when the game window is closed
					jFrame.addWindowListener(new WindowAdapter() {
						@Override
						public void windowClosing(WindowEvent e) {
							m_isRunning = false;
						}
					});
						
						
					robot = new Robot();

					m_logger.debug("Starting screen capture");
					
					while(m_isRunning)
					{
						Rectangle rectangle = jFrame.getBounds();
						m_logger.debug("Capturing rectangle " + rectangle);
						
						BufferedImage screenShot = robot.createScreenCapture(rectangle);
						ImageIO.write(screenShot, FILE_EXTENSION, 
								new File(m_directory + "/" + m_fileBase + String.valueOf(m_count) + "." + FILE_EXTENSION));
						
						++m_count;
						
						// Sleep
						try
						{
							Thread.sleep(m_sleepTimeMillis);
						}
						catch (InterruptedException e)
						{
							m_logger.debug("Sleep interrupted");
							m_isRunning = false;
						}
					}

					m_logger.debug("Screen capturing finished");
				}
				catch (AWTException e)
				{
					m_logger.error("Unable to create Robot class to take screenshots");
				}
				catch (IOException e)
				{
					m_logger.error("IOException occurred while trying to save screenshots");
				}
			}
		}).start();
	}
	
	public static void main(String[] args)
	{
		String path = System.getProperty("user.home");
		File f = new File(path + File.separatorChar + ".blockfrenzy");
		f.mkdir();
		//String path = System.getenv("USERPROFILE");
		
		// Start screenshotter
		new ScreenShotter(FILE_BASE, f.getAbsolutePath(), 250L);
	}
}
