package com.blockfrenzy;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.utils.Logger;

public class BlockFrenzyInstructions implements Screen
{
	private static final float BORDER = 20.0F;
    private static final String INSTRUCTIONS_STRING="Instructions:\n\n" + 
    		"Drag the colored blocks horizontally in order match same-colored blocks. " +
    		"Matching three or more in a row causes the blocks to be cleared from the screen. " +
    		"The stack gradually rises and new blocks are added to the bottom of the stack. " +
    		"Don't let the stack reach the top of the screen, or you lose!\n\n" +
    		"Advanced:\n\n" +
    		"Matching four or more blocks in a row is called a combo and gets you more points. " +
    		"When a block falls as a result of a previous clear, and matches with other blocks, " +
    		"a chain is started and a point multiplier is rewarded. " +
    		"You can manually raise the stack by dragging upward on an empty region.";
    
    
    private Logger m_logger = new Logger(BlockFrenzyInstructions.class.getSimpleName(), Logger.DEBUG);
    private BlockFrenzyGame m_game;
    private ScrollPane m_scrollPane;
    
    
    public BlockFrenzyInstructions(BlockFrenzyGame game)
    {
		m_game = game;
		
		LabelStyle style = new LabelStyle(BlockFrenzyTextures.getInstance().getInstructionsFont(), Color.WHITE);
		Label label = new Label(INSTRUCTIONS_STRING, style);
		label.setWrap(true);
		
		m_scrollPane = new ScrollPane(label);
		m_scrollPane.setScrollingDisabled(true, false);
		m_scrollPane.setWidth(m_game.getStage().getWidth() - BORDER*2.0F);
		m_scrollPane.setX(BORDER);
		m_scrollPane.setHeight(m_game.getStage().getHeight());
	}

	@Override
	public void render(float delta)
	{
        m_game.getStage().act(delta);
        m_game.getStage().draw();
	}
	
	@Override
	public void show()
	{
		m_logger.debug("show()");
		
		m_game.getStage().addActor(m_scrollPane);
	}

	@Override
	public void hide()
	{
		m_logger.debug("hide()");
		m_scrollPane.remove();
	}

	@Override
	public void pause() {}

	@Override
	public void resume() {}

	@Override
	public void dispose() {}


	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}
}
