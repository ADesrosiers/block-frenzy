package com.blockfrenzy;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter.ScaledNumericValue;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Logger;

public class BackgroundActor extends Group
{
    private Logger m_logger = new Logger(BackgroundActor.class.getSimpleName());
    private ParticleEffect m_bgEffect;

    public BackgroundActor()
    {
    }
    
    public void initialize()
    {
    	m_bgEffect = BlockFrenzyTextures.getInstance().getBgEffect();
    	m_bgEffect.setPosition(getWidth() / 2.0F, getHeight() / 2.0F);
    	Array<ParticleEmitter> emitters = m_bgEffect.getEmitters();
    	for(ParticleEmitter emitter : emitters)
    	{
    		ScaledNumericValue spawnWidth = emitter.getSpawnWidth();
    		spawnWidth.setHigh(this.getWidth());

    		ScaledNumericValue spawnHeight = emitter.getSpawnHeight();
    		spawnHeight.setHigh(this.getHeight());
    	}
    }
    
    @Override
    public void draw(SpriteBatch batch, float parentAlpha)
    {        
        // Clear the screen before we begin rendering
        Gdx.gl.glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        
        m_bgEffect.draw(batch);
    }
    
    @Override
    public void act(float delta)
    {
    	m_bgEffect.update(delta);
    }
}
