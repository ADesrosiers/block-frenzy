package com.blockfrenzy;

public interface BlockFrenzySounds
{
    public void playSwap();
    
    public void playClear();
    
    public void playWarning();
    
    public void playMatch();
    
    public void playCombo();
}
