package com.blockfrenzy;

public interface LossWarningListener
{
    public enum LossWarningState {ON, OFF};
    public void onLossWarningChanged(LossWarningState state);
}
