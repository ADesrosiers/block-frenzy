package com.blockfrenzy;

import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import javax.media.opengl.GL;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.PressedListener;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Json.Serializable;
import com.badlogic.gdx.utils.Logger;
import com.badlogic.gdx.utils.OrderedMap;
import com.badlogic.gdx.utils.SerializationException;
import com.blockfrenzy.Block.State;
import com.blockfrenzy.BlockGroupFactory.BlockGroup;
import com.blockfrenzy.ComboActor.CircleType;
import com.blockfrenzy.LossWarningListener.LossWarningState;

public class BlockFrenzy extends Group implements Screen, Serializable
{
    /**
     * Used to store current grid/ block states for scoring purposes. Sent via "update" to observer.
     */
    public class ScoreEvent
    {
    	public final long m_combo, m_chain;
    	
    	public ScoreEvent(long combo, long chain)
    	{
    		m_combo = combo;
    		m_chain = chain;
    	}
    }
    
    public class BlockFrenzyObservable extends Observable
    {
    	@Override
    	public void notifyObservers()
    	{
    		BlockFrenzyObservable.this.setChanged();
    		super.notifyObservers();
    	}
    	
    	@Override
    	public void notifyObservers(Object arg)
    	{
    		BlockFrenzyObservable.this.setChanged();
    		super.notifyObservers(arg);
    	}
    }
    
    public enum GameState
    {
    	Starting,
    	Normal,
    	Losing,
    	Lost
    };
    
    public static final int 		MIN_MATCH_NO = 3;
    public static final float 		GAME_OVER_TIME = 2.0F;
    public static final int 		COUNTDOWN_START = 3;
    public static final float 		COUNTDOWN_INTERVAL = 1.0F;
    private static final String 	GAME_OVER_STRING = "Game Over!";
    private static final String 	GO_STRING = "Go!";
    private static final float 		INITIAL_ROW_TIME_INTERVAL = 12.0F;
    private static final float 		ROW_INTERVAL_RATIO = 0.99F;
    private static final float 		LOSS_TIMER_RATIO = 0.2F;
    private static final float 		STOP_TIMER_RATIO = 0.1F;
    private static final float 		CLEAR_TIME_RATIO = 0.083F;
    private static final float 		FALL_TIME = 0.2F;
    
    private BlockFrenzyGrid   		m_grid;

    /**
     * The time, in ms it takes for one row to come up from the bottom of the screen.
     * This value will decrease with time in order to make the game progressively harder.
     */
    private float            		m_rowInterval = INITIAL_ROW_TIME_INTERVAL;
    
    private Timer            		m_rowTimer = new Timer(m_rowInterval);
    
    private Timer 					m_nextClear = null;
    
    private Timer 					m_gameStateTimer = new Timer(COUNTDOWN_INTERVAL);
    
    private Timer 					m_stopTimer = new Timer(0.0F);
    
    private LossWarningState 		m_lossWarningState = LossWarningState.OFF;
    
    private BlockGroupFactory 		m_blockGroupFactory = BlockGroupFactory.getInstance();
    
    private BlockGroup 				m_currGroup = m_blockGroupFactory.createBlockGroup();
    
    private int 					m_currComboSize; // Needed for ScoreEvent objects passed to ScoreObserver
    
    private int 					m_currChainCount; // Needed for ScoreEvent obj passed to ScoreObserver 
    
    private Logger 					m_logger = new Logger(BlockFrenzy.class.getName());
    
    private BlockFrenzyTextures		m_textures = BlockFrenzyTextures.getInstance();
    
    private BlockFrenzyGame 		m_game;
    
    private ScoreCalculator 		m_scoreCalculator;
    
    private ScoreActor 				m_scoreActor;
    
    private Table 					m_displayTable;
    private Label 					m_displayLabel;
    
    private int 					m_countdown = COUNTDOWN_START;
    

    private float 					m_blockLength;
    
    private Block   				m_source = null;
    private Block   				m_dest = null;
    private int     				m_distance = 0;

    
    private int 					m_upDragStart = -1;

    private int 					m_swapEnd = 0;
    private int 					m_swapStart = -1;
    private int 					m_swapIdx = -1;
    
    private BitmapFont 				m_font = BlockFrenzyTextures.getInstance().getGameplayFont();
    private float 					warningWidth = m_font.getBounds("!").width;
    private float 					warningHeight = m_font.getBounds("!").height;
    private float 					blockSize = new Block().getHeight();
    
    private BlockFrenzyObservable	m_observable = new BlockFrenzyObservable();
    
    // Start as lost in case we need to generate a new game
    private GameState 				m_gameState = GameState.Lost;
    
    
    /*
     * Only for use by json reader
     */
    public BlockFrenzy()
    {
		m_logger.setLevel(Logger.DEBUG);
        m_logger.debug("BlockFrenzy()");
	}
    
    public BlockFrenzy(BlockFrenzyGame game)
    {
		m_game = game;

        // Log for debugging
		m_logger.setLevel(Logger.DEBUG);
        m_logger.debug("BlockFrenzy(BlockFrenzyGame game)");
        
        m_grid = new BlockFrenzyGrid(m_game.getNBlocksX(), m_game.getNBlocksY());
        
        newGame(m_game.getNBlocksY() / 2);
        
        m_scoreCalculator = new ScoreCalculator();
	}
    
    public void initialize()
    {
        m_logger.debug("initialize()");
        
        //Gdx.graphics.getGLCommon().glDisable(GL.GL_DEPTH_TEST);
        //Gdx.gl11.glDisable(GL11.GL_DEPTH_TEST);
        
        m_blockLength = BlockFrenzyTextures.getInstance().getBlockGlass().getRegionWidth();
        setWidth(m_blockLength * m_grid.getWidth());
        setHeight(m_blockLength * m_grid.getHeight());
        setX(m_game.getStage().getWidth() / 2.0F - getWidth() / 2.0F);
        
        this.addListener(new BlockFrenzy.InputListener());
    	
//    	m_gridActor.setX(m_game.getStage().getGutterWidth());
//    	m_gridActor.setY(m_game.getStage().getGutterHeight());
    	//m_gridActor.setX((BlockFrenzyGame.SCREEN_WIDTH - m_gridActor.getWidth()) / 2.0F);
    	//m_gridActor.setY((BlockFrenzyGame.SCREEN_HEIGHT - m_gridActor.getHeight()) / 2.0F);
    	
    	// Setup display table for countdown and game over message
    	m_displayTable = new Table();
    	
    	m_displayTable.addCaptureListener(new PressedListener()
    	{
    		@Override
    		public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
    		{
    			// Block the user from swapping during countdown / game over
    			m_logger.debug("Touch down on table!");
    			return true;
    		}
    	});
    	
    	if(BlockFrenzyGame.DEBUG_MODE)
    	{
    		m_displayTable.debug();
    	}

    	m_displayTable.setFillParent(true);
    	m_displayTable.row().fill().expand().center();
    	m_displayLabel = new Label(
    			String.valueOf(m_countdown), 
    			new LabelStyle(BlockFrenzyTextures.getInstance().getTitleFont(), Color.WHITE));
    	m_displayLabel.setAlignment(Align.center);
    	m_displayTable.add(m_displayLabel);
    	
        /* Register ScoreCalculator for later updates */
    	m_observable.addObserver(m_scoreCalculator);
        m_logger.debug("Observers: " + m_observable.countObservers());

        
        BlockFrenzyTextures textures = BlockFrenzyTextures.getInstance();
        m_scoreActor = new ScoreActor(new LabelStyle(textures.getGameplayFont(), Color.WHITE));
        m_scoreActor.setLocation(getX(), m_game.getStage().getHeight() - m_scoreActor.getHeight());
        //m_scoreActor.setLocation(0.0F, getHeight() - m_scoreActor.getHeight());
        
        // Observe score changes and update the display
        m_scoreCalculator.addObserver(new Observer()
        {
        	@Override
        	public void update(Observable source, Object value)
        	{
        		if(source instanceof ScoreCalculator)
        		{
        			m_scoreActor.add((Long) value);
        		}
        	}
		});
        
        m_logger.debug("Score is " + m_scoreCalculator.getScore());
        m_scoreActor.setScore(m_scoreCalculator.getScore());
    }

	public GameState getGameState()
	{
		return m_gameState;
	}
	
    public void setGame(BlockFrenzyGame game)
    {
    	m_game = game;
    }

    public void reset()
    {
        newGame(m_grid.getHeight() / 2);
    	
        m_rowInterval = INITIAL_ROW_TIME_INTERVAL;
        m_rowTimer.reset(m_rowInterval);
        m_nextClear = null;
        m_gameState = GameState.Starting;
        m_gameStateTimer.reset(COUNTDOWN_INTERVAL);
        m_displayLabel.setText(String.valueOf(m_countdown));
        m_game.getStage().addActor(m_displayTable);
        
        m_scoreCalculator.resetScore();
        m_scoreActor.reset();
        
    }
    
    @Override
    public void pause() 
    {
        // Log for debugging
        m_logger.debug("pause()");
    }
    
    @Override
    public void resume() 
    {
        // Log for debugging
        m_logger.debug("onResume");
    }
    
    @Override
    public void dispose()
    {
    }

    @Override
    public void resize(int width, int height)
    {
        // TODO the stretch has no effect..how to fix?
        //m_stage.setViewport(width, height, true);
    }


    @Override
    public Actor hit(float x, float y)
    {
        return x > 0.0F && x < getWidth() && y > 0.0F && y < getHeight() ? this : null;
    }
	
	@Override
	public void show()
	{
		// Check if reset needed
		if(m_gameState == GameState.Lost)
		{
			reset();
		}
		
        m_game.getStage().addActor(this);
        m_game.getStage().addActor(m_scoreActor);

        // Add countdown
        if(this.m_gameState == GameState.Starting)
        {
        	m_game.getStage().addActor(m_displayTable);
        }
	}

	@Override
	public void hide()
	{
		// Remove from stage
		m_displayTable.remove();
		m_scoreActor.remove();
		this.remove();
	}


    @Override
    public void write(Json json)
    {
        long start = System.currentTimeMillis(); // time to pass once for all objects to be saved

        try
        {
        	json.writeValue("gameState", m_gameState);
            json.writeValue("grid", m_grid);

            if (m_nextClear != null) 
            {
                json.writeValue("nextClear", m_nextClear);
            }
            
            if (m_rowTimer != null) 
            {
                json.writeValue("rowTimer", m_rowTimer);
            }
            
            json.writeValue("rowInterval", m_rowInterval);
            json.writeValue("currGroupId", m_blockGroupFactory.getCurrGroupId());
            json.writeValue("scoring", m_scoreCalculator);
        }
        catch(SerializationException j)
        {
            m_logger.error("JSON Error: Visiting" + j.toString());
        }
        
        m_logger.debug(String.valueOf("Saving state took " + (System.currentTimeMillis() - start) + "ms"));
    }

    @Override
    public void read(Json json, OrderedMap<String, Object> jsonData)
    {
        try 
        {
        	m_gameState = json.readValue("gameState", GameState.class, jsonData);
        	if(m_gameState != GameState.Lost)
        	{
        		m_gameState = GameState.Starting;
        	}
        	
            m_grid = json.readValue("grid", BlockFrenzyGrid.class, jsonData);
            m_nextClear = json.readValue("nextClear", Timer.class, jsonData);
            m_rowTimer = json.readValue("rowTimer", Timer.class, jsonData);
            m_rowInterval = json.readValue("rowInterval", float.class, jsonData);
            int currGroupId = json.readValue("currGroupId", int.class, jsonData);
            m_blockGroupFactory.setCurrGroupId(currGroupId);
            m_scoreCalculator = json.readValue("scoring", ScoreCalculator.class, jsonData);
        } 
        catch(SerializationException j)
        {
            m_logger.error("JSON: Loading" + j.toString());
        }
        catch(Exception ex)
        {
            m_logger.error("JSON error: " + ex.toString());
        }
    }
    

	@Override
	public void render(float deltaTime)
	{
        if(m_gameState != GameState.Normal)
        {
            m_gameStateTimer.update(deltaTime);
        }
        
		if(m_gameState == GameState.Normal || m_gameState == GameState.Losing)
		{
	        if(m_stopTimer.getRemainingTime() < 0.0F)
	        {
	            m_rowTimer.update(deltaTime);
	        }
	        else
	        {
	            m_stopTimer.update(deltaTime);
	        }
	        
	        if(m_nextClear != null)
	        {
	            m_nextClear.update(deltaTime);
	        }
	        
	        // Update all the Blocks' timers
	        for(int y = 0; y < m_grid.getHeight(); ++y)
	        {
	            for(int x = 0; x < m_grid.getWidth(); ++x)
	            {
	                Timer timer = m_grid.getValue(x, y).getTimer();
	                if(timer != null && timer != m_nextClear)
	                {
	                    timer.update(deltaTime);
	                }
	            }
	        }
	        
	        this.updatePhysics();
		}
		else if(m_gameState == GameState.Lost)
		{
			if(m_gameStateTimer.getRemainingTime() < 0.0F)
			{
				m_displayTable.remove();
				
	            // Game over, go to the high scores screen
	        	m_game.getScoresScreen().addHighScore(m_scoreCalculator.getScore());
	            m_game.setScreen(m_game.getScoresScreen());
	            return;
			}
		}
		else // Starting state
		{
			if(m_gameStateTimer.getRemainingTime() < 0.0F)
			{
				if(m_countdown <= 0)
				{
					// Remove start message and move the state to normal to initiate gameplay
					m_displayTable.remove();
					m_gameState = GameState.Normal;
					m_countdown = COUNTDOWN_START; // reset the countdown so it's the correct value next time we start

			        // Check for initial matches
			        this.checkMatches();
			        this.gravity(); // TODO might not be needed
				}
				else
				{
					m_gameStateTimer.reset(null);
					--m_countdown;
					
					// Update the text
					if(m_countdown > 0)
					{
						m_displayLabel.setText(String.valueOf(m_countdown));
					}
					else
					{
						m_displayLabel.setText(GO_STRING);
					}
				}
			}
		}

        m_game.getStage().act(deltaTime);
        m_game.getStage().draw();

        if(BlockFrenzyGame.DEBUG_MODE)
        {
        	Table.drawDebug(m_game.getStage());
        }
	}
    
    @Override
    public void draw(SpriteBatch batch, float parentAlpha)
    {
        // Need to set the tint color back to white since it may have been changed by the previous render
        batch.setColor(1.0F, 1.0F, 1.0F, 1.0F);
        m_font.setColor(1.0F, 1.0F, 1.0F, 1.0F);
        // Unfortunately, we have to pre-process the grid to find the swapping destination. This is because when the user
        // initiates the drag, we don't know which way they are dragging yet. By the time we find that out, the block's position
        // in the grid array may have changed (because the blocks rise). So we have to save the block object and then when the
        // user drags, we save the drag location (left or right), and then on each render we need to save the block to the left
        // or right of the dragging block. If we don't do this before the render, we may have already rendered the left block in
        // the wrong place. The only way around this would be to register for updates when a new row is added to the bottom.

        // Draw the background. Disable blending first because it is not needed and performance boost can be gained by not using it.

        //m_font.draw(batch, String.valueOf(m_rowTimer.getRemainingTime()), 25, height - 20);
        
        // The following prints out the current group id
        // TODO remove debug
//        m_font.setColor(Color.WHITE);
//        m_font.draw(batch, 
//                  String.valueOf(m_activity.getCurrGroupId()), 
//                  m_xLeft + 10, 
//                  m_yTop + 20);
        // TODO end remove debug
        
        // Draw the boundary nine patch
        BlockFrenzyTextures.getInstance().getBoundaryPatch().draw(batch, 
        													      this.getX(), 
        													      this.getY(), 
        													      this.getWidth(),
        													      this.getHeight());
        
        int adjustedHeight = 0;
        if(m_rowTimer.getRemainingTime() > 0)
        {
            adjustedHeight = adjustedHeight - (int)((m_rowTimer.getRemainingTime()) * m_blockLength / m_rowTimer.getInterval());
        }
        
        Block curr;
        Block swap = null;
        int swapX = 0;
        int swapY = 0;
        
        float currentTintColor = 1.0F;

        boolean isSwapWarning = false;
        boolean isWarning = false;
        for(int widthNo = 0; widthNo < m_grid.getWidth(); ++widthNo)
        {
        	if(m_grid.getValue(widthNo, m_grid.getHeight() - 1).getId() != Block.EMPTY_ID)
        	{
        		isWarning = true;
        	}
        	else
        	{
        		isWarning = false;
        	}
        	
	        for(int heightNo = m_grid.getHeight() - 1; heightNo >= 0; --heightNo)
	        {
            
            // TODO remove debug
            // This prints the row offset
//            int val = (m_grid.getOffset() + y) % m_grid.getHeight();
//            if(val < 0) val += m_grid.getHeight();
//            m_font.draw(batch, 
//                       String.valueOf(val), 
//                      x + (float)(m_gridStartWidth - 15), 
//                      (float)(adjustedHeight + (m_grid.getHeight() - 1 - y) * m_blockLength + m_blockLength / 2));
            // TODO end remove debug
            
                // Display the first row greyed-out to indicate that the user can't move it yet
                if(heightNo == 0)
                {
                    currentTintColor = 0.65F;
                }
                else
                {
                	currentTintColor = 1.0F;
                }
                
                curr = m_grid.getValue(widthNo, heightNo);
                if(curr.getId() != Block.EMPTY_ID)
                {
                	if(curr == m_source)
                    {
                        // Just set it up for rendering later. We do this so the swapping block is always 
                        // rendered on top of all other blocks.
                        swap = curr;
                        // Need to make sure the block can't be moved outside of the play area
                        swapX = widthNo * (int)m_blockLength + (m_distance);
                        swapY = adjustedHeight + (heightNo) * (int)m_blockLength;
                        
                        // Setup for later
                    	isSwapWarning = isWarning;
                    }
                    else if(curr == m_dest)
                    {
                        // Render the swapping destination block. It should move towards the
                        // swapping block as the swapping block moves towards it, and vice-versa
                    	curr.setX(getX() + (float)(widthNo * m_blockLength - (m_distance)));
                    	curr.setY((float)(adjustedHeight + (heightNo) * m_blockLength));
                    	curr.draw(batch, currentTintColor);
                    	
                    	if(isWarning)
                    	{
                    		batch.setColor(Color.WHITE);
                    		m_font.draw(batch, "!", curr.getX() + (blockSize - warningWidth) / 2.0F, curr.getY() + (blockSize + warningHeight) / 2.0F);
                    	}
                    }
                    else if(curr.getState() == Block.State.FALLING)
                    {
                        // Render it according to the percentage of the fall that is left
                        float percentLeft = (float)curr.getTimer().getRemainingTime() / (float)curr.getTimer().getInterval();


                    	curr.setX(getX() + (float)(widthNo * m_blockLength));
                    	curr.setY((float)(adjustedHeight + (heightNo) * m_blockLength + (percentLeft*m_blockLength)));
                    	curr.draw(batch, currentTintColor);

                    	if(isWarning)
                    	{
                    		batch.setColor(Color.WHITE);
                    		m_font.draw(batch, "!", curr.getX() + (blockSize - warningWidth) / 2.0F, curr.getY() + (blockSize + warningHeight) / 2.0F);
                    	}
                    }
                    else
                    {
                    	curr.setX(getX() + (float)(widthNo * m_blockLength));
                    	curr.setY((float)(adjustedHeight + (heightNo) * m_blockLength));
                    	curr.draw(batch, currentTintColor);

                    	if(isWarning)
                    	{
                    		batch.setColor(Color.WHITE);
                    		m_font.draw(batch, "!", curr.getX() + (blockSize - warningWidth) / 2.0F, curr.getY() + (blockSize + warningHeight) / 2.0F);
                    	}
                    }
                }
                
                // TODO remove debug
                // This section prints out the state. Useful for debugging various issues
//                if(curr.getState().ordinal() != 0)
//                {
//                    char[] chars = new char[] {'N', 'C', 'F', 'S'};
//                    String s = String.valueOf(chars[curr.getState().ordinal()]);
//                    m_font.draw(batch, s, 
//                              x + (float)(x * m_blockLength + m_blockLength / 2), 
//                              (float)(adjustedHeight + y * m_blockLength + m_blockLength / 2));
//                }
                
                // This section prints out the block group. Useful for debugging combo/chain issues
//                if(curr.getBlockGroup() != null)
//                {
//                    int code = curr.getBlockGroup().hashCode();
//                    code = code - (code / 100 * 100);
//                    m_font.draw(batch, String.valueOf(code), 
//                              x + (float)(x * m_blockLength + 2), 
//                              (float)(adjustedHeight + y * m_blockLength + m_blockLength / 2));
//                }
                
                // This section prints out the timer info
//                if(curr.getTimer() != null)
//                {
//                    long rem = curr.getTimer().getRemainingTime(currentTime);
//                    //if(rem < 0) rem = 0;
//                    font.draw(batch, String.valueOf(rem), 
//                              x + (float)(m_gridStartWidth + x * image.getWidth() + 2), 
//                              (float)(adjustedHeight + y * image.getHeight() + image.getHeight() / 2));
//                }
                // TODO end remove debug
            }
        }
        
        currentTintColor = 1.0F;
        if(swap != null)
        {
        	swap.setX(getX() + (float)(swapX));
        	swap.setY((float)(swapY));
        	swap.draw(batch, currentTintColor);

        	if(isSwapWarning)
        	{
        		batch.setColor(Color.WHITE);
        		m_font.draw(batch, "!", getX() + swap.getX() + (blockSize - warningWidth) / 2.0F, swap.getY() + (blockSize + warningHeight) / 2.0F);
        	}
        	
            // TODO remove debug
//            char[] chars = new char[] {'N', 'C', 'F', 'S'};
//            String s = String.valueOf(chars[swap.getState().ordinal()]);
//            font.draw(batch, s, 
//                      x + (float)(swapX + image.getWidth() / 2), 
//                      (float)(swapY + image.getHeight() / 2));
            // TODO end remove debug
        }
        
        // Call super draw()
        super.draw(batch, parentAlpha);
    }
    
    // Only set the timer if it actually expires sooner than the current next timer
    private void setNextTimer(Timer t)
    {
        if(t != null && (m_nextClear == null || m_nextClear.getRemainingTime() > t.getRemainingTime()))
        {
            m_nextClear = t;
        }
    }
    
    private void updatePhysics()
    {
        // Check if the matches are ready to be removed
        Block curr = null;
        boolean isMatchCheckNeeded = false;
        if(m_nextClear != null && m_nextClear.getRemainingTime() < 0)
        {
            m_nextClear = null;
            Timer timer = null;
            boolean hasClears = false;
            
            // Need to check all blocks
            for(int x = 0; x < m_grid.getWidth(); ++x)
            {
                // We start checking at row 1 because row 0 is not in play yet and therefore there can't
                // be any action in that row.
                for(int y = 1; y < m_grid.getHeight(); ++y)
                {
                    curr = m_grid.getValue(x, y);
                    if(curr.getTimer() != null && curr.getTimer().getRemainingTime() < 0)
                    {
                        if(curr.getState() == Block.State.CLEARING)
                        {
                            doChain(x, y, curr.getBlockGroup());
                            
                            // Note that we have to adjust the location because BlockFrenzy group takes care of
                            // adjusting for getX() location
                            curr.setX(curr.getX() - getX());
                            
                            // Add curr to the GlassRenderer's actor list so that the explosion will be rendered
                            addActor(curr);
                            
                            curr = new Block(Block.EMPTY_ID);
                            m_grid.setValue(x, y, curr);
                            hasClears = true;
                        }
                        else if(curr.getState() == Block.State.FALLING)
                        {
                            if(m_grid.getValue(x, y-1).getId() == Block.EMPTY_ID)
                            {
                                if(timer == null)
                                {
                                    timer = new Timer(FALL_TIME);
                                    setNextTimer(timer);
                                }
                                
                                // Need to move the block down one and reset the timer
                                curr.setTimer(timer);
                                m_grid.setValue(x, y-1, curr);
                                m_grid.setValue(x, y, new Block(Block.EMPTY_ID));
                            }
                            else
                            {
                                // It's done falling so put the state back to normal and clear the timer
                                curr.setTimer(null);
                                curr.setState(Block.State.NORMAL);
//                                curr.setBlockGroup(null);
//                                
//                                // Also need to clear the block groups applied above this block
//                                // OK to re-use the curr variable here because this is the last place it's used in the loop
//                                // TODO might be a better way to do this without the extra loop here
//                                for(int z = y + 1; z < m_grid.getHeight(); ++z)
//                                {
//                                    curr = m_grid.getValue(x, z);
//                                    if(curr.getId() == Block.EMPTY_ID || 
//                                       curr.getState() == Block.State.CLEARING || 
//                                       curr.getState() == Block.State.FALLING)
//                                    {
//                                        break;
//                                    }
//                                    else
//                                    {
//                                        curr.setBlockGroup(null);
//                                    }
//                                }
                                
                                isMatchCheckNeeded = true;
                            }
                        }
                        else
                        {
                            // Do nothing
                        }
                    }
                    else
                    {
                        setNextTimer(curr.getTimer());
                    }
                }
            }
            
            // Only need to invoke gravity if blocks were cleared
            if(hasClears == true)
            {
                //m_logger.log("nClears is " + nClears);
            	m_textures.playClear();
                this.gravity();
            }
        }
        
        if(m_gameState == GameState.Losing)
        {
            if(m_gameStateTimer.getRemainingTime() < 0.0F)
            {
            	m_gameState = GameState.Lost;
            	
            	// Setup gameover table
            	m_displayLabel.setText(GAME_OVER_STRING);
            	m_game.getStage().addActor(m_displayTable);
            	
            	m_gameStateTimer.reset(2.0F);
            	return;
            }
            else
            {
                // Need to check for game over or for game over warning
            	m_gameState = GameState.Normal;
                for(int x = 0; x < m_grid.getWidth(); ++x)
                {
                    if(m_grid.getValue(x, m_grid.getHeight() - 1).getId() != Block.EMPTY_ID)
                    {
                    	m_gameState = GameState.Losing;
                        break;
                    }
                }
            }
        }
        
        if (m_gameState != GameState.Losing && m_rowTimer.getRemainingTime() <= 0.0F)
        {
            m_logger.debug("Row timer elapsed. Interval is " + m_rowInterval);
            
            // Disable the stop time if the user is dragging up
            if((m_upDragStart != -1) && m_stopTimer.getRemainingTime() > 0.0F)
            {
                m_stopTimer.update(m_stopTimer.getRemainingTime() + 1.0F);
            }
            
            // Need to check for game over or for game over warning
            for(int x = 0; x < m_grid.getWidth(); ++x)
            {
                if(m_grid.getValue(x, m_grid.getHeight() - 1).getId() != Block.EMPTY_ID)
                {
                    // Activate loss timer
                    m_gameStateTimer.reset(m_rowInterval * LOSS_TIMER_RATIO);
                	m_gameState = GameState.Losing;
                    break;
                }
            }
            
            // Only move the row if we are not already at the top
            if(m_gameState != GameState.Losing)
            {
                // Check for loss warning
                for(int x = 0; x < m_grid.getWidth(); ++x)
                {
                    if(m_grid.getValue(x, m_grid.getHeight() - 2).getId() != Block.EMPTY_ID)
                    {
                        // Notify the observers
                        m_lossWarningState = LossWarningState.ON;
                        
                        m_textures.playWarning();
                        
                        break;
                    }
                }
                
                // Shift the grid by one row to introduce the a new row at the
                // bottom of the
                // playing field.
                m_grid.shiftRows(1);
    
                m_textures.playSwap();
    
                // Fill a new row
                fillRow(0);
    
                // Update the row interval and reset the row timer.
                // Let's decrement by 1% each time so that it speeds up quickly at first, but then
                // doesn't speed up as much if the speed is already fast.
                m_rowInterval *= ROW_INTERVAL_RATIO;
                m_rowTimer.reset(m_rowInterval);
    
                // Check for matches and clear them
                isMatchCheckNeeded = true;
            }
        }

        if(isMatchCheckNeeded == true)
        {
            // We also need to check the matches because the falling block may have just matched with something.
            checkMatches();
            
            // Clear blockgroups
            for(int x = 0; x < m_grid.getWidth(); ++x)
            {
                for(int y = 1; y < m_grid.getHeight(); ++y)
                {
                    curr = m_grid.getValue(x, y);
                    if(curr.getId() != Block.EMPTY_ID && curr.getState() != Block.State.FALLING && curr.getState() != Block.State.CLEARING)
                    {
                        curr.setBlockGroup(null);
                    }
                }
            }
        }
    }

    private void newGame(int fillHeight)
    {
		for (int y = (m_grid.getHeight() - 1); y > fillHeight; --y)
		{
			for(int x = 0; x < m_grid.getWidth(); ++x)
			{
				m_grid.setValue(x, y, new Block(Block.EMPTY_ID));
			}
		}
		
		for(int y = fillHeight; y >= 0; --y)
		{
			fillRow(y);
		}
    }
    
    private void fillRow(int y)
    {
        // Ensure there are no matches in the new row
        int sameCount = 0;
        int lastId = -1;
    	Block block;
        for (int x = 0; x < m_grid.getWidth(); ++x)
        {
        	// Get a new random blocks
        	block = BlockGenerator.getInstance().randomBlock();
        	
        	// Check the vertical
        	checkVertMatch(block, x, y);
        	
        	if(block.getId() == lastId)
        	{
            	// If the id is the same as the last one, increment the same counter
        		++sameCount;

        		// If we have reached enough of the same blocks to make a match
        		// we will change the one we just got to something else
            	if(sameCount >= MIN_MATCH_NO - 1)
            	{
            		// Just use the next id, but avoid id 0 (empty block id)
            		int val = (lastId + 1) % BlockGenerator.getInstance().getNBlockTypes();
            		if(val == 0) val = 1;
            		block.setId(val);
            		//m_logger.debug("Changing matching horizontal at " + x);
            		
            		// Reset the counter
            		sameCount = 0;
            		
                	// Need to re-check the vertical because we just changed the id
                	checkVertMatch(block, x, y);
            	}
        	}
        	else
        	{
        		// Reset the counter if the current id is different from the last one
        		sameCount = 0;
        	}
        	
        	// Finally, set the value in the grid.
            m_grid.setValue(x, y, block);
        	
            // Update the last id
    		lastId = block.getId();
        }
    }
    
    private void checkVertMatch(Block block, int x, int y)
    {
    	int id = block.getId();
    	int startY = 1;
    	for(; startY < MIN_MATCH_NO; ++startY)
    	{
    		if(m_grid.getValue(x, startY + y).getId() != id)
    		{
    			break;
    		}
    	}
    	
    	// If we got to the end of the loop, then all blocks matched, so change this one
    	// so there is no longer a match.
    	if(startY >= MIN_MATCH_NO)
    	{
    		int val = (id + 1) % BlockGenerator.getInstance().getNBlockTypes();
    		if(val == 0) val = 1;
    		block.setId(val);
    		//m_logger.debug("Changing matching vertical at " + x);
    	}
    	
    }
    
    private BlockGroup mergeGroups(BlockGroup group1, BlockGroup mergeFromGroup, Timer t)
    {
        BlockGroup mergeToGroup = group1;
        BlockGroup currGroup = null;

        // Temporary move all blocks into the mergeFromGroup
        mergeFromGroup.addAll(group1);
        
        // Find the minimum group out of all the blockGroups. Note that this is not necessarily one of the input groups
        for(Block b : mergeFromGroup)
        {
            currGroup = b.getBlockGroup();
            if(currGroup != null && currGroup.getId() < mergeToGroup.getId())
            {
                mergeToGroup = currGroup;
            }
        }
        
        // Now merge all the blocks into the minimum group
        for(Block b : mergeFromGroup)
        {
            currGroup = b.getBlockGroup();
            if(currGroup != null && currGroup.getId() != mergeToGroup.getId())
            {
                // Make sure to clear out the old groups so they don't stick around in memory
                currGroup.clear();
            }
            
            // Add the block to the toGroup
            if(mergeToGroup.contains(b) == false)
            {
                mergeToGroup.add(b);
            }
            
            // Set the block's group to be the toGroup
            b.setBlockGroup(mergeToGroup);
            
            // Also set the clear timer
            b.setTimer(t);
        }
        
        // Clear the fromGroup
        mergeFromGroup.clear();
        
        // Return the mergeTo group
        return mergeToGroup;
    }
    
    private void checkMatches()
    {
        Block curr;
        Vector<BlockGroup> groups = new Vector<BlockGroup>();
        Timer t = new Timer(CLEAR_TIME_RATIO * m_rowInterval);
        
        // This is used to figure out if a match was caused by a chain
        int startGroupId = m_currGroup.getId();
        
        // Check horizontal
        int lastId = -1;
        for(int y = 1; y < m_grid.getHeight(); ++y)
        {
            //m_currGroup.clear(); // Added to possibly fix disappearing blocks issue. May not be necessary
            lastId = -1;
            for(int x = 0; x <= m_grid.getWidth(); ++x)
            {
                // We do this in order to avoid duplicating code after the for loop
                if(x < m_grid.getWidth())
                {
                    curr = m_grid.getValue(x, y);
                }
                else
                {
                    curr = new Block(Block.EMPTY_ID);
                }
                
                if(lastId != curr.getId() || curr.getState() == Block.State.FALLING || curr.getState() == Block.State.CLEARING)
                {
                    if(m_currGroup.size() >= MIN_MATCH_NO)
                    {
                        BlockGroup nextGroup = m_blockGroupFactory.createBlockGroup();
                        BlockGroup minGroup = mergeGroups(m_currGroup, nextGroup, t);
                        
                        minGroup.setLocation(new Point(x - 1, y));
                        groups.add(minGroup);
                        
                        if(minGroup.getId() < startGroupId)
                        {
                            minGroup.incrementChain();
                        }
                        
                        m_currGroup = nextGroup;
                    }
                    
                    m_currGroup.clear();
                    lastId = curr.getId();
                }
                
                // Add current block to the group if it's not empty, not being cleared, and not falling
                if(lastId != Block.EMPTY_ID && curr.getState() != Block.State.CLEARING && curr.getState() != Block.State.FALLING)
                {
                    m_currGroup.add(curr);
                }
            }
        }
        
        // Check vertical. Note that we have to check for intersections with horizontal matches and
        // merge them if we find them.
        for(int x = 0; x < m_grid.getWidth(); ++x)
        {
            //m_currGroup.clear(); // Added to possibly fix disappearing blocks issue. May not be necessary
            lastId = -1;
            for(int y = 1; y <= m_grid.getHeight(); ++y)
            {
                // We do this in order to avoid duplicating code after the for loop
                if(y < m_grid.getHeight())
                {
                    curr = m_grid.getValue(x, y);
                }
                else
                {
                    curr = new Block(Block.EMPTY_ID);
                }
                
                if(lastId != curr.getId() || curr.getState() == Block.State.FALLING || curr.getState() == Block.State.CLEARING)
                {

                    if(m_currGroup.size() >= MIN_MATCH_NO)
                    {
                        BlockGroup nextGroup = m_blockGroupFactory.createBlockGroup();
                        BlockGroup minGroup = mergeGroups(m_currGroup, nextGroup, t);

                        if(minGroup.getId() < startGroupId)
                        {
                            minGroup.incrementChain();
                        }
                        
                        minGroup.setLocation(new Point(x, y - 1));
                        
                        if(groups.contains(minGroup) == false)
                        {
                            groups.add(minGroup);
                        }
                        
                        m_currGroup = m_blockGroupFactory.createBlockGroup();
                    }
                    
                    m_currGroup.clear();
                    lastId = curr.getId();
                }
                
                // Add current block to the group if it's not empty and not being cleared
                if(lastId != Block.EMPTY_ID && curr.getState() != Block.State.CLEARING && curr.getState() != Block.State.FALLING)
                {
                    m_currGroup.add(curr);
                }
            }
        }
    
        if(groups.isEmpty() == false)
        {   
            // Need to mark the block as clearing and notify timer observers
            // TODO we might be able to remove some unused groups first
            int totalSize = 0;
            for(BlockGroup bg : groups)
            {
                totalSize += bg.size();
                
                for(Block b : bg)
                {
                    // If we just matched a swapping block, stop swapping
                    if(b.getState() == Block.State.SWAPPING)
                    {
                        stopSwap();
                    }
                    
                    b.setState(Block.State.CLEARING);
                }
            }

            
            m_currComboSize = totalSize; // Needed for ScoreObserver
            
            // Check for combo
            boolean isComboChain = false;
            ComboActor comboActor = null;
            if(totalSize > MIN_MATCH_NO)
            {
                Point point = groups.get(0).getLocation();
                final ComboActor actor = new ComboActor(
                        CircleType.COMBO, 
                        (int)m_blockLength * point.x, 
                        (int)m_blockLength * point.y, 
                        String.valueOf(totalSize));
                addActor(actor);
                comboActor = actor;
                isComboChain = true;
            }

            // Check for chains
            for(BlockGroup group : groups)
            {
                if((m_currChainCount = group.getChainCount())  > 0)
                {
                    Point chainLocation = groups.get(0).getLocation();
                    
                    // Calculate the x location of the chain circle. Move it over if there is a combo circle already in position
                    float xLoc = (int)m_blockLength * chainLocation.x;
                    if(isComboChain)
                    {
                        xLoc += comboActor.getWidth();
                    }
                    
                    final ComboActor chainActor = new ComboActor(
                            CircleType.CHAIN,
                            xLoc, 
                            (int)m_blockLength * chainLocation.y, 
                            "x" + String.valueOf(group.getChainCount() + 1));
                    addActor(chainActor);
                    isComboChain = true;
                }
                
                // Clear the group so chains don't have the old blocks counted
                group.clear();
            }
            
            if(isComboChain)
            {
                // Play combo sound
            	m_textures.playCombo();

                // Stop the stack from rising if we got a combo or chain
                // We will not accumulate multiple combos, but if a previous combo would
                // stop the stack for longer than the current combo, don't change the stop time.
                float stopInterval = m_rowInterval * STOP_TIMER_RATIO * Math.max(totalSize, m_currChainCount*MIN_MATCH_NO);
                if(m_stopTimer.getRemainingTime() < 0.0F || stopInterval > m_stopTimer.getRemainingTime())
                {
                    m_stopTimer.reset(stopInterval);
                }
            }

            setNextTimer(t);
            
            m_textures.playMatch();
            
            /*Update ScoreOberver here??*/
            m_observable.notifyObservers(new ScoreEvent(m_currComboSize, m_currChainCount));
        }
        
        // Need to check if we are no longer in a loss warning state
        if(m_lossWarningState == LossWarningState.ON)
        {
            boolean isWarning = false;
            for(int x = 0; x < m_grid.getWidth(); ++x)
            {
                if(m_grid.getValue(x, m_grid.getHeight() - 1).getId() != Block.EMPTY_ID)
                {
                    isWarning = true;
                    break;
                }
            }
            
            if(isWarning == false)
            {
                // Notify the observers
                m_lossWarningState = LossWarningState.OFF;
            }
        }
    }
    
    // This is called when a block 
    private void doChain(int x, int y, BlockGroup group)
    {
        Block chain = null;
        // Also need to clear the block groups applied above this block
        // TODO might be a better way to do this without the extra loop here
        for(int z = y + 1; z < m_grid.getHeight(); ++z)
        {
            chain = m_grid.getValue(x, z);
            if(chain.getId() == Block.EMPTY_ID || 
               chain.getState() == Block.State.CLEARING || 
               chain.getState() == Block.State.FALLING)
            {
                break;
            }
            else
            {
                chain.setBlockGroup(group);
            }
        }
    }
    
    private void gravity()
    {
        Timer timer = null;
        Block curr = null;
        Block prev = null;
        boolean isFalling = false;
        
        for(int x = 0; x < m_grid.getWidth(); ++x)
        {
            isFalling = false;
            prev = m_grid.getValue(x, 1);
            // Start at two because the first row is not in play yet and we want to check if the block below this one is empty
            for(int y = 2; y < m_grid.getHeight(); ++y)
            {
                curr = m_grid.getValue(x, y);
                
                // If the block is clearing or it's already falling, don't do anything to it.
                // Need to check the case where the block below is falling in case we swap onto a falling block
                if(curr.getId() != Block.EMPTY_ID && 
                   curr.getState() != Block.State.CLEARING && 
                   curr.getState() != Block.State.FALLING)
                {
                    if(prev.getId() == Block.EMPTY_ID || isFalling == true)
                    {
                        // If we haven't created the timer yet then do it now and check if it is the closest to elapsing by
                        // calling setNextTimer().
                        if(timer == null)
                        {
                            timer = new Timer(FALL_TIME);
                            setNextTimer(timer);
                        }
                        
                        // Let's stop the swap.
                        // TODO maybe in the future can restore the swap after the gravity is finished
                        if(curr.getState() == Block.State.SWAPPING)
                        {
                            stopSwap();
                        }
                        
                        // Set the block to falling and initiate the falling timer. Also immediately move this block down one row.
                        // This is because as soon as the falling starts the block is assumed to be in the next row.
                        curr.setState(Block.State.FALLING);
                        curr.setTimer(timer);
                        
                        m_grid.setValue(x, y-1, curr);
                        
                        // We set the isFalling to true. This allows us to just copy the block from above to fill in the space
                        // instead of setting it to a new empty block and then copying the block from above.
                        isFalling = true;
                    }
                    else if(prev.getState() == Block.State.FALLING)
                    {
                        // Important need to check this condition in case a block is swapped directly on top of a falling block.
                        curr.setState(Block.State.FALLING);
                        curr.setTimer(prev.getTimer());
                    }
                }
                else
                {
                    if(isFalling == true)
                    {
                        // Need to put an empty block                        
                        curr = new Block(Block.EMPTY_ID);
                        m_grid.setValue(x, y-1, curr);
                    }
                    
                    isFalling = false;
                }
                    
                prev = curr;
            }

            // Need to check the top block
            if(isFalling == true)
            {
                // Need to put an empty block                        
                curr = new Block(Block.EMPTY_ID);
                m_grid.setValue(x, m_grid.getHeight()-1, curr);
            }
        }
    }

    private Point getBlockAtLocation(int x, int y)
    {
        Point p = null;
        
        int remaining = (int)((m_rowTimer.getRemainingTime()) * m_blockLength / m_rowTimer.getInterval());
        
        int xIdx = x / (int)m_blockLength;
        int yIdx = (y + remaining) / (int)m_blockLength;
        
        if(xIdx >= 0 && xIdx < m_grid.getWidth() && yIdx >= 0 && yIdx < m_grid.getHeight())
        {
            p = new Point(xIdx, yIdx);
        }
        
        return p;
    }
    
    private void swap(int y, int x1, int x2)
    {
        Block b1 = m_grid.getValue(x1, y);
        Block b2 = m_grid.getValue(x2, y);
        
        if(b1.getState() == Block.State.FALLING)
        {
            b2.setState(State.FALLING);
            b2.setTimer(b1.getTimer());
            b1.setState(State.NORMAL);
            b1.setTimer(null);
        }
        else if(b2.getState() == Block.State.FALLING)
        {
            b1.setState(State.FALLING);
            b1.setTimer(b2.getTimer());
            b2.setState(State.NORMAL);
            b2.setTimer(null);
        }
        else
        {
            // Do nothing
        }
        
        m_grid.swap(x1, y, x2, y);

        BlockFrenzyTextures.getInstance().playSwap();
    }

    private void setSwappingLocation(int x)
    {
        if(m_source != null)
        {
            // Deselect the block if it is being cleared
            // Added "m_source.getId() == Block.EMPTY_ID" to make sure empty blocks can't be swapped.
            // This caused strange side effects when put into setSwapSource(), so it's here instead.
            if(m_source.getState() == Block.State.CLEARING)
            {
                m_source = null;
                m_dest = null;
                m_swapEnd = -1;
            }
            else
            {
                // Need to make sure the destination is not in the clear state
                int y = 1;
                for(; y < m_grid.getHeight(); ++y)
                {
                    if(m_grid.getValue(m_swapIdx, y) == m_source)
                    {
                        break;
                    }
                }
                
                // Make sure the user isn't dragging off the screen or trying to swap with a clearing block.
                if((m_swapIdx == m_grid.getWidth() - 1 && x > m_swapStart) ||
                   (m_swapIdx == 0 &&  x < m_swapStart) ||
                   (x > m_swapStart && m_swapIdx < m_grid.getWidth() - 1 && m_grid.getValue(m_swapIdx + 1, y).getState() == Block.State.CLEARING) ||
                   (x < m_swapStart && m_swapIdx > 0 && m_grid.getValue(m_swapIdx - 1, y).getState() == Block.State.CLEARING))
                {
                    m_swapEnd = m_swapStart;
                }
                else
                {
                    m_swapEnd = x;
                }
                
                // Need to check if the block has moved far enough to be swapped. If it has,
                // swap it and update all relevant variables. Note that we can only swap if
                // the destination is not being cleared
                if(Math.abs(m_swapEnd - m_swapStart) >= m_blockLength)
                {
                    // Found the swap source, now find the destination
                    if(m_swapEnd > m_swapStart)
                    {
                        if(m_grid.getValue(m_swapIdx + 1, y).getState() != Block.State.CLEARING)
                        {
                            swap(y, m_swapIdx, m_swapIdx + 1);
                            ++this.m_swapIdx;
                        }
                    }
                    else 
                    {
                        if(m_grid.getValue(m_swapIdx - 1, y).getState() != Block.State.CLEARING)
                        {
                            swap(y, m_swapIdx, m_swapIdx - 1);
                            --this.m_swapIdx;
                        }
                    }
                    m_swapStart = m_swapEnd;
                    
                    // Also need to check for matches and update gravity
                    this.gravity();
                    this.checkMatches();
                    
                    // Check if our swap source block made a match and deselect it if it did
                    // The call to gravity() may have set the swap source to null.
                    if(m_source != null && (m_source.getState() == Block.State.CLEARING || m_source.getState() == Block.State.FALLING))
                    {
                        m_source = null;
                        this.m_swapEnd = -1;
                    }
                    else if(m_source == null)
                    {
                        this.m_swapEnd = -1;
                    }
                    else
                    {
                        // Do nothing
                    }
                }

                if(m_swapEnd >= 0 && m_swapIdx >= 0)
                {
                    // We will skip row 0 because it isn't in play
                    y = 1;
                    for(; y < m_grid.getHeight(); ++y)
                    {
                        if(m_grid.getValue(m_swapIdx, y) == m_source)
                        {
                            // Found the swap source, now find the destination
                            if(m_swapEnd > m_swapStart)
                            {
                                m_dest = m_grid.getValue(m_swapIdx + 1, y);
                            }
                            else if(m_swapEnd < m_swapStart)
                            {
                                m_dest = m_grid.getValue(m_swapIdx - 1, y);
                            }
                        }
                    }
                }
            }

            m_distance = m_swapEnd - m_swapStart;
        }
    }
    
    private class InputListener extends com.badlogic.gdx.scenes.scene2d.InputListener
    {
    	@Override
    	public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
    	{
            // Log for debugging
            m_logger.debug("touchDown()");
            
            final Point p = getBlockAtLocation((int)x, (int)y);
            //StringBuilder sb = new StringBuilder("Touch detected");
            
            if(p != null && p.y > 0)
            {
                //sb.append(": ").append(p.x).append(", ").append(p.y).append(")");
                Block source = m_grid.getValue(p.x, p.y);
                if(source.getId() != Block.EMPTY_ID)
                {
                    if(source.getState() != Block.State.CLEARING && source.getState() != Block.State.FALLING)
                    {
                        source.setState(State.SWAPPING);
                        m_source = source;
                        m_swapEnd = (int)x;
                        m_swapIdx = p.x;
                        m_swapStart = (int)x;
                        
                        m_dest = null;
                        m_distance = 0;
                    }
                }
                else
                {
                    // Only allow up drag if the press was on an empty spot and didn't have a
                    // block to the left, right or beneath.
                    boolean isDownEmpty = (p.y == 0 || m_grid.getValue(p.x, p.y - 1).getId() == Block.EMPTY_ID);
                    boolean isLeftEmpty = (p.x == 0 || m_grid.getValue(p.x - 1, p.y).getId() == Block.EMPTY_ID);
                    boolean isRightEmpty = (p.x == m_grid.getWidth() - 1 || m_grid.getValue(p.x + 1, p.y).getId() == Block.EMPTY_ID);
                    if(isDownEmpty && isLeftEmpty && isRightEmpty)
                    {
                        m_upDragStart = (int)y;
                    }
                }
            }
            return true;
    	}
    	
    	@Override
    	public void touchUp(InputEvent event, float x, float y, int pointer, int button)
    	{
            // Log for debugging
            m_logger.debug("touchUp()");
            
            // Clear the up drag
            if(m_upDragStart != -1)
            {
                m_upDragStart = -1;
            }
            
            dragReleased((int)x);
    	}
    	
    	@Override
    	public void touchDragged(InputEvent event, float x, float y, int pointer)
    	{
          setSwappingLocation((int)x);

          // Process the upward drag if it has been initiated
          int upDragUpdate = (int)y;
          if(m_upDragStart != -1 && m_upDragStart < upDragUpdate)
          {
              // To figure out the time to update the row timer by, we need to figure out the percent of a block
              // that the user moved the stack. For instance, moving up by half a block would be 50%. Then we figure out what
              // 50% of the timer interval is and advance the timer by that much. Note that we may spill over into another row.
              float updateTime = m_rowInterval * (upDragUpdate - m_upDragStart) / m_blockLength;
              float remTime = m_rowTimer.getRemainingTime();
              
              if(remTime > 0)
              {
                  if(updateTime > remTime)
                  {
                      updateTime = remTime;
                  }
                  
                  // Update the timer by resetting it and adjusting the start time by updateTime.
                  m_rowTimer.update(updateTime);
              }
              
              // Update the start drag
              m_upDragStart = upDragUpdate;
          }
    	}
    }

    private void dragReleased(int x)
    {
        do
        {   
            // Set the state back to normal
            if(m_source == null)
            {
                break;
            }
            
            m_source.setState(Block.State.NORMAL);
            
            // Make sure the user isn't dragging off the screen
            if((m_swapIdx < 0) ||
               (m_swapIdx == m_grid.getWidth() - 1 && x > m_swapStart) || 
               (m_swapIdx == 0 &&  x < m_swapStart))
            {
                break;
            }

            // We will skip row 0 because it isn't in play
            int y = 1;
            for(; y < m_grid.getHeight(); ++y)
            {
                if(m_grid.getValue(m_swapIdx, y) == m_source)
                {
                    break; // Found the y value
                }
            }
            
            // Make sure user isn't swapping with clearing block
            if((m_swapIdx < m_grid.getWidth() - 1 && m_grid.getValue(m_swapIdx + 1, y).getState() == Block.State.CLEARING) ||
               (m_swapIdx > 0 && m_grid.getValue(m_swapIdx - 1, y).getState() == Block.State.CLEARING))
            {
                break;
            }
            
            // TODO remove magic numbers
            // Swap left
            if(x - m_swapStart <= -m_blockLength * 0.5)
            {
                swap(y, m_swapIdx, m_swapIdx - 1);
                // Also need to check for matches and update gravity
                this.gravity();
                this.checkMatches();
            }
            else if(x - m_swapStart >= m_blockLength * 0.5)
            {
                swap(y, m_swapIdx, m_swapIdx + 1);
                // Also need to check for matches and update gravity
                this.gravity();
                this.checkMatches();
            }
            else
            {
                // Don't swap it, because we were less than halfway over the swap point.
            }
        }
        while(false);
        
        m_source = null;
        m_dest = null;
        m_swapEnd = -1;
        this.m_swapIdx = -1;
        m_swapStart = -1;

        m_distance = m_swapEnd - m_swapStart;
    }
    
    // This will just call dragReleased() and pass it the last known swap location.
    // This is useful if a block being swapped suddenly starts falling and we need to
    // Clear the swapping state.
    // TODO currently this will not swap the blocks even if they are close enough to be considered swaps.
    private void stopSwap()
    {
        dragReleased(m_swapStart);
    }
}
