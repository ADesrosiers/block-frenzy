package com.blockfrenzy;

public interface AbstractGrid<T>
{
    public int getHeight();
    public int getWidth();
    
    public void setValue(int x, int y, T value);
    
    /**
     */
    public void swap(int x1, int y1, int x2, int y2) throws ArrayIndexOutOfBoundsException;
    
    /**
     */
    public T getValue(int x, int y);
    
    public void shiftRows(int shiftAmount);
}
