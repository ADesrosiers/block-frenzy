package com.blockfrenzy;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Json.Serializable;
import com.badlogic.gdx.utils.OrderedMap;

public class BlockFrenzyGrid implements AbstractGrid<Block>, Serializable
{
    private Block[] m_blocks = null;
    
    /**
     * Width of the playing area
     */
    private int m_width = 0;
    
    /**
     * Height of the playing area
     */
    private int m_height = 0;
    
    /**
     * Since new rows need to be inserted into the bottom of the playing area, this index variable
     * allows for the array to be shifted without physically moving all the blocks. This essentially
     * makes a circular buffer.
     */
    private int m_currentBaseRow = 0;

    /**
     * Default constructor to be used by the JSON API
     */
    public BlockFrenzyGrid() 
    {
    }
    
    /**
     * Constructor takes a width and height for the grid and creates a width * (height + 1) array
     * for the Blocks. The extra +1 is for the next line to be inserted into the playing field. 
     * @param width Width of the playing area, expressed in number of blocks
     * @param height Height of the playing area, expressed in number of blocks
     */
    public BlockFrenzyGrid(int width, int height) 
    {
        m_blocks = new Block[width * height];
        m_width = width;
        m_height = height;
        m_currentBaseRow = 0;
        
        for(int x = 0; x < width; ++x)
        {
            for(int y = 0; y < height; ++y)
            {
                Block block = new Block(Block.EMPTY_ID);
                m_blocks[y * width + x] = block;
            }
        }
        
    }
    
    public int getHeight()
    {
        return m_height;
    }
    
    public int getWidth()
    {
        return m_width;
    }

    @Override
    public void setValue(int x, int y, Block value) 
    {
        int val = y + m_currentBaseRow;
        if(val < 0) val = m_height + val;
        m_blocks[(val % m_height)* m_width + x] = value;
    }

    @Override
    public void swap(int x1, int y1, int x2, int y2) throws ArrayIndexOutOfBoundsException 
    {
        y1 = y1 + m_currentBaseRow;
        if(y1 < 0) y1 = m_height + y1;
        y2 = y2 + m_currentBaseRow;
        if(y2 < 0) y2 = m_height + y2;
        
        Block swap = m_blocks[(y1 % m_height) * m_width + x1];
        m_blocks[(y1 % m_height) * m_width + x1] = m_blocks[(y2 % m_height) * m_width + x2];
        m_blocks[(y2 % m_height) * m_width + x2] = swap;
    }
    
    // TODO remove debug
    public int getOffset()
    {
        return m_currentBaseRow;
    }

    @Override
    public Block getValue(int x, int y) 
    {
        int val = y + m_currentBaseRow;
        if(val < 0) val = m_height + val;
        return m_blocks[(val % m_height) * m_width + x];
    }

    public void shiftRows(int shiftAmount)
    {
        int val = m_currentBaseRow - shiftAmount;
        m_currentBaseRow = (val % m_height);
    }

    @Override
    public void write(Json json)
    {   
        json.writeValue("width", m_width);
        json.writeValue("height", m_height);
        json.writeArrayStart("array");
        for(int y = 0; y < getHeight(); ++y)
        {
            for(int x = 0; x < getWidth(); ++x)
            {
                // capture block
                json.writeValue(getValue(x, y));
            }
        }
        json.writeArrayEnd();
    }

    @Override
    public void read(Json json, OrderedMap<String, Object> jsonData)
    {
        m_width = json.readValue("width", int.class, jsonData);
        m_height = json.readValue("height", int.class, jsonData);
        m_blocks = json.readValue("array", Block[].class, jsonData);
        m_currentBaseRow = 0;
    }
}
