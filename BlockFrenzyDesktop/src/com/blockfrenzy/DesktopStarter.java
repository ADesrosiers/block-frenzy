package com.blockfrenzy;

import com.badlogic.gdx.backends.jogl.JoglApplication;
import com.badlogic.gdx.tools.imagepacker.TexturePacker2;

public class DesktopStarter implements ActivityRequestHandler
{
	private JoglApplication m_application = null;
	
    public static void main(String[] args)
    {
        //TexturePacker2.process("../images", "../BlockFrenzyAndroid/assets/data/packed", "pack");
        new DesktopStarter(480, 800);
    }
    
    public DesktopStarter(int width, int height)
    {
    	m_application = new JoglApplication(new BlockFrenzyGame(this), "BlockFrenzy", width, height, false);
	}

	@Override
	public void showAds(boolean show)
	{
		// Do nothing, no ads to display on desktop version
	}
	
	public JoglApplication getApplication()
	{
		return m_application;
	}
}
