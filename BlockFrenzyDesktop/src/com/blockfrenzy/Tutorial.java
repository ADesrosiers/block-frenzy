package com.blockfrenzy;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.jogl.JoglApplication;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.tools.imagepacker.TexturePacker2;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Logger;

public class Tutorial implements ApplicationListener
{
    public static void main(String[] args)
    {
        new JoglApplication(new Tutorial(), "Test", 400, 640, false);
    }
    
    @Override
    public void create()
    {
    	Color[] blockTypes = {Color.BLACK, Color.BLUE, Color.RED};
    	Json json = new Json();
    	String output = json.toJson(blockTypes);
    	Gdx.app.setLogLevel(Application.LOG_DEBUG);
    	Logger logger = new Logger(Tutorial.class.getSimpleName(), Logger.DEBUG);
    	logger.debug("\"" + output + "\"");
    }

    @Override
    public void dispose()
    {
        
    }

    @Override
    public void pause()
    {
        
    }

    @Override
    public void render()
    {
    }

    @Override
    public void resize(int arg0, int arg1)
    {
        
    }

    @Override
    public void resume()
    {
        
    }

}
