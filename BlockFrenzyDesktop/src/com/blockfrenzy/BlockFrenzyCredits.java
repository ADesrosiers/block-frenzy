package com.blockfrenzy;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Logger;
import com.esotericsoftware.tablelayout.Cell;

public class BlockFrenzyCredits implements Screen
{
	public static final String CREDITS_STRING = "Credits";
	private static final int PAD = 20;

    private Logger m_logger = new Logger(BlockFrenzyCredits.class.getSimpleName(), Logger.DEBUG);
    private BlockFrenzyGame m_game;
	private Table m_table = null;
    private ScrollPane m_scrollPane;
    
    public BlockFrenzyCredits(BlockFrenzyGame game)
    {
		m_game = game;
		
	    LabelStyle labelStyle = new LabelStyle(BlockFrenzyTextures.getInstance().getInstructionsFont(), Color.WHITE);
		
    	m_table = new Table();

		m_scrollPane = new ScrollPane(null);
		m_scrollPane.setFillParent(true);
		m_scrollPane.setScrollingDisabled(true, false);

		// Do not use m_table.setFillParent(true)!!!!
		m_table.clear();
		m_table.top().center();
		m_table.row();
		m_table.add(new Label(CREDITS_STRING, labelStyle));
		m_table.pad(PAD);
		
		Table subTable = new Table().center().bottom();
		m_table.row().fill().expand();
		m_table.add(subTable);

		subTable.row();
		subTable.add(new Label("--- Producer ---", labelStyle)).top().padTop(45.0F).padBottom(25.0F);
		subTable.row();
		subTable.add(new Label("Aaron Desrosiers", labelStyle)).top();
		
		subTable.row();
		subTable.add(new Label("--- Developers ---", labelStyle)).top().padTop(45.0F).padBottom(25.0F);
		subTable.row();
		subTable.add(new Label("Aaron Desrosiers", labelStyle)).top();
		subTable.row();
		subTable.add(new Label("Jesse Foote", labelStyle)).top();

		subTable.row();
		subTable.add(new Label("--- Artwork ---", labelStyle)).top().padTop(45.0F).padBottom(25.0F);
		subTable.row();
		subTable.add(new Label("Aaron Desrosiers", labelStyle)).top();
		subTable.row();
		subTable.add(new Label("Derek Foote", labelStyle)).top();

		Label label = new Label(
				"Sounds based on sounds from freesound.org, Licensed under the Attribution License. See creativecommons.org/licenses/by/3.0/", 
				labelStyle);
		label.setWrap(true);
		label.setAlignment(Align.center);
		subTable.row().fill();
		subTable.add(label).top().padTop(45.0F);
		
		Array<Cell> labels = new Array<Cell>(4);
		Label currLabel = null;
		float maxLabelX = 0.0F;
		
		Table soundsTable = new Table().center().bottom().padTop(PAD);
		soundsTable.defaults().center();
		
		currLabel = new Label("Match", labelStyle);
		currLabel.setAlignment(Align.right);
		maxLabelX = Math.max(maxLabelX, currLabel.getWidth());
		labels.add(soundsTable.add(currLabel).right().padRight(PAD));
		
		currLabel = new Label("Eliot Lash", labelStyle);
		maxLabelX = Math.max(maxLabelX, currLabel.getWidth());
		labels.add(soundsTable.add(currLabel).left().padLeft(PAD));

		label = new Label(
				"freesound.org/people/Halleck/sounds/21915/", 
				labelStyle);
		label.setWrap(true);
		label.setAlignment(Align.center);
		soundsTable.row().fill().expandX();
		soundsTable.add(label).colspan(2).padBottom(PAD);
		
		soundsTable.row();
		currLabel = new Label("Clear", labelStyle);
		currLabel.setAlignment(Align.right);
		maxLabelX = Math.max(maxLabelX, currLabel.getWidth());
		labels.add(soundsTable.add(currLabel).right().padRight(PAD));
		labels.add(soundsTable.add().left().padLeft(PAD));
		
		label = new Label("freesound.org/people/themfish/sounds/34201/", labelStyle);
		label.setWrap(true);
		label.setAlignment(Align.center);
		soundsTable.row().fill().expandX();
		soundsTable.add(label).colspan(2).padBottom(PAD);
		
		soundsTable.row();
		currLabel = new Label("Swap", labelStyle);
		currLabel.setAlignment(Align.right);
		maxLabelX = Math.max(maxLabelX, currLabel.getWidth());
		labels.add(soundsTable.add(currLabel).right().padRight(PAD));

		currLabel = new Label("Richard Frohlich", labelStyle);
		maxLabelX = Math.max(maxLabelX, currLabel.getWidth());
		labels.add(soundsTable.add(currLabel).left().padLeft(PAD));
		
		label = new Label("freesound.org/people/FreqMan/sounds/42899/", labelStyle);
		label.setWrap(true);
		label.setAlignment(Align.center);
		soundsTable.row().fill().expandX();
		soundsTable.add(label).colspan(2).padBottom(PAD);
		
		soundsTable.row();
		currLabel = new Label("Warning", labelStyle);
		currLabel.setAlignment(Align.right);
		maxLabelX = Math.max(maxLabelX, currLabel.getWidth());
		labels.add(soundsTable.add(currLabel).right().padRight(PAD));
		labels.add(soundsTable.add().left().padLeft(PAD));
		
		label = new Label("freesound.org/people/daveincamas/sounds/43801/", labelStyle);
		label.setWrap(true);
		label.setAlignment(Align.center);
		soundsTable.row().fill().expandX();
		soundsTable.add(label).colspan(2).padBottom(PAD);
		
		soundsTable.row();
		currLabel = new Label("Combo", labelStyle);
		currLabel.setAlignment(Align.right);
		maxLabelX = Math.max(maxLabelX, currLabel.getWidth());
		labels.add(soundsTable.add(currLabel).right().padRight(PAD));
		labels.add(soundsTable.add().left().padLeft(PAD));
		
		label = new Label("freesound.org/people/zerolagtime/sounds/29625/", labelStyle);
		label.setWrap(true);
		label.setAlignment(Align.center);
		soundsTable.row().fill().expandX();
		soundsTable.add(label).colspan(2).padBottom(PAD);
		
		soundsTable.row();
		soundsTable.add().colspan(2).expandY();

		subTable.row().fill().expand();
		subTable.add(soundsTable).center();
		
		// Make sure labels are the same size
//		for(Cell cell : labels)
//		{
//			cell.minWidth(maxLabelX);
//		}
		
		if(BlockFrenzyGame.DEBUG_MODE)
		{
			soundsTable.debug();
			//subTable.debug();
			//m_table.debug();
		}
		
		m_scrollPane.setWidget(m_table);
	}
    
	@Override
	public void render(float delta)
	{
        m_game.getStage().act(delta);
        m_game.getStage().draw();

        if(BlockFrenzyGame.DEBUG_MODE)
        {
        	Table.drawDebug(m_game.getStage());
        }
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show()
	{
		m_logger.debug("show()");
		
		m_game.getStage().addActor(m_scrollPane);
	}

	@Override
	public void hide()
	{
		m_logger.debug("hide()");
		m_scrollPane.remove();
	}

	@Override
	public void pause() {}

	@Override
	public void resume() {}

	@Override
	public void dispose() {}

}
