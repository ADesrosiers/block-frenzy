package com.blockfrenzy;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;


public class ScoreActor extends Actor
{
	private static final String PLUS = "+";
	
	private Label m_scoreLabel;
	private Label m_addLabel;
	private long m_score = 0;
	private long m_add = 0;
	private NumberFormat m_format = NumberFormat.getInstance();
	private float m_waitTime = 0.0F;
	private float m_deltaTotal = 0.0F;
	
	/*
	 * Number of point to add to the total score each millisecond
	 */
	private float m_addRate = 0.005F;
	
	public ScoreActor(LabelStyle style)
	{
		this(style, style);
	}
	
	public ScoreActor(LabelStyle scoreStyle, LabelStyle addStyle)
	{
		if(m_format instanceof DecimalFormat)
		{
			DecimalFormat format = (DecimalFormat) m_format;
			format.applyPattern("000000");
		}
		
		m_scoreLabel = new Label(m_format.format(m_score), scoreStyle);
		m_addLabel = new Label(m_format.format(m_add), addStyle);
		this.setWidth(Math.max(m_scoreLabel.getWidth(), m_addLabel.getWidth()));
		this.setHeight(Math.max(m_scoreLabel.getHeight(), m_addLabel.getHeight()));
	}
	
	public void setScore(long score)
	{
		m_score = score;
		m_scoreLabel.setText(m_format.format(m_score));
	}
	
	public void setLocation(float x, float y)
	{
		m_scoreLabel.setX(x);
		m_scoreLabel.setY(y);

		m_addLabel.setX(m_scoreLabel.getX() + m_scoreLabel.getWidth());
		m_addLabel.setY(y);
	}
	

	@Override
	public void draw(SpriteBatch batch, float parentAlpha)
	{
		m_scoreLabel.draw(batch, parentAlpha);
		
		// Only draw the update if > 0
		if(m_add > 0)
		{
			m_addLabel.draw(batch, parentAlpha);
		}
	}

	@Override
	public Actor hit(float x, float y)
	{
		return null;
	}

	
	@Override
	public void act(float delta)
	{
		// Update the score displays if necessary
		if(m_add > 0)
		{
			m_waitTime -= delta;
			if(m_waitTime < 0.0F)
			{
				// Update the score and the add
				m_deltaTotal += delta;
				if(m_deltaTotal > m_addRate)
				{
					m_deltaTotal = 0.0F;
					m_score += 1;
					m_add -= 1;
				}
			}
			
			// Update the labels
			m_scoreLabel.setText(m_format.format(m_score));
			
			// TODO might not be necessary for fixed number of digits
			//m_addLabel.x = m_scoreLabel.x + m_scoreLabel.getWidth() + 5.0F;
			
			m_addLabel.setText(PLUS + String.valueOf(m_add));
		}
	}
	
	public void add(long add)
	{
		if(m_add == 0)
		{
			m_waitTime = 1.0F;
		}
		m_add += add;
	}
	
	public void setAddRate(float rate)
	{
		m_addRate = rate;
	}
	
	public void reset()
	{
		m_score = 0;
		m_add = 0;
		m_scoreLabel.setText(m_format.format(m_score));
		m_addLabel.setText(PLUS + String.valueOf(m_add));
	}
}
