package com.blockfrenzy.actions;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.utils.Pool;

public class BlockFrenzyInterpolation
{
	/** Gets a new {@link ParabolicInterpolator} from a maintained pool of {@link Interpolator}s.
	 * <p>
	 * The initial tension is set to <code>{@value ParabolicInterpolator#DEFAULT_TENSION}</code>.
	 * 
	 * @return the obtained {@link ParabolicInterpolator} */
	public static final Interpolation parabolic = new Interpolation() {
		
		@Override
		public float apply(float a)
		{
			float value = 2*a - 0.5F;
			value *= value;
			return value - 0.25F;
		}
	};
}
