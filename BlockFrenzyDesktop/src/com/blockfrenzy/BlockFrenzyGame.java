package com.blockfrenzy;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Logger;
import com.badlogic.gdx.utils.SerializationException;
import com.blockfrenzy.BlockFrenzy.GameState;

public class BlockFrenzyGame extends Game
{
	public static final boolean DEBUG_MODE = false;
	
	private static final float FPS_LOCATION = 0.0325F;
	
	public static final int GRID_HEIGHT = 1280;
	public static final int GRID_WIDTH = 768;
	
    private static final String SAVE_FILE = "BlockFrenzySavedState.json";
    private static final String HISCORE_FILE = "BFHighScores.json";
	private static final String GRID_CONFIG_FILE = "data/grid.json";
    
    private Stage       		m_stage;
	private MenuScreen 			m_menuScreen;
	private BlockFrenzy     	m_blockFrenzy;
	private HighScoresScreen    m_highScoresScreen;
	private BlockFrenzyCredits  m_creditsScreen;
	private BlockFrenzyInstructions	m_instructions;
	private BackgroundActor     m_background;
	private Actor				m_fpsActor;
	private InputMultiplexer 	m_inputMultiplexer;
    private Logger 				m_logger = new Logger(BlockFrenzyGame.class.getName());
    private BlockFrenzyTextures m_textures;
    private ActivityRequestHandler m_adHandler;
    private int					m_nBlocksX;
    private int					m_nBlocksY;

    public BlockFrenzyGame(ActivityRequestHandler adHandler)
    {
		m_adHandler = adHandler;
	}
    

    // Create an input adapter that will switch to the menu screen when the back key or the space key is pressed
    private InputAdapter m_inputAdapter = new InputAdapter()
    {
        @Override
        public boolean keyDown(int keycode)
        {
            m_logger.debug("keyDown");
            if(keycode == Keys.BACK || keycode == Keys.SPACE)
            {
                setScreen(m_menuScreen);
            }
            return true;
        }
    };
    
	@Override
	public void create()
	{
		if(DEBUG_MODE)
		{
			Gdx.app.setLogLevel(Application.LOG_DEBUG);
		}
		else
		{
			Gdx.app.setLogLevel(Application.LOG_NONE);
		}
        
		m_logger.setLevel(Logger.DEBUG);
		m_logger.debug("create()");

        m_textures = BlockFrenzyTextures.getInstance();
		m_textures.initialize();
        
        // Set up the stage for use by the different Screens
		float resAspectRatio = (float)Gdx.graphics.getWidth() / (float)Gdx.graphics.getHeight();
		float stageAspectRatio =  (float)GRID_WIDTH / (float)GRID_HEIGHT;

	    float stageX = GRID_WIDTH;
		float stageY = GRID_HEIGHT;
		
		m_logger.debug("resAspectRatio: " + resAspectRatio);
		m_logger.debug("stageAspectRatio: " + stageAspectRatio);
		
		if(resAspectRatio < stageAspectRatio)
		{
			stageY = stageX / resAspectRatio;
		}
		else if(resAspectRatio > stageAspectRatio)
		{
			stageX = stageY * resAspectRatio;
		}
		
        m_stage = new Stage(stageX, stageY, true);

        m_logger.debug("Stage width: " + stageX + ", Stage height: " + stageY);
        m_logger.debug("Gutter width: " + (-m_stage.getGutterWidth()) + ", Gutter height: " + -m_stage.getGutterHeight());

        /* Load all game state data back out to objects from file */
        m_blockFrenzy = loadStateData();
        if(m_blockFrenzy == null)
        {
        	m_blockFrenzy = new BlockFrenzy(this);
        }
        else
        {
        	m_blockFrenzy.setGame(this);
        }
        m_blockFrenzy.initialize();
		
		// Initialize the high scores screen
        m_highScoresScreen.initialize();
        
        // Create a menu screen and set it as the initial screen
		m_menuScreen = new MenuScreen(this);
		
		// Use an input multiplexer so the screens can add other InputProcessors if necessary
		m_inputMultiplexer = new InputMultiplexer(m_stage);
        Gdx.input.setInputProcessor(m_inputMultiplexer);
        
        // The background Actor will be used by all Screens so we will add it to the stage here
        m_background = new BackgroundActor();
        m_background.setWidth(stageX);
        m_background.setHeight(stageY);
        m_background.initialize();
        m_stage.addActor(m_background);
        
        if(DEBUG_MODE)
        {
	        LabelStyle style = new LabelStyle(BlockFrenzyTextures.getInstance().getGameplayFont(), Color.WHITE);
	        m_fpsActor = new FpsActor(style);
	        m_fpsActor.setX(FPS_LOCATION * stageX);
	        
	        // This is not a mistake, want it to be equally far from each side
	        m_fpsActor.setY(FPS_LOCATION * stageX);
	        m_stage.addActor(m_fpsActor);
        }

		this.setScreen(m_menuScreen);
	}

	@Override
	public void render()
	{
		super.render();
		
//		m_logger.debug("render calls: " + m_stage.getSpriteBatch().renderCalls);
//		m_logger.debug("max sprites: " + m_stage.getSpriteBatch().maxSpritesInBatch);
	}
	
	public BlockFrenzyTextures getBlockFrenzyTextures()
	{
		return m_textures;
	}
	
	public Stage getStage()
	{
		return m_stage;
	}
	
	@Override
	public void setScreen(Screen screen)
	{
		super.setScreen(screen);
		
		if(screen != m_menuScreen)
		{
	        m_inputMultiplexer.addProcessor(m_inputAdapter);
	        
	        Gdx.input.setCatchBackKey(true);
	        
	        // Hide ads
	        m_adHandler.showAds(false);
		}
		else
		{
	        Gdx.input.setCatchBackKey(false);
	        m_inputMultiplexer.removeProcessor(m_inputAdapter);
	        
	        // Show ads
	        m_adHandler.showAds(true);
		}

        if(DEBUG_MODE)
        {
			// Make sure FPS is displayed on top of everything else
			m_fpsActor.remove();
			m_stage.addActor(m_fpsActor);
        }
	}
	
	public InputMultiplexer getInputMultiplexer()
	{
		return m_inputMultiplexer;
	}
	
	public MenuScreen getMenuScreen()
	{
		return m_menuScreen;
	}
	
	public HighScoresScreen getScoresScreen()
	{
		return m_highScoresScreen;
	}
	
	public BlockFrenzyCredits getCreditsScreen()
	{
		if(m_creditsScreen == null)
		{
			m_creditsScreen = new BlockFrenzyCredits(this);
		}
		
		return m_creditsScreen;
	}
	
	public BlockFrenzyInstructions getInstructionsScreen()
	{
		if(m_instructions == null)
		{
			m_instructions = new BlockFrenzyInstructions(this);
		}
		
		return m_instructions;
	}
	
	public BlockFrenzy getBlockFrenzy()
	{
		return m_blockFrenzy;
	}
	
	public int getNBlocksX() {
		return m_nBlocksX;
	}
	
	public int getNBlocksY() {
		return m_nBlocksY;
	}
	
	public GameState getGameState()
	{
		return m_blockFrenzy.getGameState();
	}

	@Override
	public void pause()
	{
		m_logger.debug("pause()");
		saveStateData();
		//m_textures.terminate();
		super.pause();
	}
	
	@Override
	public void resume()
	{
		m_logger.debug("resume()");
		//m_textures.initialize();
		super.resume();
	}

	@Override
	public void dispose()
	{
		m_logger.debug("dispose()");
		m_textures.terminate();
		super.dispose();
	}
	
	/**
	 * @purpose Create state-holding Json from all necessary game objects' data and write to file
	 * @current Saving following objects: Grid blocks (state, id, timer); nextClear timer.
	 */
	public void saveStateData()
	{
	    Json json = new Json();
	    
	    // Convert BlockFrenzy object to json (will use the Serializable interface's write() method)
	    String text = json.toJson(m_blockFrenzy);
	    
	    // Write the string to file
		BlockFrenzyIO.write(Gdx.files.external(SAVE_FILE), text);

	    // Convert HighScores object to json (will use the Serializable interface's write() method)
	    text = json.toJson(m_highScoresScreen);
		BlockFrenzyIO.write(Gdx.files.external(HISCORE_FILE), text);
	}
	/**
	 * @purpose Load all objects' data from JSONObjects retrieved via file (internal storage)
	 */
	public BlockFrenzy loadStateData()
	{
        BlockFrenzy blockFrenzy = null;
        
        String text = null;
        
        try
        {
	        text = BlockFrenzyIO.read(Gdx.files.external(SAVE_FILE));
	        
	        // Make sure the read worked
	        if(text != null && text.isEmpty() == false)
	        {
	            Json json = new Json();
	            blockFrenzy = json.fromJson(BlockFrenzy.class, text);
	            
	            // Clear out the hash map used to link the blockgroups with the blocks
	            BlockGroupFactory.getInstance().getSetupMap().clear();
	        }
        }
        catch(Exception exc)
        {
        	// Do nothing, we will just return null
        }

        
        text = null;
        
        try
        {
	        // Read hi scores
	        // TODO fix
	        text = BlockFrenzyIO.read(Gdx.files.external(HISCORE_FILE));
        }
        catch(IllegalArgumentException exc)
        {
        	// Do nothing, we will just return null
        }

        if(text != null && text.isEmpty() == false)
        {
	        try
	        {
		        Json json = new Json();
		        m_highScoresScreen = json.fromJson(HighScoresScreen.class, text);
	        }
	        catch(SerializationException se)
	        {
	        	// Do nothing, we will just return null
	        }
        }
        
        if(m_highScoresScreen == null)
        {
        	m_highScoresScreen = new HighScoresScreen(this);
        }
        else
        {
        	m_highScoresScreen.setGame(this);
        }


        text = null;
        
        try
        {
	
	        text = BlockFrenzyIO.read(Gdx.files.internal(GRID_CONFIG_FILE));
        }
        catch(IllegalArgumentException exc)
        {
        	// Do nothing, we will just return null
        }
        
        if(text != null && text.isEmpty() == false)
        {
	        Json json = new Json();
	        int[] vals = json.fromJson(int[].class, text);
	        m_nBlocksX = vals[0];
	        m_nBlocksY = vals[1];
        }
        
        return blockFrenzy;
	}
}
