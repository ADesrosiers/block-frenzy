package com.blockfrenzy;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;


public class FpsActor extends Label
{
	public FpsActor(LabelStyle style)
	{
		super("", style);
	}
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha)
	{
        // Draw frames per second
		setText(String.valueOf(Gdx.graphics.getFramesPerSecond()));
		super.draw(batch, parentAlpha);
	}

	@Override
	public Actor hit(float x, float y)
	{
		return null;
	}

}
