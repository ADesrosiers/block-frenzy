package com.blockfrenzy;

import java.util.Observable;
import java.util.Observer;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Json.Serializable;
import com.badlogic.gdx.utils.Logger;
import com.badlogic.gdx.utils.OrderedMap;
import com.blockfrenzy.BlockFrenzy.ScoreEvent;

public class ScoreCalculator extends Observable implements Serializable, Observer
{
	private Logger m_logger = new Logger(ScoreCalculator.class.getSimpleName());

	private static final long SCORE_PER_BLOCK = 10;
	
	// The following vals need to be static because of deserialization. See readResolve() of this class
	private long m_totalScore = 0; 
	private long m_briefScore = 0; //TODO Planning on using this for publishing scores to graphics module/Renderer; see calculateScore()
	
	public ScoreCalculator()
	{
		m_logger.setLevel(Logger.DEBUG);
	}
	
	@Override
	public void update(Observable o, Object arg) {
		
		if(arg instanceof ScoreEvent)
		{
			ScoreEvent event = (ScoreEvent) arg;
			
			// Get SCORE_PER_BLOCK points per block, plus SCORE_PER_BLOCK extra points per block over BlockFrenzy.MIN_MATCH_NO
			m_briefScore = SCORE_PER_BLOCK * event.m_combo + SCORE_PER_BLOCK * (event.m_combo - BlockFrenzy.MIN_MATCH_NO);
			
			if(event.m_chain > 0) 
			{	
				m_briefScore *= event.m_chain; //Multiply this Clear's score by whatever num chain we are in
			}

			// Now update total score to reflect addition of briefScore
			m_totalScore += m_briefScore; 
			//TODO remove debugging logger
			m_logger.debug("TotalScore is " + m_totalScore);
			
			this.setChanged();
			this.notifyObservers(m_briefScore);
		}
	}
	
	public long getScore()
	{
		return m_totalScore;
	}
	
	/**
	 * TODO Find out if there may be a safer way to do this! 
	 * Seems unsafe to be able to just call this reset method to be able to erase an entire game history!!!
	 * Perhaps we should write it to a score file "database" or something
	 */
	public void resetScore()
	{
		m_totalScore = 0;
		m_briefScore = 0; // This is not really necessary, but just in case
	}
	
	@Override
	public void read(Json json, OrderedMap<String, Object> jsonData)
	{
		// TODO Verify accuracy (if this is all we want to do).
		m_totalScore = json.readValue("totalScore", long.class, jsonData);
	}
	
	@Override
	public void write(Json json)
	{
		// TODO Verify accuracy (if this is all we want to do).
		json.writeValue("totalScore", m_totalScore);
	}
}
