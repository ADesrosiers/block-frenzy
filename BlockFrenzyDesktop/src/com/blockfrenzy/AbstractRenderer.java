package com.blockfrenzy;

/**
 * @author Aaron
 *
 */
public interface AbstractRenderer extends LossWarningListener
{   
    /** 
     * This method renders the display on the given canvas
     */
    public void render();
    
    public int getBlockWidth();
    
    /**
     * This method sets specifies the x and y bounds of the renderer, in terms of pixels on the screen.
     * Note that by convention, the top-left of the screen is denoted as position (0, 0).
     * @param xLeft X value of the left border of the renderer
     * @param xRight X value of the right border of the renderer
     * @param yTop Y value of the top border of the renderer
     * @param yBottom Y value of the bottom border of the renderer
     */
    public void setRenderBounds(int xLeft, int xRight, int yTop, int yBottom);
    
    /**
     * Gets the block that the point (x,y) is within. Returns null if the location does not refer to a block.
     * This method is needed for the TouchListener to figure out which block was touched
     * @param x The x value of the point
     * @param y The y value of the point
     * @return The block at location (x,y) or null if there is no block at the location
     */
    public Point getBlockAtLocation(int x, int y);
}
