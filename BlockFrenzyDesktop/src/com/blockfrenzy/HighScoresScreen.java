package com.blockfrenzy;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.TextInputListener;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Json.Serializable;
import com.badlogic.gdx.utils.Logger;
import com.badlogic.gdx.utils.OrderedMap;
import com.esotericsoftware.tablelayout.BaseTableLayout;

public class HighScoresScreen implements Screen, Serializable
{
	private static final String HIGH_SCORES_STRING = "High Scores";
	private static final int NUM_SCORES = 10;
	private static final String DEFAULT_NAME = "Nobody";
	private static final Long DEFAULT_SCORE = 0L;
	private static final int PAD = 20;
	private static final Pattern DISALLOWED_PATTERN = Pattern.compile("[^A-Za-z0-9_ ]");
	
	private static class HighScore implements Serializable
	{
		
		private String m_name;
		private Long m_score;

		public HighScore()
		{
			m_name = DEFAULT_NAME;
			m_score = DEFAULT_SCORE;
		}
		
		public HighScore(String name, long score)
		{
			m_name = name;
			m_score = score;
		}
		
		@Override
		public void write(Json json)
		{
			json.writeValue("name", m_name);
			json.writeValue("score", m_score);
		}

		@Override
		public void read(Json json, OrderedMap<String, Object> jsonData)
		{
			m_name = json.readValue("name", String.class, jsonData);
			m_score = json.readValue("score", Long.class, jsonData);
		}
		
		public String getName()
		{
			return m_name;
		}
		
		public Long getScore()
		{
			return m_score;
		}
	}
	
	private class ScoreInputListener implements TextInputListener
	{
		private long m_score = 0;
		private int m_pos = 0;
		
		public ScoreInputListener(long score, int pos)
		{
			m_score = score;
			m_pos = pos;
		}
		
		@Override
		public void input(String name)
		{
			Matcher matcher = DISALLOWED_PATTERN.matcher(name);
			if(matcher.find())
			{
		    	Gdx.input.getTextInput(new ScoreInputListener(m_score, m_pos), "Only A-Z, a-z, 0-9, \"_\" and space are allowed:", "Player1");
			}
			else
			{
				m_logger.debug("User = " + name + ", score = " + m_score);
		    	
				m_highScores.add(m_pos, new HighScore(name, m_score));
				
				// Remove the last entry
				m_highScores.remove(m_highScores.get(NUM_SCORES));
				updateScores(m_pos);
			}
		}
		
		@Override
		public void canceled()
		{
			m_logger.debug("User cancelled text input");
		}
	}
	
	private ArrayList<HighScore> m_highScores = new ArrayList<HighScoresScreen.HighScore>();
	private Table m_table = new Table();
	private Table m_subTable = new Table();
    private BlockFrenzyGame m_game;
    private Logger m_logger = new Logger(HighScoresScreen.class.getSimpleName(), Logger.DEBUG);

	public HighScoresScreen()
	{
	}
	
	public HighScoresScreen(BlockFrenzyGame game)
	{
		m_game = game;
	}
	
	public void initialize()
	{
		if(m_highScores == null || m_highScores.isEmpty())
		{
			m_highScores = new ArrayList<HighScoresScreen.HighScore>(NUM_SCORES);
			for(int i = 0; i < NUM_SCORES; ++i)
			{
				m_highScores.add(new HighScore());
			}
		}

//		m_table.setWidth(m_game.getWidth());
//		m_table.setHeight(m_game.getHeight());
//		m_table.setX(0.0F);
//		m_table.setY(0.0F);

		m_table.clear();
		m_table.setFillParent(true);
		m_table.top().center();
		m_table.row();
		m_table.add(new Label(HIGH_SCORES_STRING, new LabelStyle(BlockFrenzyTextures.getInstance().getTitleFont(), Color.WHITE)));
		m_table.pad(PAD);
		m_subTable.center().bottom();
		
		m_table.row().fill().expand();
		m_table.add(m_subTable);
		
		updateScores(-1);
	}
    
    public void setGame(BlockFrenzyGame game)
    {
    	m_game = game;
    }
	
	@Override
	public void render(float delta)
	{
        m_game.getStage().act(delta);
        m_game.getStage().draw();
        
        if(BlockFrenzyGame.DEBUG_MODE)
        {
        	Table.drawDebug(m_game.getStage());
        }
	}
	
	private void updateScores(int newPos)
	{
		m_subTable.clear();
		LabelStyle style = new LabelStyle(BlockFrenzyTextures.getInstance().getGameplayFont(), Color.WHITE);
		for(int i = 0; i < m_highScores.size(); ++i)
		{
			LabelStyle currStyle = style;
			HighScore score = m_highScores.get(i);
			if(i == newPos)
			{
				currStyle = new LabelStyle(style);
				currStyle.fontColor.r = 0.5F;
				currStyle.fontColor.g = 1.0F;
				currStyle.fontColor.b = 0.0F;
			}
			m_subTable.row().expand();
			m_subTable.add(new Label(String.valueOf(i + 1) + ") " + score.getName(), currStyle)).align(BaseTableLayout.LEFT);
			Label label = new Label(String.valueOf(score.getScore().longValue()), currStyle);
			m_subTable.add(label).align(BaseTableLayout.RIGHT);
		}
	}
	
	public void addHighScore(final long score)
	{
		for(int i = 0; i < m_highScores.size(); ++i)
		{
			if(m_highScores.get(i).getScore() < score)
			{
		    	// Prompt for name entry
		    	Gdx.input.getTextInput(new ScoreInputListener(score, i), "Enter your name:", "Player1");
		    	
				break;
			}
		}
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show()
	{
		m_logger.debug("show()");
		m_game.getStage().addActor(m_table);
	}

	@Override
	public void hide()
	{
		m_logger.debug("hide()");
		m_table.remove();
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume()
	{
	}

	@Override
	public void dispose()
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void write(Json json)
	{
		json.writeValue("scores", m_highScores);
	}

	@Override
	public void read(Json json, OrderedMap<String, Object> jsonData)
	{
		json.setElementType(HighScoresScreen.class, "m_highScores", HighScore.class);
		m_highScores = json.readValue("scores", m_highScores.getClass(), jsonData);
	}

}
