package com.blockfrenzy;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Json.Serializable;
import com.badlogic.gdx.utils.OrderedMap;

/**
 * @author Aaron
 *
 */
public class Timer implements Serializable
{
    /**
     * The time the timer started at
     */
    protected float m_remaining;
    
    /**
     * Amount of time the timer is set to
     */
    protected float m_interval;
    
    // Should only be used by Json reader
    public Timer(){}
    
    /**
     * This constructor creates a new Timer object initialized with the interval given. If the 
     * start parameter is non-null, it is used as the start time, otherwise System.currentTimeMillis()
     * is used.
     * @param interval The interval this timer represents
     * @param start The optional parameter indicating the start time.
     */
    public Timer(float interval) throws IllegalArgumentException
    {
        m_interval = interval;
        m_remaining = m_interval;
    }
    
    public Timer(Timer src)
    {
        this.m_interval = src.m_interval;
        this.m_remaining = src.m_remaining;
    }
    
    public float getInterval() 
    {
        return m_interval;
    }
    
    
    /**
     * Checks if the timer has elapsed. The calculation will use currentTime if it is non-null,
     * otherwise System.currentTimeMillis() will be used.
     * @param currentTime The optional parameter indicating the current time
     * @return The time remaining (will be negative if timer elapsed)
     */
    public float getRemainingTime()
    {
        return m_remaining;
    }
    
    /**
     * This resets the timer. If the interval parameter is non-null, the Timer object's 
     * interval will be set to this value. Otherwise it will remain unchanged. If the start is
     * non-null, it will be used instead of a call to System.currentTimeMillis()
     * @param interval The new interval for the Timer object to use
     * @param start The optional start value to use
     */
    public void reset(Float interval) throws IllegalArgumentException
    {
        if(interval != null)
        {
            if(interval < 0.0F) throw new IllegalArgumentException("interval time must be > 0");
            m_interval = interval;
        }
        
        m_remaining = m_interval;
    }
    
    public void update(float delta)
    {
        m_remaining -= delta;
    }

    @Override
    public void write(Json json)
    {
        json.writeValue("interval", m_interval);
        
        float remaining = m_remaining;
        if(remaining < 0) remaining = 0;
        
        json.writeValue("remains", remaining);
    }

    @Override
    public void read(Json json, OrderedMap<String, Object> jsonData)
    {
        m_interval = json.readValue("interval", float.class, jsonData);
        
        // The start time should be the current time minus the elapsed time
        m_remaining = json.readValue("remains", float.class, jsonData);
    }
}
