package com.blockfrenzy;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.utils.Logger;

public class MenuScreen implements Screen
{
    private SelectWindow   	m_selectWindow;
    private BlockFrenzyGame m_game;
    private Logger m_logger = new Logger(MenuScreen.class.getSimpleName());

    public MenuScreen(BlockFrenzyGame game)
    {
    	m_logger.setLevel(Logger.DEBUG);
        m_game = game;
        m_selectWindow = new SelectWindow(game);
        m_selectWindow.setWidth(m_game.getStage().getWidth() /*BlockFrenzyGame.SCREEN_WIDTH*/);
        m_selectWindow.setHeight(m_game.getStage().getHeight() /*BlockFrenzyGame.SCREEN_HEIGHT*/);
        m_selectWindow.setupButtons();
	}
    
	@Override
	public void render(float delta)
	{
        m_game.getStage().act(delta);
		m_game.getStage().draw();
	}

	@Override
	public void resize(int width, int height)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show()
	{
    	m_logger.debug("show");
		m_game.getStage().addActor(m_selectWindow);
        m_selectWindow.setupButtons();
	}

	@Override
	public void hide()
	{
    	m_logger.debug("hide");
		m_selectWindow.remove();
	}

	@Override
	public void pause()
	{
        //m_selectWindow.setupButtons();
		hide();
	}

	@Override
	public void resume()
	{
		show();
	}

	@Override
	public void dispose()
	{
		// TODO Auto-generated method stub
		
	}

}
