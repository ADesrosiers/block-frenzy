package com.blockfrenzy;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Json.Serializable;
import com.badlogic.gdx.utils.Logger;
import com.badlogic.gdx.utils.OrderedMap;

public class BlockGenerator
{
	private static final String BLOCKS_CONFIG_FILE = "data/blocks.json";
	
	private Color[] blockTypes = null;
	private static BlockGenerator instance = null;
	
	private BlockGenerator()
	{
	    // Initialize the block generator
		String text = BlockFrenzyIO.read(Gdx.files.internal(BLOCKS_CONFIG_FILE));
			
	    Json json = new Json();
		blockTypes = json.fromJson(Color[].class, text);
	}
	
	public static BlockGenerator getInstance()
	{
		if(instance == null)
		{
		    instance = new BlockGenerator();
		}
		return instance;
	}

    public Block randomBlock()
    {
        return new Block(((int) (Math.random() * blockTypes.length)) + 1);
    }
    
    public int getNBlockTypes()
    {
    	return blockTypes.length;
    }
    
    public Color getColor(int id)
    {
    	return blockTypes[id];
    }
}
