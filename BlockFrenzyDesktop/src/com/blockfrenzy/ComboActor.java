package com.blockfrenzy;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;
import static com.badlogic.gdx.math.Interpolation.*;

public class ComboActor extends Actor
{
    private static final float DURATION = 2.0F;
    private BitmapFont m_font = BlockFrenzyTextures.getInstance().getGameplayFont();
    private BlockFrenzyTextures m_textures = BlockFrenzyTextures.getInstance();
    private String m_value;
    private CircleType m_type;

    public enum CircleType
    {
    	COMBO(new Color(0.5F, 0.5F, 1.0F, 0.5F)),
    	CHAIN(new Color(0.0F, 1.0F, 1.0F, 0.5F));
    	
    	private final Color m_color;
    	
    	private CircleType(Color color)
    	{
    		m_color = color;
    	}
    	
    	public Color getColor()
    	{
    		return m_color;
    	}
    }
    
    public ComboActor(CircleType type, float x, float y, String value)
    {
        this.setX(x);
        this.setY(y);
        this.setColor(1.0F, 1.0F, 1.0F, 1.0F);
        
        TextureRegion circleEdge = m_textures.getCircleEdgeTexture();
        this.setWidth(circleEdge.getRegionWidth());
        this.setHeight(circleEdge.getRegionHeight());
        
        m_type = type;
        
        addAction(sequence(parallel(
			        			fadeOut(DURATION, pow2Out), 
			        			moveBy(0.0F, (float)m_textures.getCircleTexture().getRegionWidth(), DURATION, pow2Out)),
	        		removeActor()));
        m_value = value;
    }

    @Override
    public void draw(SpriteBatch batch, float parentAlpha)
    {
        Color pushColor = batch.getColor(); 
		batch.setColor(this.getColor());
        batch.draw(m_textures.getCircleEdgeTexture(), getX(), getY());
        
        Color c = m_type.getColor();
        c.a = this.getColor().a;
        batch.setColor(m_type.getColor());

        TextureRegion circle = m_textures.getCircleTexture();
        TextureRegion glare = m_textures.getCircleGlare();
        batch.draw(circle, getX(), getY());
        
        m_font.setColor(0.0F, 0.0F, 0.0F, this.getColor().a);
        TextBounds textBounds = m_font.getBounds(m_value);
        m_font.draw(batch, m_value, getX() + circle.getRegionWidth() / 2 - textBounds.width / 2, getY() + circle.getRegionHeight() / 2 + m_font.getCapHeight() / 2);

		// Draw the glare
		batch.setColor(new Color(Color.WHITE.r, Color.WHITE.g, Color.WHITE.b, this.getColor().a));
        batch.draw(glare, getX(), getY());
        
        batch.setColor(pushColor);
    }

    @Override
    public Actor hit(float x, float y)
    {
        // TODO Auto-generated method stub
        return null;
    }
}
