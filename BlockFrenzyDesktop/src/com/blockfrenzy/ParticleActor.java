package com.blockfrenzy;

import com.badlogic.gdx.graphics.g2d.ParticleEffectPool.PooledEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class ParticleActor extends Actor
{
	PooledEffect m_effect;
	
	public ParticleActor(PooledEffect effect)
	{
		m_effect = effect;
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha)
	{
		m_effect.draw(batch);
	}

	@Override
	public Actor hit(float x, float y)
	{
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void act(float delta)
	{
		m_effect.update(delta);
		if(m_effect.isComplete())
		{
			m_effect.free();
			this.remove();
		}
	}

}
